/*****************************************************************************
    Torsion

    Last Updated   - February 5, 2008
    Updated By     - Geoff Rollins

*****************************************************************************/

/*****************************************************************************
    Package and import statments
*/

package net.geoffrollins.oscar;

import java.io.*;


/*****************************************************************************
    Class Definitions
*/

/*============================================================================
    Harmonic oscillator approximation of the angle between two covalent bonds
============================================================================*/
public class Torsion 
extends AbstractEnergyTerm
{

	static final double RADIAN_CONVERSION_FACTOR = Math.PI / 180.0;
	static final String IDENTIFIER = "Torsion";
	/** 
    *	Default class constructor.
    */
    public Torsion( )
        {
			setAtoms( new Atom[4] ); //allocates memory for the four atoms joined by this torsion
			setName( IDENTIFIER );
			setForceConstant( 0.0 );
			setMultiplicity( 0 );
			setDelta( 0.0 );
        }

	/** 
    *	Class constructor.
    */
    public Torsion( Atom[] atoms, double forceConstant, int multiplicity, double delta )
        {
        	setAtoms( atoms );
			setName( IDENTIFIER );
			setForceConstant( forceConstant * AbstractEnergyTerm.CEU_CONVERSION_FACTOR );
			setMultiplicity( multiplicity );
			setDelta( delta * RADIAN_CONVERSION_FACTOR );
			
			/*
			try
        	{
        		myLogWriter = new PrintWriter( new File( "torsion.txt" ) );
        	}
        	catch( Exception e )
        	{
        		myLogWriter = null;
        	}
        	*/
        }

    
    /*====================================================================
        Public Member Functions
    */
    
    public double
    calculateDeltaX()
    {
    	Atom[] atoms = getAtoms();
    	return atoms[1].getX() - atoms[0].getX(); 
    }

	public double
	calculateDeltaY()
	{
		Atom[] atoms = getAtoms();
		return atoms[1].getY() - atoms[0].getY();
    }
    
    public double
    calculateDeltaZ()
    {
    	Atom[] atoms = getAtoms();
    	return atoms[1].getZ() - atoms[0].getZ();
    }
    
    public double
    calculateDeltaX2()
    {
    	Atom[] atoms = getAtoms();
    	return atoms[2].getX() - atoms[1].getX(); 
    }

	public double
	calculateDeltaY2()
	{
		Atom[] atoms = getAtoms();
		return atoms[2].getY() - atoms[1].getY();
    }
    
    public double
    calculateDeltaZ2()
    {
    	Atom[] atoms = getAtoms();
    	return atoms[2].getZ() - atoms[1].getZ();
    }
    
    public double
    calculateDeltaX3()
    {
    	Atom[] atoms = getAtoms();
    	return atoms[3].getX() - atoms[2].getX(); 
    }

	public double
	calculateDeltaY3()
	{
		Atom[] atoms = getAtoms();
		return atoms[3].getY() - atoms[2].getY();
    }
    
    public double
    calculateDeltaZ3()
    {
    	Atom[] atoms = getAtoms();
    	return atoms[3].getZ() - atoms[2].getZ();
    }
        
    /**-----------------------------------------------------------------
    *	Calculate the angle measure (the distance between the two atoms connected by this angle)
	*
	*	@return			current angle measure (radians)
	--------------------------------------------------------------------*/
    public double
    calculateSeparation( )
    {	
    	double dx1 = calculateDeltaX(); 
    	double dy1 = calculateDeltaY();
    	double dz1 = calculateDeltaZ();
    	double dx2 = calculateDeltaX2(); 
    	double dy2 = calculateDeltaY2();
    	double dz2 = calculateDeltaZ2();
    	double dx3 = calculateDeltaX3();
    	double dy3 = calculateDeltaY3();
    	double dz3 = calculateDeltaZ3();
    	
    	double dot11 = (dx1 * dx1) + (dy1 * dy1) + (dz1 * dz1);
    	double dot12 = (dx1 * dx2) + (dy1 * dy2) + (dz1 * dz2);
    	double dot13 = (dx1 * dx3) + (dy1 * dy3) + (dz1 * dz3);
    	double dot22 = (dx2 * dx2) + (dy2 * dy2) + (dz2 * dz2);
    	double dot23 = (dx2 * dx3) + (dy2 * dy3) + (dz2 * dz3);
    	double dot33 = (dx3 * dx3) + (dy3 * dy3) + (dz3 * dz3);
    	
    	double cA  = (dot13 * dot22) - (dot12 * dot23);
    	double cB1 = (dot11 * dot22) - (dot12 * dot12);
    	double cB2 = (dot22 * dot33) - (dot23 * dot23);
    	double cD  = Math.sqrt( cB1 * cB2 );
    	double c   = cA / cD;
    	
    	double torsionAngle = Math.acos( c );
    	
    	return Math.PI - torsionAngle;
    }
    
	/**-----------------------------------------------------------------
    *	Calculate the energy of this angle
	*	E = 1/2 * forceConstant * (measure - equilibriumAngle)^2
	*
	*	@return			energy of this angle
	--------------------------------------------------------------------*/
    public double
    calculateEnergy( )
    {
    	double currentAngle = calculateSeparation();
  	
  		//double cosAngle =  Math.cos( currentAngle );
  
  		//writeLog( String.valueOf(currentAngle / RADIAN_CONVERSION_FACTOR) );
  
  		return getForceConstant() * (1 + Math.cos( getMultiplicity() * currentAngle - getDelta()));
    	//return 15.50 * (1.000 + (1.310 + (-1.414 + (-0.330 + (2.828 - 3.394 * cosAngle) * cosAngle) * cosAngle) * cosAngle) * cosAngle);
    }

	/**-----------------------------------------------------------------
    *	Calculate the force of this term based on the current
    *	positions of its constituent atoms. Add this force to
    *	the atoms' current force values.
	*
	*	F = -forceConstant * (angle - equilibriumAngle)
	*	Atoms 0, 1, and 2. Atom 1 is the vertex.
	--------------------------------------------------------------------*/
    public double
    calculateForce()
    {	
    	double dx1 = calculateDeltaX(); 
    	double dy1 = calculateDeltaY();
    	double dz1 = calculateDeltaZ();
    	double dx2 = calculateDeltaX2(); 
    	double dy2 = calculateDeltaY2();
    	double dz2 = calculateDeltaZ2();
    	double dx3 = calculateDeltaX3();
    	double dy3 = calculateDeltaY3();
    	double dz3 = calculateDeltaZ3();
    	
    	double dot11 = (dx1 * dx1) + (dy1 * dy1) + (dz1 * dz1);
    	double dot12 = (dx1 * dx2) + (dy1 * dy2) + (dz1 * dz2);
    	double dot13 = (dx1 * dx3) + (dy1 * dy3) + (dz1 * dz3);
    	double dot22 = (dx2 * dx2) + (dy2 * dy2) + (dz2 * dz2);
    	double dot23 = (dx2 * dx3) + (dy2 * dy3) + (dz2 * dz3);
    	double dot33 = (dx3 * dx3) + (dy3 * dy3) + (dz3 * dz3);
    	
    	double cA  = (dot13 * dot22) - (dot12 * dot23);
    	double cB1 = (dot11 * dot22) - (dot12 * dot12);
    	double cB2 = (dot22 * dot33) - (dot23 * dot23);
    	double cD  = Math.sqrt( cB1 * cB2 );
    	double c   = cA / cD;
    	//double force = 15.50 * (1.31 + (2 * -1.414 + (3 * -0.33 + (4 * 2.828 + 5 * -3.394 * c ) * c ) * c ) * c );
	
		double angle = Math.PI - Math.acos( c );

		double force = getForceConstant() * getMultiplicity() * Math.sin(getMultiplicity() * angle - getDelta());

		double t1 = cA;
		double t2 = (dot11 * dot23) - (dot12 * dot13);
		double t3 = -cB1;
		double t4 = cB2;
		double t5 = (dot13 * dot23) - (dot12 * dot33);
		double t6 = -cA;
		
		double cR1 = dot12 / dot22;
		double cR2 = dot23 / dot22;
    	
    	double f1x = force * dot22 * (t1 * dx1 + t2 * dx2 + t3 * dx3) / (cD * cB1);
    	double f1y = force * dot22 * (t1 * dy1 + t2 * dy2 + t3 * dy3) / (cD * cB1);
    	double f1z = force * dot22 * (t1 * dz1 + t2 * dz2 + t3 * dz3) / (cD * cB1);
    	
    	double f2x = force * dot22 * (t4 * dx1 + t5 * dx2 + t6 * dx3) / (cD * cB2);
    	double f2y = force * dot22 * (t4 * dy1 + t5 * dy2 + t6 * dy3) / (cD * cB2);
    	double f2z = force * dot22 * (t4 * dz1 + t5 * dz2 + t6 * dz3) / (cD * cB2);
    	
    	double xForceOnAtom1 = f1x;
    	double yForceOnAtom1 = f1y;
    	double zForceOnAtom1 = f1z;
    	
    	double xForceOnAtom2 = (-(1+cR1) * f1x) + (cR2 * f2x);
    	double yForceOnAtom2 = (-(1+cR1) * f1y) + (cR2 * f2y);
    	double zForceOnAtom2 = (-(1+cR1) * f1z) + (cR2 * f2z);
    	
    	double xForceOnAtom3 = (-(1+cR2) * f2x) + (cR1 * f1x);
    	double yForceOnAtom3 = (-(1+cR2) * f2y) + (cR1 * f1y);
    	double zForceOnAtom3 = (-(1+cR2) * f2z) + (cR1 * f1z);
    	
    	double xForceOnAtom4 = f2x;
    	double yForceOnAtom4 = f2y;
    	double zForceOnAtom4 = f2z;
    	
    	double[] forceOnAtom1 = {xForceOnAtom1, yForceOnAtom1, zForceOnAtom1};
    	double[] forceOnAtom2 = {xForceOnAtom2, yForceOnAtom2, zForceOnAtom2};
    	double[] forceOnAtom3 = {xForceOnAtom3, yForceOnAtom3, zForceOnAtom3};
    	double[] forceOnAtom4 = {xForceOnAtom4, yForceOnAtom4, zForceOnAtom4};
    	
    	Atom[] atoms = getAtoms();
    	atoms[0].addForce( forceOnAtom1 );
    	atoms[1].addForce( forceOnAtom2 );
    	atoms[2].addForce( forceOnAtom3 );
    	atoms[3].addForce( forceOnAtom4 );
    	
    	/*
    	System.out.print( "Atom1: " ); System.out.print( xForceOnAtom1 ); System.out.print( "\t" ); System.out.print( yForceOnAtom1 ); System.out.print( "\t" ); System.out.println( zForceOnAtom1 );
    	System.out.print( "Atom2: " ); System.out.print( xForceOnAtom2 ); System.out.print( "\t" ); System.out.print( yForceOnAtom2 ); System.out.print( "\t" ); System.out.println( zForceOnAtom2 );
    	System.out.print( "Atom3: " ); System.out.print( xForceOnAtom3 ); System.out.print( "\t" ); System.out.print( yForceOnAtom3 ); System.out.print( "\t" ); System.out.println( zForceOnAtom3 );
    	System.out.print( "Atom4: " ); System.out.print( xForceOnAtom4 ); System.out.print( "\t" ); System.out.print( yForceOnAtom4 ); System.out.print( "\t" ); System.out.println( zForceOnAtom4 ); System.out.println( "" );
    	*/
    	
    	return 0.0;
    }



	public String
    toString( )
    {
	    Atom[] atoms = getAtoms();
	    return atoms[0].getSerialNumber() + "\t" + atoms[1].getSerialNumber() + "\t" + atoms[2].getSerialNumber() + "\t" + atoms[3].getSerialNumber();
    }

	/**-----------------------------------------------------------------
	*	Accessors
	*--------------------------------------------------------------------*/
	public double
	getForceConstant()
	{
		return myForceConstant;
	}
	public void
	setForceConstant( double newForceConstant )
	{
		myForceConstant = newForceConstant;
	}
	public int
	getMultiplicity()
	{
		return myMultiplicity;
	}
	public void
	setMultiplicity( int newMultiplicity )
	{
		myMultiplicity = newMultiplicity;
	}
	public double
	getDelta()
	{
		return myDelta;
	}
	public void
	setDelta( double newDelta )
	{
		myDelta = newDelta;
	}

    /*====================================================================
        Private Member Functions
    */
	private void 
	writeLog( String text )
	{
		// Stub function
		//myLogWriter.println( text );
		;
	}

    /*====================================================================
        Private Data Members
    */
	private double myForceConstant;
	private int myMultiplicity;
	private double myDelta;
	
	private PrintWriter myLogWriter;
	
    }

