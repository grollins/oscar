/*
 * TempProtocolPanel.java
 *
 * Last Modified: January 30, 2008
 */

/**
 *
 * @author  geoff
 */
 
package net.geoffrollins.oscar;
import java.util.LinkedList;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

public class TempProtocolPanel extends JPanel {
    
    /** Creates new form TempProtocolPanel */
    public TempProtocolPanel( ) 
    {
    	super();
                 
        contentPanel = getContentPanel();
        contentPanel.setBorder(new EmptyBorder(new Insets(10, 10, 10, 10)));
		contentPanel.setBackground( Color.decode("0xD0E6FF") );
		Font headerFont = new Font("Arial", Font.BOLD, 24);

        ImageIcon icon = getImageIcon();
        
        titlePanel = new javax.swing.JPanel();
        textLabel = new javax.swing.JLabel();
        iconLabel = new javax.swing.JLabel();
        separator = new javax.swing.JSeparator();

        setLayout(new java.awt.BorderLayout());

        titlePanel.setLayout(new java.awt.BorderLayout());
        titlePanel.setBackground(Color.decode("0x3C8CE4"));
        
        textLabel.setBackground(Color.decode("0x3C8CE4"));
        textLabel.setForeground(Color.white);
        textLabel.setFont(headerFont);
        textLabel.setText("Temperature Protocol");
        textLabel.setBorder(new EmptyBorder(new Insets(10, 10, 10, 10)));
        textLabel.setOpaque(true);

        iconLabel.setBackground(Color.white);
        if (icon != null)
            iconLabel.setIcon(icon);
        
        titlePanel.add(textLabel, BorderLayout.CENTER);
        titlePanel.add(iconLabel, BorderLayout.EAST);
        titlePanel.add(separator, BorderLayout.SOUTH);
        
        add(titlePanel, BorderLayout.NORTH);
        JPanel secondaryPanel = new JPanel();
        secondaryPanel.add(contentPanel, BorderLayout.NORTH);
        secondaryPanel.setBackground(Color.decode("0xD0E6FF"));
        add(secondaryPanel, BorderLayout.WEST);
        
        
    }
    
    public String[]
    getCycle1()
    {
    	String[] cycleParameters = new String[6];
    	cycleParameters[0] = "0";
    	cycleParameters[1] = String.valueOf(thermMethod1.getSelectedItem());
    	cycleParameters[2] = String.valueOf(thermInterval1.getSelectedItem());
    	cycleParameters[3] = String.valueOf(percentSim1.getSelectedItem());
    	cycleParameters[4] = initialTemp1.getText();
    	cycleParameters[5] = targetTemp1.getText();	
    	return cycleParameters;
    }
    
    public String[]
    getCycle2()
    {
    	String[] cycleParameters = new String[6];
    	cycleParameters[0] = "1";
    	cycleParameters[1] = String.valueOf(thermMethod2.getSelectedItem());
    	cycleParameters[2] = String.valueOf(thermInterval2.getSelectedItem());
    	cycleParameters[3] = String.valueOf(percentSim2.getSelectedItem());
    	cycleParameters[4] = initialTemp2.getText();
    	cycleParameters[5] = targetTemp2.getText();	
    	return cycleParameters;
    }
    
    public String[]
    getCycle3()
    {
    	String[] cycleParameters = new String[6];
    	cycleParameters[0] = "2";
    	cycleParameters[1] = String.valueOf(thermMethod3.getSelectedItem());
    	cycleParameters[2] = String.valueOf(thermInterval3.getSelectedItem());
    	cycleParameters[3] = String.valueOf(percentSim3.getSelectedItem());
    	cycleParameters[4] = initialTemp3.getText();
    	cycleParameters[5] = targetTemp3.getText();	
    	return cycleParameters;
    }
    
    public String[]
    getCycle4()
    {
    	String[] cycleParameters = new String[6];
    	cycleParameters[0] = "3";
    	cycleParameters[1] = String.valueOf(thermMethod4.getSelectedItem());
    	cycleParameters[2] = String.valueOf(thermInterval4.getSelectedItem());
    	cycleParameters[3] = String.valueOf(percentSim4.getSelectedItem());
    	cycleParameters[4] = initialTemp4.getText();
    	cycleParameters[5] = targetTemp4.getText();	
    	return cycleParameters;
    }
    
    public String[]
    getCycle5()
    {
    	String[] cycleParameters = new String[6];
    	cycleParameters[0] = "4";
    	cycleParameters[1] = String.valueOf(thermMethod5.getSelectedItem());
    	cycleParameters[2] = String.valueOf(thermInterval5.getSelectedItem());
    	cycleParameters[3] = String.valueOf(percentSim5.getSelectedItem());
    	cycleParameters[4] = initialTemp5.getText();
    	cycleParameters[5] = targetTemp5.getText();	
    	return cycleParameters;
    }
    
    public void
    setCycle1( String[] cycleParameters )
    {
    	thermMethod1.setSelectedItem( cycleParameters[1] );
    	thermInterval1.setSelectedItem( cycleParameters[2] );
		percentSim1.setSelectedItem( cycleParameters[3] );
		targetTemp1.setText( cycleParameters[4] );
		initialTemp1.setText( cycleParameters[5] );
    
    }
    
    public void
    setCycle2( String[] cycleParameters )
    {
    	thermMethod2.setSelectedItem( cycleParameters[1] );
    	thermInterval2.setSelectedItem( cycleParameters[2] );
		percentSim2.setSelectedItem( cycleParameters[3] );
		targetTemp2.setText( cycleParameters[4] );
		initialTemp2.setText( cycleParameters[5] );
    
    }
    
    public void
    setCycle3( String[] cycleParameters )
    {
    	thermMethod3.setSelectedItem( cycleParameters[1] );
    	thermInterval3.setSelectedItem( cycleParameters[2] );
		percentSim3.setSelectedItem( cycleParameters[3] );
		targetTemp3.setText( cycleParameters[4] );
		initialTemp3.setText( cycleParameters[5] );
    
    }
    
    public void
    setCycle4( String[] cycleParameters )
    {
    	thermMethod4.setSelectedItem( cycleParameters[1] );
    	thermInterval4.setSelectedItem( cycleParameters[2] );
		percentSim4.setSelectedItem( cycleParameters[3] );
		targetTemp4.setText( cycleParameters[4] );
		initialTemp4.setText( cycleParameters[5] );
    
    }
    
    public void
    setCycle5( String[] cycleParameters )
    {
    	thermMethod5.setSelectedItem( cycleParameters[1] );
    	thermInterval5.setSelectedItem( cycleParameters[2] );
		percentSim5.setSelectedItem( cycleParameters[3] );
		targetTemp5.setText( cycleParameters[4] );
		initialTemp5.setText( cycleParameters[5] );
    
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">//GEN-BEGIN:initComponents
    private JPanel 
    getContentPanel() {
    
    	Font titleFont = new Font("Arial", Font.PLAIN, 18);
        Font labelFont = new Font("Arial", Font.BOLD, 16);
        Font labelFont2 = new Font("Arial", Font.PLAIN, 16);
    
    	JPanel contentPanel1 = new JPanel();
        welcomeTitle = new JLabel();
        blankSpace = new JLabel();
		jPanel1 = new javax.swing.JPanel();
    
        cycleTitle = new javax.swing.JLabel();
        cycle1 = new javax.swing.JLabel();
        cycle2 = new javax.swing.JLabel();
        cycle3 = new javax.swing.JLabel();
        cycle4 = new javax.swing.JLabel();
        cycle5 = new javax.swing.JLabel();
        thermMethodTitle = new javax.swing.JLabel();
        thermIntervalTitle = new javax.swing.JLabel();
        percentSimTitle = new javax.swing.JLabel();
        targetTempTitle = new javax.swing.JLabel();
        thermMethod1 = new javax.swing.JComboBox();
        thermMethod2 = new javax.swing.JComboBox();
        thermMethod3 = new javax.swing.JComboBox();
        thermMethod4 = new javax.swing.JComboBox();
        thermMethod5 = new javax.swing.JComboBox();
        thermInterval1 = new javax.swing.JComboBox();
        thermInterval2 = new javax.swing.JComboBox();
        thermInterval3 = new javax.swing.JComboBox();
        thermInterval4 = new javax.swing.JComboBox();
        thermInterval5 = new javax.swing.JComboBox();
        percentSim1 = new javax.swing.JComboBox();
        targetTemp1 = new javax.swing.JTextField();
        targetTemp2 = new javax.swing.JTextField();
        targetTemp3 = new javax.swing.JTextField();
        targetTemp4 = new javax.swing.JTextField();
        targetTemp5 = new javax.swing.JTextField();
        percentSim2 = new javax.swing.JComboBox();
        percentSim3 = new javax.swing.JComboBox();
        percentSim4 = new javax.swing.JComboBox();
        percentSim5 = new javax.swing.JComboBox();
        initialTempTitle = new javax.swing.JLabel();
        initialTemp1 = new javax.swing.JTextField();
        initialTemp2 = new javax.swing.JTextField();
        initialTemp3 = new javax.swing.JTextField();
        initialTemp4 = new javax.swing.JTextField();
        initialTemp5 = new javax.swing.JTextField();

        jPanel1.setBorder(javax.swing.BorderFactory.createEmptyBorder());
        cycleTitle.setFont(labelFont);
        cycleTitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        cycleTitle.setText("Cycle");

        cycle1.setFont(labelFont);
        cycle1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        cycle1.setText("1");

        cycle2.setFont(labelFont);
        cycle2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        cycle2.setText("2");

        cycle3.setFont(labelFont);
        cycle3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        cycle3.setText("3");

        cycle4.setFont(labelFont);
        cycle4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        cycle4.setText("4");

        cycle5.setFont(labelFont);
        cycle5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        cycle5.setText("5");

        thermMethodTitle.setFont(labelFont);
     	thermMethodTitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        thermMethodTitle.setText("Thermalization Method");

        thermIntervalTitle.setFont(labelFont);
        thermIntervalTitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        thermIntervalTitle.setText("Therm. Interval");

        percentSimTitle.setFont(labelFont);
        percentSimTitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        percentSimTitle.setText("% of Simulation");

        targetTempTitle.setFont(labelFont);
        targetTempTitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        targetTempTitle.setText("Target T (K)");

        thermMethod1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Rescaling", "Reassignment", "Berendsen Bath", "Off" }));
        thermMethod1.setSelectedIndex(3);

        thermMethod2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Rescaling", "Reassignment", "Berendsen Bath", "Off" }));
        thermMethod2.setSelectedIndex(3);

        thermMethod3.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Rescaling", "Reassignment", "Berendsen Bath", "Off" }));
		thermMethod3.setSelectedIndex(3);

        thermMethod4.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Rescaling", "Reassignment", "Berendsen Bath", "Off" }));
        thermMethod4.setSelectedIndex(3);

        thermMethod5.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Rescaling", "Reassignment", "Berendsen Bath", "Off" }));
        thermMethod5.setSelectedIndex(3);

        thermInterval1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "0", "1", "5", "10", "50", "100", "500", "1000", "5000", "10000" }));
        thermInterval1.setSelectedIndex(0);

        thermInterval2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "0", "1", "5", "10", "50", "100", "500", "1000", "5000", "10000" }));
        thermInterval2.setSelectedIndex(0);

        thermInterval3.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "0", "1", "5", "10", "50", "100", "500", "1000", "5000", "10000" }));
        thermInterval3.setSelectedIndex(0);

        thermInterval4.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "0", "1", "5", "10", "50", "100", "500", "1000", "5000", "10000" }));
		thermInterval4.setSelectedIndex(0);

        thermInterval5.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "0", "1", "5", "10", "50", "100", "500", "1000", "5000", "10000" }));
		thermInterval5.setSelectedIndex(0);

        percentSim1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "0", "10", "20", "30", "40", "50", "60", "70", "80", "90", "100" }));
		percentSim1.setSelectedIndex(0);
		
        percentSim2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "0", "10", "20", "30", "40", "50", "60", "70", "80", "90", "100" }));
		percentSim2.setSelectedIndex(0);

        percentSim3.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "0", "10", "20", "30", "40", "50", "60", "70", "80", "90", "100" }));
		percentSim3.setSelectedIndex(0);
		
        percentSim4.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "0", "10", "20", "30", "40", "50", "60", "70", "80", "90", "100" }));
		percentSim4.setSelectedIndex(0);

        percentSim5.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "0", "10", "20", "30", "40", "50", "60", "70", "80", "90", "100" }));
		percentSim5.setSelectedIndex(0);

		targetTemp1.setText("0.0");
        targetTemp2.setText("0.0");
        targetTemp3.setText("0.0");
        targetTemp4.setText("0.0");
        targetTemp5.setText("0.0");

        initialTempTitle.setFont(labelFont);
        initialTempTitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        initialTempTitle.setText("Initial T (K)");

        initialTemp1.setText("0.0");
        initialTemp2.setText("0.0");
        initialTemp3.setText("0.0");
        initialTemp4.setText("0.0");
        initialTemp5.setText("0.0");

		thermMethod1.setFont( labelFont2 );
		thermMethod2.setFont( labelFont2 );
		thermMethod3.setFont( labelFont2 );
		thermMethod4.setFont( labelFont2 );
		thermMethod5.setFont( labelFont2 );
		thermInterval1.setFont( labelFont2 );
		thermInterval2.setFont( labelFont2 );
		thermInterval3.setFont( labelFont2 );
		thermInterval4.setFont( labelFont2 );
		thermInterval5.setFont( labelFont2 );
		percentSim1.setFont( labelFont2 );
		percentSim2.setFont( labelFont2 );
		percentSim3.setFont( labelFont2 );
		percentSim4.setFont( labelFont2 );
		percentSim5.setFont( labelFont2 );
		targetTemp1.setFont( labelFont2 );
		targetTemp2.setFont( labelFont2 );
		targetTemp3.setFont( labelFont2 );
		targetTemp4.setFont( labelFont2 );
		targetTemp5.setFont( labelFont2 );
		initialTemp1.setFont( labelFont2 );
		initialTemp2.setFont( labelFont2 );
		initialTemp3.setFont( labelFont2 );
		initialTemp4.setFont( labelFont2 );
		initialTemp5.setFont( labelFont2 );

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(cycleTitle, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 50, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, cycle5, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 50, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, cycle4, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 50, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, cycle3, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 50, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, cycle2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 50, Short.MAX_VALUE)
                    .add(cycle1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 50, Short.MAX_VALUE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, thermMethod1, 0, 183, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, thermMethod2, 0, 183, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, thermMethod3, 0, 183, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, thermMethod4, 0, 183, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, thermMethod5, 0, 183, Short.MAX_VALUE)
                    .add(thermMethodTitle, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 183, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, thermIntervalTitle, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, thermInterval1, 0, 110, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, thermInterval2, 0, 110, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, thermInterval3, 0, 110, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, thermInterval4, 0, 110, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, thermInterval5, 0, 110, Short.MAX_VALUE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(percentSim5, 0, 114, Short.MAX_VALUE)
                    .add(percentSim4, 0, 114, Short.MAX_VALUE)
                    .add(percentSim3, 0, 114, Short.MAX_VALUE)
                    .add(percentSim2, 0, 114, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, percentSimTitle, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, percentSim1, 0, 114, Short.MAX_VALUE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(initialTempTitle, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(initialTemp1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 83, Short.MAX_VALUE)
                    .add(initialTemp2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 83, Short.MAX_VALUE)
                    .add(initialTemp3, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 83, Short.MAX_VALUE)
                    .add(initialTemp4, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 83, Short.MAX_VALUE)
                    .add(initialTemp5, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 83, Short.MAX_VALUE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, targetTempTitle, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, targetTemp1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 87, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, targetTemp2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 87, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, targetTemp3, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 87, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, targetTemp4, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 87, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, targetTemp5, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 87, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanel1Layout.createSequentialGroup()
                        .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(thermIntervalTitle)
                            .add(percentSimTitle)
                            .add(targetTempTitle)
                            .add(thermMethodTitle)
                            .add(initialTempTitle))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(thermMethod1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(thermInterval1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(percentSim1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(targetTemp1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(initialTemp1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(cycle1))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(thermMethod2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(thermInterval2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(targetTemp2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(percentSim2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(cycle2)
                            .add(initialTemp2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(thermMethod3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(thermInterval3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(targetTemp3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(percentSim3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(cycle3)
                            .add(initialTemp3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(thermMethod4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(thermInterval4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(targetTemp4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(percentSim4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(cycle4)
                            .add(initialTemp4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(thermMethod5, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(thermInterval5, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(targetTemp5, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(percentSim5, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(cycle5)
                            .add(initialTemp5, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                    .add(cycleTitle))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        
        contentPanel1.setLayout(new java.awt.BorderLayout());

        jPanel1.add(blankSpace);
        jPanel1.setBackground( Color.decode("0xD0E6FF") );

        contentPanel1.add(jPanel1, java.awt.BorderLayout.CENTER);        
        contentPanel1.setBackground( Color.decode("0xD0E6FF") );
        
        return contentPanel1;
    }// </editor-fold>//GEN-END:initComponents
	
	
	private ImageIcon 
    getImageIcon() 
    {
        
        //  Icon to be placed in the upper right corner.
        
        return null;
    }
   
	
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel cycle1;
    private javax.swing.JLabel cycle2;
    private javax.swing.JLabel cycle3;
    private javax.swing.JLabel cycle4;
    private javax.swing.JLabel cycle5;
    private javax.swing.JLabel cycleTitle;
    private javax.swing.JTextField initialTemp1;
    private javax.swing.JTextField initialTemp2;
    private javax.swing.JTextField initialTemp3;
    private javax.swing.JTextField initialTemp4;
    private javax.swing.JTextField initialTemp5;
    private javax.swing.JLabel initialTempTitle;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JComboBox percentSim1;
    private javax.swing.JComboBox percentSim2;
    private javax.swing.JComboBox percentSim3;
    private javax.swing.JComboBox percentSim4;
    private javax.swing.JComboBox percentSim5;
    private javax.swing.JLabel percentSimTitle;
    private javax.swing.JTextField targetTemp1;
    private javax.swing.JTextField targetTemp2;
    private javax.swing.JTextField targetTemp3;
    private javax.swing.JTextField targetTemp4;
    private javax.swing.JTextField targetTemp5;
    private javax.swing.JLabel targetTempTitle;
    private javax.swing.JComboBox thermInterval1;
    private javax.swing.JComboBox thermInterval2;
    private javax.swing.JComboBox thermInterval3;
    private javax.swing.JComboBox thermInterval4;
    private javax.swing.JComboBox thermInterval5;
    private javax.swing.JLabel thermIntervalTitle;
    private javax.swing.JComboBox thermMethod1;
    private javax.swing.JComboBox thermMethod2;
    private javax.swing.JComboBox thermMethod3;
    private javax.swing.JComboBox thermMethod4;
    private javax.swing.JComboBox thermMethod5;
    private javax.swing.JLabel thermMethodTitle;
    // End of variables declaration//GEN-END:variables
    
    private javax.swing.JLabel welcomeTitle;
    private JPanel contentPanel;
    private JLabel iconLabel;
    private JSeparator separator;
    private JLabel textLabel;
    private JPanel titlePanel;
    private javax.swing.JLabel blankSpace;

	
}
