/*****************************************************************************
    VanDerWaals.java

    Last Updated   - January 17, 2008
    Updated By     - Geoff Rollins

*****************************************************************************/

/*****************************************************************************
    Package and import statments
*/

package net.geoffrollins.oscar;

/*****************************************************************************
    Class Definitions
*/

/*============================================================================
    van der Waals, non-bonded energy term. 12-6 Lennard-Jones potential.
============================================================================*/
public class VanDerWaals 
extends AbstractEnergyTerm
{

	static final String IDENTIFIER = "VDW";

	/** 
    *	Default class constructor.
    */
    public VanDerWaals( )
        {
			setAtoms( new Atom[2] ); //allocates memory for the two atoms joined by this bond
			setWellDepth( 0.0 );
			setEquilibriumSeparation( 0.0 );
			setName( IDENTIFIER );
        }

	/** 
    *	Class constructor.
    */
    public VanDerWaals( Atom[] atoms, double equilibriumSeparation, double wellDepth  )
        {
        	setAtoms( atoms );
			setWellDepth( wellDepth );
			setEquilibriumSeparation( equilibriumSeparation );
        	setName( IDENTIFIER );
        	
        	/*DEBUG
        	System.out.print( equilibriumSeparation );
        	System.out.print( "  " );
       		System.out.println( wellDepth );
       		*/
        }

    
    /*====================================================================
        Public Member Functions
    */
    
    public double
    calculateDeltaX()
    {
    	Atom[] atoms = getAtoms();
    	return atoms[1].getX() - atoms[0].getX(); 
    }

	public double
	calculateDeltaY()
	{
		Atom[] atoms = getAtoms();
		return atoms[1].getY() - atoms[0].getY();
    }
    
    public double
    calculateDeltaZ()
    {
    	Atom[] atoms = getAtoms();
    	return atoms[1].getZ() - atoms[0].getZ();
    }

    
    /**-----------------------------------------------------------------
    *	Calculate the distance between the two atoms governed by this van der Waals term
	*
	*	@return			current separation
	--------------------------------------------------------------------*/
    public double
    calculateSeparation()
    {
    	Atom[] atoms = getAtoms();
    	double deltaX = atoms[1].getX() - atoms[0].getX(); 
    	double deltaY = atoms[1].getY() - atoms[0].getY();
    	double deltaZ = atoms[1].getZ() - atoms[0].getZ();
    	return Math.sqrt( Math.pow( deltaX, 2 ) + Math.pow( deltaY, 2 ) + Math.pow( deltaZ, 2 ) );
    }
    
	/**-----------------------------------------------------------------
    *	Calculate the energy of this VDW term.
	*	E = well depth * ( (r_min/r)^12 - 2 * (r_min/r)^6 )
	*
	*	@return			energy of this van der Waals term
	--------------------------------------------------------------------*/
    public double
    calculateEnergy( )
    {
    	double separationRatio = getEquilibriumSeparation() / calculateSeparation();
  
    	return getWellDepth() * ( Math.pow( separationRatio, 12 ) - 2 * Math.pow( separationRatio, 6 ) );
    }

	/**-----------------------------------------------------------------
    *	Calculate the force of this VDW term. Add this force to atoms' current
    *	force values. Return virial (force DOT separation).
    *
	*	F = 12* well depth * ( (r_min^12/r^13) - (r_min^6/r^7) )
	--------------------------------------------------------------------*/
	public double
    calculateForce()
    {
    	double currentSeparation = calculateSeparation();
    	double idealSeparation = getEquilibriumSeparation();
    	
    	double totalForce = 12 * getWellDepth() * 
    			( Math.pow( idealSeparation, 12 )/Math.pow( currentSeparation, 13 ) - 
    			Math.pow( idealSeparation, 6 )/Math.pow( currentSeparation, 7 ) );
    	
    	double dx = calculateDeltaX(); 
    	double dy = calculateDeltaY();
    	double dz = calculateDeltaZ();
    	
    	double xForce = totalForce * dx/currentSeparation; 
    	double yForce = totalForce * dy/currentSeparation;
    	double zForce = totalForce * dz/currentSeparation;
    
    	double[] forceOnAtom0 = {-xForce, -yForce, -zForce};
    	double[] forceOnAtom1 = {xForce, yForce, zForce};
    	
    	Atom[] atoms = getAtoms();
    	atoms[0].addForce( forceOnAtom0 );
    	atoms[1].addForce( forceOnAtom1 );
    
    	double virial = (dx * xForce) + (dy * yForce) + (dz * zForce);
    	
    	return virial;
    
    }

	public String
    toString( )
    {
	    Atom[] atoms = getAtoms();
	    return atoms[0].getSerialNumber() + "\t" + atoms[1].getSerialNumber() + "\t" + getWellDepth()/AbstractEnergyTerm.CEU_CONVERSION_FACTOR + "\t" + getEquilibriumSeparation();
    }

	/**-----------------------------------------------------------------
	*	Accessors
	*--------------------------------------------------------------------*/
	
	public double
    getWellDepth( )
        {
			return myWellDepth;
        }

    public void
    setWellDepth( double newWellDepth )
        {
			myWellDepth = newWellDepth;
        }
        
	public double
    getEquilibriumSeparation( )
        {
			return myEquilibriumSeparation;
        }

    public void
    setEquilibriumSeparation( double newEquilibriumSeparation )
        {
			myEquilibriumSeparation = newEquilibriumSeparation;
        }     


    /*====================================================================
        Private Member Functions
    */
	

    /*====================================================================
        Private Data Members
    */
    private double myWellDepth;					// the depth of the energy minimum
	private double myEquilibriumSeparation;		// the separation with the minimum energy
	
    }

