/*****************************************************************************
    AbstractSimAlgorithm.java

    Last Updated   - January 16, 2008
    Updated By     - Geoff Rollins

	Abstract parent class for simulation algorithms, such as energy minimization, 
	monte carlo, and molecular dynamics.

*****************************************************************************/

/*****************************************************************************
    Package and import statments
*/

package net.geoffrollins.oscar;


/*****************************************************************************
    Class Definitions
*/

/*============================================================================
    Provides the framework for simulation algorithms
 ............................................................................

    Every simulation algorithm should know how to advance a MolecularSystem
    by one step and how to output the resulting energies. The algorithm must
    handle the energy output because each algorithm will output slightly
    different information. For example, an energy minimization algorithm would 
    include the current value of its convergence criterion but molecular
    dynamics and Monte Carlo algorithms don't have such a criterion.

============================================================================*/
public abstract class AbstractSimAlgorithm
    {

	/*====================================================================
        Public Member Functions
    */
	abstract public String simStep( MolecularSystem molSystem, Hamiltonian hamiltonian, int stepNumber, int numDimensions );

	abstract public String formatEnergy( MolecularSystem molSystem, Hamiltonian hamiltonian, int stepNumber );
	
	abstract public String getEnergyBanner( Hamiltonian hamiltonian );
	
	abstract public boolean isTimeToQuit();
	
	abstract public void
    setTemperature( double newTemperature );
    
    abstract public double
    getTemperature( );
    
    public void
    setID( String newID )
    {
    	myID = newID;
    }
    
    public String
    getID( )
    {
    	return myID;
    }
    
    /*====================================================================
        Private Member Functions
    */
	

    /*====================================================================
        Private Data Members
    */
	private String myID;
	
    } //end of public abstract class SimAlgorithm

