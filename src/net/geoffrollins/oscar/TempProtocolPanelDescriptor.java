package net.geoffrollins.oscar;
import com.nexes.wizard.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.LinkedList;


public class TempProtocolPanelDescriptor 
extends WizardPanelDescriptor
{
    
    public static final String IDENTIFIER = "TEMP_PROTOCOL_PANEL";
    
    TempProtocolPanel tempProtocolPanel;
    
    public TempProtocolPanelDescriptor() {        
        tempProtocolPanel = new TempProtocolPanel( );
		
        setPanelDescriptorIdentifier(IDENTIFIER);
        setPanelComponent(tempProtocolPanel);
        
    }
    
    public Object getNextPanelDescriptor() {
        return SimulationPanelDescriptor.IDENTIFIER;
    }
    
    public Object getBackPanelDescriptor() {
        return MDPanelDescriptor.IDENTIFIER;
    }
    
    public void aboutToDisplayPanel()
    {
    	// Check the temperature protocol for any initial values
		
		ParameterManager parameterManager = getWizard().getParameterManager();
		LinkedList cycles = parameterManager.getTemperatureProtocol().getCycles();
		
		//DEBUG
		//System.out.print( "Number of cycles: " );
		//System.out.println( cycles.size() );
		
		for( int i = 0; i < cycles.size(); i++ )
		{
			TemperatureCycle thisCycle = (TemperatureCycle)cycles.get( i );
			String[] cycleParameters = new String[6];
			cycleParameters[0] = String.valueOf( thisCycle.getID() );
			cycleParameters[1] = thisCycle.getThermMethod();
			cycleParameters[2] = String.valueOf( thisCycle.getThermInterval() );
			cycleParameters[3] = String.valueOf( thisCycle.getPercentSim() );
			cycleParameters[4] = String.valueOf( thisCycle.getTargetTemp() );
			cycleParameters[5] = String.valueOf( thisCycle.getInitialTemp() );
			
			switch( thisCycle.getID() )
			{
				case 0:
					tempProtocolPanel.setCycle1( cycleParameters );
					break;
				case 1:
					tempProtocolPanel.setCycle2( cycleParameters );
					break;
				case 2:
					tempProtocolPanel.setCycle3( cycleParameters );
					break;
				case 3:
					tempProtocolPanel.setCycle4( cycleParameters );
					break;
				case 4:
					tempProtocolPanel.setCycle5( cycleParameters );
					break;
				default:
					System.out.print( "Unexpected temperature cycle ID: " );
					System.out.print( thisCycle.getID() );
					
			} //end switch
			
		} //end for loop
    }
    
    public void aboutToHidePanel() 
    {
    	// save temperature protocol to parameter manager
    	ParameterManager parameterManager = getWizard().getParameterManager();
    	
    	//save cycle1 parameters
    	String[] cycleParameters = tempProtocolPanel.getCycle1();
    	parameterManager.addCycle( cycleParameters[0], cycleParameters[1], cycleParameters[2], cycleParameters[3], cycleParameters[4], cycleParameters[5] );
		
		//save cycle2 parameters
		cycleParameters = tempProtocolPanel.getCycle2();
    	parameterManager.addCycle( cycleParameters[0], cycleParameters[1], cycleParameters[2], cycleParameters[3], cycleParameters[4], cycleParameters[5] );
		
		//save cycle3 parameters
		cycleParameters = tempProtocolPanel.getCycle3();
    	parameterManager.addCycle( cycleParameters[0], cycleParameters[1], cycleParameters[2], cycleParameters[3], cycleParameters[4], cycleParameters[5] );
		
		//save cycle4 parameters
		cycleParameters = tempProtocolPanel.getCycle4();
    	parameterManager.addCycle( cycleParameters[0], cycleParameters[1], cycleParameters[2], cycleParameters[3], cycleParameters[4], cycleParameters[5] );
		
		//save cycle5 parameters
		cycleParameters = tempProtocolPanel.getCycle5();
    	parameterManager.addCycle( cycleParameters[0], cycleParameters[1], cycleParameters[2], cycleParameters[3], cycleParameters[4], cycleParameters[5] );
    	
    }      

}
