/*****************************************************************************
    SteepestDescentMinimization.java

    Last Updated   - January 22, 2008
    Updated By     - Geoff Rollins

    Energy minimization, steepest descent algorithm
    
    Steepest Descent:
    1. Compute trial configuration for atoms in the system
    	x_trial = x + Fx * step size
    2. If the trial configuration reduces the energy of the system...
    	a. save trial configuration
    	b. increase step size by multiplicative factor (1.2)
    3. If the trial configuration increases the energy of the system...
    	a. restore previous configuration
    	b. decrease step size by multiplicative factor (0.5)
    

*****************************************************************************/

/*****************************************************************************
    Package and import statments
*/

package net.geoffrollins.oscar;

import java.util.LinkedList;
import java.util.ListIterator;
import java.util.HashMap;
import com.braju.format.*;

/*****************************************************************************
    Class Definitions
*/

/*============================================================================
    One-line summary of class goal
 ............................................................................

    Description of class

============================================================================*/
public class SteepestDescentMinimization
    extends AbstractSimAlgorithm 
    {
    
    /*====================================================================
        Constants
    */
    static double GAS_CONSTANT = 0.836; //gas constant = 2 cal/mol*K  =  0.836 CEU/mol*K
    public static final String EM_IDENTIFIER = "Energy Minimization";
    /*--------------------------------------------------------------------
        Constructors
    --------------------------------------------------------------------*/

    /** 
    *	Default class constructor.
    */
    public SteepestDescentMinimization( )
        {
        	setOldAtomPositions( new HashMap() );
			setStepSize( 1.0 );
			setConvergenceCriterion( 0.0001 );
			setID( EM_IDENTIFIER );
			setIsConverged( false );
        }

    /** 
    *	Class constructor.
    */
    public SteepestDescentMinimization( double stepSize, double convCrit )
        {
        	setOldAtomPositions( new HashMap() );
        	setStepSize( stepSize );
			setConvergenceCriterion( convCrit );
			setID( EM_IDENTIFIER );
			setIsConverged( false );
		}


    /*====================================================================
        Public Member Functions
    */

	/** 
    *	Performs one step of minimization
    *
    *	@param	molSystem
    *	@param	hamiltonian
    *	@param	stepNumber
    *	@return the energy of the system at this step
    */
    public String 
    simStep( MolecularSystem molSystem, Hamiltonian hamiltonian, int stepNumber, int numDimensions )
    {
    	ListIterator atomList;
    	Atom thisAtom;
    	
    	// 1. Initialize force on each atom to zero
    	atomList = molSystem.iterateAtoms();
    	while( atomList.hasNext() )
    	{
    		thisAtom = (Atom)atomList.next();
    		thisAtom.zeroForce();
    	}
    	
    	// 2. Calculate force on each atom
    	hamiltonian.calculateForce();
    	
    	// 3. Save atomic coordinates
    	saveAtomPositions( molSystem );
    	
    	// 4. Calculate energy of current configuration
    	double oldPotentialEnergy = calculatePotentialEnergy( hamiltonian );
    	
    	// 5. Advance atoms to trial positions
    	atomList = molSystem.iterateAtoms(); //reset iterator to the start of the atom list
    	while( atomList.hasNext() )
    	{
    		thisAtom = (Atom)atomList.next();
    		
    		double[] thisForce = thisAtom.getForce();
    		double[] thisPosition = thisAtom.getPosition();
    		
    		/*
    		if( thisAtom.getSerialNumber() == 1 )
    		{
				System.out.print( "Atom " ); 
				System.out.print( thisAtom.getSerialNumber() );
				System.out.print( " Force = {" );
				System.out.print( thisForce[0] );
				System.out.print( "}," );
				System.out.print( "searchVector = {" );
				System.out.print( thisForce[0] * getStepSize() );
				//System.out.print( ", " );
				//System.out.print( thisForce[1] * getStepSize() );
				//System.out.print( ", " );
				//System.out.print( thisForce[2] * getStepSize() );
				System.out.print( "} at step " );
				System.out.println( stepNumber );
				System.out.print( " with step size " );
				System.out.println( getStepSize() );
			}
			*/
    		
    		
    		double xTrial = thisPosition[0] + thisForce[0] * getStepSize(); // x
    		double yTrial = thisPosition[1] + thisForce[1] * getStepSize(); // y
    		double zTrial = thisPosition[2] + thisForce[2] * getStepSize(); // z
    		double[] trialPosition = { xTrial, yTrial, zTrial };
    		
    		thisAtom.setPosition( trialPosition );
    		
    	}
    	
    	// 6. Calculate energy of the trial configuration
    	double trialPotentialEnergy = calculatePotentialEnergy( hamiltonian );
    	
    	// 7. Compare energy of the trial configuration with that of the previous configuration
    	if( trialPotentialEnergy <= oldPotentialEnergy )
    	{
    		//Keep trial positions
    			//nothing extra needs to be done since the atoms are already in the trial positions 
    		
    		//Check for convergence
    		if( Math.abs((oldPotentialEnergy - trialPotentialEnergy) / oldPotentialEnergy) < getConvergenceCriterion() )
    		{
    			setIsConverged( true );
    		}
    		
    		//increase the step size
    		setStepSize( getStepSize() * 1.2 );
    	}
    	else
    	{
    		//Restore old positions
    		restoreAtomPositions( molSystem );
    		//Cut the step size in half for the next step
    		setStepSize( getStepSize()/2 );
    	}
    
    	// Calculate energies and create energy output string
    	String outputString = formatEnergy( molSystem, hamiltonian, stepNumber );
    	
    	// Return energy output string
    	return outputString;
    }

	/*-----------------------------------------------------------------
	*	Accessors
	*--------------------------------------------------------------------*/
    public HashMap
    getOldAtomPositions(  )
    {
    	return myOldAtomPositions;
    	
    }
    
    public void
    setOldAtomPositions( HashMap newOldAtomPositions )
        {
			myOldAtomPositions = newOldAtomPositions;
        }
    
    public double
    getStepSize( )
        {
			return myStepSize;
        }

	public void
    setStepSize( double newStepSize )
        {
			myStepSize = newStepSize;
        }

	public double
    getConvergenceCriterion( )
        {
			return myConvergenceCriterion;
        }

	public void
    setConvergenceCriterion( double newConvergenceCriterion )
        {
			myConvergenceCriterion = newConvergenceCriterion;
        }
	
	public double
    getTemperature( )
    {
    	//temperature is not defined in energy minimization
    	return 0.0;
    }
	public void
    setTemperature( double newTemperature )
        {
        	//no need for temperature in minimization but we have to define
        	//this stub function in order to satisfy the requirements of the
        	//abstract parent class (AbstractSimAlgorithm)
			return;
        }

	public boolean
    getIsConverged( )
    {
    	return myIsConverged;
    }
    
	public void
    setIsConverged( boolean newIsConverged )
        {
			myIsConverged = newIsConverged;
        }

	public String formatEnergy( MolecularSystem molSystem, Hamiltonian hamiltonian, int stepNumber )
	{
		String outputString = "%8d     ";
		Parameters outputStringParameters = new Parameters( stepNumber );
		double potentialEnergy = 0.00;
		
		// Sum potential energy terms
		LinkedList energyList = hamiltonian.calculateEnergy( );
		ListIterator energyIterator = energyList.listIterator( 0 );
		double thisEnergy;
		while( energyIterator.hasNext() )
		{
			thisEnergy = (Double)energyIterator.next();
			// For output, convert energy from CEU to kcal/mol
			thisEnergy = thisEnergy / AbstractEnergyTerm.CEU_CONVERSION_FACTOR;
			
			outputString += "%10.4f     ";
			outputStringParameters = outputStringParameters.add( thisEnergy );
			
			potentialEnergy += thisEnergy;
		}
		
		// Add potential and step size to the output string
		outputString += "%10.4f     %9.12f \n";
		// Add potential and step size to the list of output parameters
		outputStringParameters.add( potentialEnergy ).add( getStepSize() );

		//System.out.println( outputString );
		return Format.sprintf( outputString, outputStringParameters );		
		
	}
	
	public String
	getEnergyBanner( Hamiltonian hamiltonian )
	{
		String bannerString = "%8s     ";
		Parameters bannerStringParameters = new Parameters( "Step" );
		
		LinkedList interactionList = hamiltonian.getInteractionNames( );
		ListIterator interactionIterator = interactionList.listIterator( 0 );
		String thisInteraction;
		while( interactionIterator.hasNext() )
		{
			thisInteraction = (String)interactionIterator.next();

			bannerString += "%10s     ";
			bannerStringParameters = bannerStringParameters.add( thisInteraction );
		}
		
		// Add potential, kinetic, total, and temp to the banner string
		bannerString += "%10s     %9s \n";
		bannerStringParameters.add( "Potential" ).add( "Step Size" );

		return Format.sprintf( bannerString, bannerStringParameters );	
	}
	
	/**
	*	If the convergence criterion has been satisfied, we can
	*	quit the simulation early.
	*
	*/
	public boolean isTimeToQuit()
	{
		if( getConvergenceCriterion() > 0.0 )
		{
			return getIsConverged();
		}
		// if the convergence criterion is not greater than zero, we're going to ignore it and let the minimization continue for the full step count specified by the user
		else
		{
			return false;
		}
	}
	
	/*====================================================================
        Private Member Functions
    */
    
    /** 
    *	Save current atomic positions
    */
	private void
	saveAtomPositions( MolecularSystem molSystem )
	{
		ListIterator atomList = molSystem.iterateAtoms();
		while( atomList.hasNext() )
		{
			Atom thisAtom = (Atom)atomList.next();
			double oldX = thisAtom.getX();
			double oldY = thisAtom.getY();
			double oldZ = thisAtom.getZ();
			double[] savedCoords = { oldX, oldY, oldZ };
			getOldAtomPositions().put( thisAtom, savedCoords );
		}
		
	}
	
	/** 
    *	Restore the atoms to their previous postitions
    */
	private void
	restoreAtomPositions( MolecularSystem molSystem )
	{
		ListIterator atomList = molSystem.iterateAtoms();
		Atom thisAtom;
		while( atomList.hasNext() )
		{
			thisAtom = (Atom)atomList.next();
			double[] oldPosition = (double[])getOldAtomPositions().get( thisAtom );
			thisAtom.setPosition( oldPosition );
		}
	}

	private double
	calculatePotentialEnergy( Hamiltonian hamiltonian )
	{
		LinkedList energyList = hamiltonian.calculateEnergy( );
		ListIterator energyIterator = energyList.listIterator( 0 );
		double potentialEnergy = 0.0;
		double thisEnergy;
		while( energyIterator.hasNext() )
		{
			thisEnergy = (Double)energyIterator.next();
			// For output, convert energy from CEU to kcal/mol
			thisEnergy = thisEnergy / AbstractEnergyTerm.CEU_CONVERSION_FACTOR;			potentialEnergy += thisEnergy;
		}
		
		return potentialEnergy;
	}

    /*====================================================================
        Private Data Members
    */
	private HashMap myOldAtomPositions;
    private double myStepSize;
    private double myConvergenceCriterion;
	private boolean myIsConverged;
    }

