package net.geoffrollins.oscar;
import com.nexes.wizard.*;

import javax.swing.*;
import com.birosoft.liquid.*;

public class Main {
    
    public Main() {
    }
    
    public static void main(String[] args) {
        
        try
        {
        	UIManager.setLookAndFeel("com.birosoft.liquid.LiquidLookAndFeel");
        }
        catch( Exception e )
        {
        	System.err.println( "Error initializing GUI" );
        	System.exit(0);
        }
        
        LiquidLookAndFeel.setLiquidDecorations(true);
        LiquidLookAndFeel.setStipples(false);
        LiquidLookAndFeel.setToolbarFlattedButtons(false);
        
        
        Wizard wizard = new Wizard();
        wizard.getDialog().setTitle("Oscar v1.4");
        
        WizardPanelDescriptor descriptor1 = new FilePanelDescriptor();
        wizard.registerWizardPanel(FilePanelDescriptor.IDENTIFIER, descriptor1);
	
        WizardPanelDescriptor descriptor2 = new AlgorithmPanelDescriptor();
        wizard.registerWizardPanel(AlgorithmPanelDescriptor.IDENTIFIER, descriptor2);

        WizardPanelDescriptor descriptor3 = new MDPanelDescriptor();
        wizard.registerWizardPanel(MDPanelDescriptor.IDENTIFIER, descriptor3);
        
        WizardPanelDescriptor descriptor4 = new TempProtocolPanelDescriptor();
        wizard.registerWizardPanel(TempProtocolPanelDescriptor.IDENTIFIER, descriptor4);
        
        WizardPanelDescriptor descriptor5 = new MinimizationPanelDescriptor();
        wizard.registerWizardPanel(MinimizationPanelDescriptor.IDENTIFIER, descriptor5);
        
        WizardPanelDescriptor descriptor6 = new MCPanelDescriptor();
        wizard.registerWizardPanel(MCPanelDescriptor.IDENTIFIER, descriptor6);
        
        WizardPanelDescriptor descriptor7 = new MCTempProtocolPanelDescriptor();
        wizard.registerWizardPanel(MCTempProtocolPanelDescriptor.IDENTIFIER, descriptor7);
        
        WizardPanelDescriptor descriptor8 = new SimulationPanelDescriptor();
        wizard.registerWizardPanel(SimulationPanelDescriptor.IDENTIFIER, descriptor8);  
        
        wizard.setCurrentPanel(FilePanelDescriptor.IDENTIFIER);
        
        int ret = wizard.showModalDialog();
        
        /* DEBUG
        System.out.println("Dialog return code is (0=Finish,1=Cancel,2=Error): " + ret);
        
        System.out.println("Second panel selection is: " + 
            (((AlgorithmPanel)descriptor2.getPanelComponent()).getAlgorithmSelected()));
        */
        
        System.exit(0);
        
    }
    
}
