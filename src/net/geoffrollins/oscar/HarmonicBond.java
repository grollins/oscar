/*****************************************************************************
    HarmonicBond

    Last Updated   - January 17, 2008
    Updated By     - Geoff Rollins

*****************************************************************************/

/*****************************************************************************
    Package and import statments
*/

package net.geoffrollins.oscar;


/*****************************************************************************
    Class Definitions
*/

/*============================================================================
    Harmonic oscillator approximation of a covalent bond
============================================================================*/
public class HarmonicBond 
extends AbstractEnergyTerm
{
	static final String IDENTIFIER = "Bond";

	/** 
    *	Default class constructor.
    */
    public HarmonicBond( )
        {
			setAtoms( new Atom[2] ); //allocates memory for the two atoms joined by this bond
			setForceConstant( 0.0 );
			setEquilibriumSeparation( 0.0 );
			setName( IDENTIFIER );
        }

	/** 
    *	Class constructor.
    */
    public HarmonicBond( Atom[] atoms, double forceConstant, double equilibriumLength  )
        {
        	setAtoms( atoms );
			setForceConstant( forceConstant * AbstractEnergyTerm.CEU_CONVERSION_FACTOR );
			setEquilibriumSeparation( equilibriumLength );
        	setName( IDENTIFIER );
        }

    
    /*====================================================================
        Public Member Functions
    */
    
    public double
    calculateDeltaX()
    {
    	Atom[] atoms = getAtoms();
    	return atoms[1].getX() - atoms[0].getX(); 
    }

	public double
	calculateDeltaY()
	{
		Atom[] atoms = getAtoms();
		return atoms[1].getY() - atoms[0].getY();
    }
    
    public double
    calculateDeltaZ()
    {
    	Atom[] atoms = getAtoms();
    	return atoms[1].getZ() - atoms[0].getZ();
    }
    
    /**-----------------------------------------------------------------
    *	Calculate the bond length (the distance between the two atoms connected by this bond)
	*
	*	@return			current bond length
	--------------------------------------------------------------------*/
    public double
    calculateSeparation()
    {
    	Atom[] atoms = getAtoms();
    	double dx = calculateDeltaX(); 
    	double dy = calculateDeltaY();
    	double dz = calculateDeltaZ();
    	return Math.sqrt( Math.pow( dx, 2 ) + Math.pow( dy, 2 ) + Math.pow( dz, 2 ) );
    }
    
	/**-----------------------------------------------------------------
    *	Calculate the energy of this bond
	*	E = 1/2 * forceConstant * (length - equilibriumLength)^2
	*
	*	@return			energy of this bond
	--------------------------------------------------------------------*/
    public double
    calculateEnergy( )
    {
    	double currentBondLength = calculateSeparation();
    	double idealBondLength = getEquilibriumSeparation();
  
    	return 0.5 * getForceConstant() * Math.pow( ( currentBondLength - idealBondLength ), 2 );
    }

	/**-----------------------------------------------------------------
    *	Calculate the force of this term based on the current
    *	positions of its constituent atoms. Add this force to
    *	the atoms' current force values.
	*
	*	F = -forceConstant * (length - equilibriumLength)
	--------------------------------------------------------------------*/
    public double
    calculateForce()
    {
    	double currentBondLength = calculateSeparation();
    	double idealBondLength = getEquilibriumSeparation();
    	double totalForce = -getForceConstant() * ( currentBondLength - idealBondLength );
    	
    	double dx = calculateDeltaX(); 
    	double dy = calculateDeltaY();
    	double dz = calculateDeltaZ();
    	
    	double xForce = totalForce * dx/currentBondLength; 
    	double yForce = totalForce * dy/currentBondLength;
    	double zForce = totalForce * dz/currentBondLength;
    	
    	double[] forceOnAtom0 = {-xForce, -yForce, -zForce};
    	double[] forceOnAtom1 = {xForce, yForce, zForce};
    	
    	Atom[] atoms = getAtoms();
    	atoms[0].addForce( forceOnAtom0 );
    	atoms[1].addForce( forceOnAtom1 );
    	
    	double virial = (dx * xForce) + (dy * yForce) + (dz * zForce);
    	
    	return virial;
    	
    }



	public String
    toString( )
    {
	    Atom[] atoms = getAtoms();
	    return atoms[0].getSerialNumber() + "\t" + atoms[1].getSerialNumber() + "\t" + getForceConstant()/AbstractEnergyTerm.CEU_CONVERSION_FACTOR + "\t" + getEquilibriumSeparation();
    }

	/**-----------------------------------------------------------------
	*	Accessors
	*--------------------------------------------------------------------*/
	
	public double
    getForceConstant( )
        {
			return myForceConstant;
        }

    public void
    setForceConstant( double newForceConstant )
        {
			myForceConstant = newForceConstant;
        }
        
	public double
    getEquilibriumSeparation( )
        {
			return myEquilibriumSeparation;
        }

    public void
    setEquilibriumSeparation( double newEquilibriumSeparation )
        {
			myEquilibriumSeparation = newEquilibriumSeparation;
        }     


    /*====================================================================
        Private Member Functions
    */

    /*====================================================================
        Private Data Members
    */
    private double myForceConstant;			// the stiffness of the bond
	private double myEquilibriumSeparation;	// the ideal bond length

    }

