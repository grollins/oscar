/*****************************************************************************
    HarmonicAngle

    Last Updated   - January 24, 2008
    Updated By     - Geoff Rollins

*****************************************************************************/

/*****************************************************************************
    Package and import statments
*/

package net.geoffrollins.oscar;

import java.io.*;


/*****************************************************************************
    Class Definitions
*/

/*============================================================================
    Harmonic oscillator approximation of the angle between two covalent bonds
============================================================================*/
public class HarmonicAngle 
extends AbstractEnergyTerm
{

	static final double RADIAN_CONVERSION_FACTOR = Math.PI / 180.0;
	static final String IDENTIFIER = "Angle";
	/** 
    *	Default class constructor.
    */
    public HarmonicAngle( )
        {
			setAtoms( new Atom[3] ); //allocates memory for the three atoms joined by this angle
			setForceConstant( 0.0 );
			setEquilibriumSeparation( 0.0 );
			setName( IDENTIFIER );
        }

	/** 
    *	Class constructor.
    */
    public HarmonicAngle( Atom[] atoms, double forceConstant, double equilibriumAngle  )
        {
        	setAtoms( atoms );
			setForceConstant( forceConstant * AbstractEnergyTerm.CEU_CONVERSION_FACTOR );
			setEquilibriumSeparation( equilibriumAngle );
        	setName( IDENTIFIER );
        	try
        	{
        		myLogWriter = new PrintWriter( new File( "angle.txt" ) );
        	}
        	catch( Exception e )
        	{
        		myLogWriter = null;
        	}
        	
        }

    
    /*====================================================================
        Public Member Functions
    */
    
    public double
    calculateDeltaX()
    {
    	Atom[] atoms = getAtoms();
    	return atoms[1].getX() - atoms[0].getX(); 
    }

	public double
	calculateDeltaY()
	{
		Atom[] atoms = getAtoms();
		return atoms[1].getY() - atoms[0].getY();
    }
    
    public double
    calculateDeltaZ()
    {
    	Atom[] atoms = getAtoms();
    	return atoms[1].getZ() - atoms[0].getZ();
    }
    
    public double
    calculateDeltaX2()
    {
    	Atom[] atoms = getAtoms();
    	return atoms[2].getX() - atoms[1].getX(); 
    }

	public double
	calculateDeltaY2()
	{
		Atom[] atoms = getAtoms();
		return atoms[2].getY() - atoms[1].getY();
    }
    
    public double
    calculateDeltaZ2()
    {
    	Atom[] atoms = getAtoms();
    	return atoms[2].getZ() - atoms[1].getZ();
    }
    
    /**-----------------------------------------------------------------
    *	Calculate the angle measure (the distance between the two atoms connected by this angle)
	*
	*	@return			current angle measure (radians)
	--------------------------------------------------------------------*/
    public double
    calculateSeparation( )
    {	
    	double dx1 = calculateDeltaX(); 
    	double dy1 = calculateDeltaY();
    	double dz1 = calculateDeltaZ();
    	double dx2 = calculateDeltaX2(); 
    	double dy2 = calculateDeltaY2();
    	double dz2 = calculateDeltaZ2();
    	
    	double dot11 = (dx1 * dx1) + (dy1 * dy1) + (dz1 * dz1);
    	double dot12 = (dx1 * dx2) + (dy1 * dy2) + (dz1 * dz2);
    	double dot22 = (dx2 * dx2) + (dy2 * dy2) + (dz2 * dz2);
    	
    	double cD = Math.sqrt( dot11 * dot22);
    	double c = dot12 / cD;
    	double angle = Math.acos( c );
    	
    	return (Math.PI - angle);
    }
    
	/**-----------------------------------------------------------------
    *	Calculate the energy of this angle
	*	E = 1/2 * forceConstant * (measure - equilibriumAngle)^2
	*
	*	@return			energy of this angle
	--------------------------------------------------------------------*/
    public double
    calculateEnergy( )
    {
    	double angle = calculateSeparation();
    	double equilAngle = getEquilibriumSeparation() * RADIAN_CONVERSION_FACTOR;
    	
    	return 0.5 * getForceConstant() * Math.pow( (angle - equilAngle), 2 );
    }

	/**-----------------------------------------------------------------
    *	Calculate the force of this term based on the current
    *	positions of its constituent atoms. Add this force to
    *	the atoms' current force values.
	*
	*	F = -forceConstant * (angle - equilibriumAngle)
	*	Atoms 0, 1, and 2. Atom 1 is the vertex.
	--------------------------------------------------------------------*/
    public double
    calculateForce()
    {
    	double dx1 = calculateDeltaX(); 
    	double dy1 = calculateDeltaY();
    	double dz1 = calculateDeltaZ();
    	double dx2 = calculateDeltaX2(); 
    	double dy2 = calculateDeltaY2();
    	double dz2 = calculateDeltaZ2();
    	
    	double dot11 = (dx1 * dx1) + (dy1 * dy1) + (dz1 * dz1);
    	double dot12 = (dx1 * dx2) + (dy1 * dy2) + (dz1 * dz2);
    	double dot22 = (dx2 * dx2) + (dy2 * dy2) + (dz2 * dz2);
    	
    	double cD = Math.sqrt( dot11 * dot22);
    	double c = dot12 / cD;
    	double angle = Math.acos( c );
    	double equilAngle = getEquilibriumSeparation() * RADIAN_CONVERSION_FACTOR;
    	
    	writeLog( String.valueOf((Math.PI - angle) / RADIAN_CONVERSION_FACTOR) );
    	
    	double force = -getForceConstant() * ((Math.PI - angle) - equilAngle); 
    	
    	double xForce1 = force * ((dot12/dot11 * dx1) - dx2) / cD;
    	double yForce1 = force * ((dot12/dot11 * dy1) - dy2) / cD;
    	double zForce1 = force * ((dot12/dot11 * dz1) - dz2) / cD;
    	
    	double xForce2 = force * ((-dot12/dot22 * dx2) + dx1) / cD;
    	double yForce2 = force * ((-dot12/dot22 * dy2) + dy1) / cD;
    	double zForce2 = force * ((-dot12/dot22 * dz2) + dz1) / cD;
    	
    	double xForceOnAtom1 = xForce1;
    	double yForceOnAtom1 = yForce1;
    	double zForceOnAtom1 = zForce1;
    	
    	double xForceOnAtom2 = -xForce1 - xForce2;
    	double yForceOnAtom2 = -yForce1 - yForce2;
    	double zForceOnAtom2 = -zForce1 - zForce2;
    	
    	double xForceOnAtom3 = xForce2;
    	double yForceOnAtom3 = yForce2;
    	double zForceOnAtom3 = zForce2;
    	
    	double[] forceOnAtom1 = {xForceOnAtom1, yForceOnAtom1, zForceOnAtom1};
    	double[] forceOnAtom2 = {xForceOnAtom2, yForceOnAtom2, zForceOnAtom2};
    	double[] forceOnAtom3 = {xForceOnAtom3, yForceOnAtom3, zForceOnAtom3};
    	
    	Atom[] atoms = getAtoms();
    	atoms[0].addForce( forceOnAtom1 );
    	atoms[1].addForce( forceOnAtom2 );
    	atoms[2].addForce( forceOnAtom3 );
    	
    	/*
    	System.out.print( "Atom1: " ); System.out.print( xForceOnAtom1 ); System.out.print( "\t" ); System.out.print( yForceOnAtom1 ); System.out.print( "\t" ); System.out.println( zForceOnAtom1 );
    	System.out.print( "Atom2: " ); System.out.print( xForceOnAtom2 ); System.out.print( "\t" ); System.out.print( yForceOnAtom2 ); System.out.print( "\t" ); System.out.println( zForceOnAtom2 );
    	System.out.print( "Atom3: " ); System.out.print( xForceOnAtom3 ); System.out.print( "\t" ); System.out.print( yForceOnAtom3 ); System.out.print( "\t" ); System.out.println( zForceOnAtom3 ); System.out.println( "" );
    	*/
    	
    	return 0.0;
    }



	public String
    toString( )
    {
	    Atom[] atoms = getAtoms();
	    return atoms[0].getSerialNumber() + "\t" + atoms[1].getSerialNumber() + "\t" + atoms[2].getSerialNumber() + "\t" +  getForceConstant()/AbstractEnergyTerm.CEU_CONVERSION_FACTOR + "\t" + getEquilibriumSeparation();
    }

	/**-----------------------------------------------------------------
	*	Accessors
	*--------------------------------------------------------------------*/
	
	public double
    getForceConstant( )
        {
			return myForceConstant;
        }

    public void
    setForceConstant( double newForceConstant )
        {
			myForceConstant = newForceConstant;
        }
        
	public double
    getEquilibriumSeparation( )
        {
			return myEquilibriumSeparation;
        }

    public void
    setEquilibriumSeparation( double newEquilibriumSeparation )
        {
			myEquilibriumSeparation = newEquilibriumSeparation;
        }     

	/*====================================================================
        Private Member Functions
    */
	private void 
	writeLog( String text )
	{
		// Stub function
		//myLogWriter.println( text );	
		;
	}

    /*====================================================================
        Private Data Members
    */
    private double myForceConstant;			// the stiffness of the angle, kcal/mol/rad^2
	private double myEquilibriumSeparation;	// the ideal angle measure, degrees
	private PrintWriter myLogWriter;
    }

