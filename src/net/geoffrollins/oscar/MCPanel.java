package net.geoffrollins.oscar;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;


public class MCPanel extends JPanel {
        
    public MCPanel() {
     
        super();
                
        contentPanel = getContentPanel();
        contentPanel.setBorder(new EmptyBorder(new Insets(10, 10, 10, 10)));
		contentPanel.setBackground( Color.decode("0xD0E6FF") );
		Font headerFont = new Font("Arial", Font.BOLD, 24);

        ImageIcon icon = getImageIcon();
        
        titlePanel = new javax.swing.JPanel();
        textLabel = new javax.swing.JLabel();
        iconLabel = new javax.swing.JLabel();
        separator = new javax.swing.JSeparator();

        setLayout(new java.awt.BorderLayout());


        titlePanel.setLayout(new java.awt.BorderLayout());
        titlePanel.setBackground(Color.decode("0x3C8CE4"));
        
        textLabel.setBackground(Color.decode("0x3C8CE4"));
        textLabel.setForeground(Color.white);
        textLabel.setFont(headerFont);
        textLabel.setText("MC Simulation Setup");
        textLabel.setBorder(new EmptyBorder(new Insets(10, 10, 10, 10)));
        textLabel.setOpaque(true);

        iconLabel.setBackground(Color.white);
        if (icon != null)
            iconLabel.setIcon(icon);
        
        titlePanel.add(textLabel, BorderLayout.CENTER);
        titlePanel.add(iconLabel, BorderLayout.EAST);
        titlePanel.add(separator, BorderLayout.SOUTH);
        
        add(titlePanel, BorderLayout.NORTH);
        JPanel secondaryPanel = new JPanel();
        secondaryPanel.add(contentPanel, BorderLayout.NORTH);
        secondaryPanel.setBackground(Color.decode("0xD0E6FF"));
        add(secondaryPanel, BorderLayout.CENTER);

    }  
    
    public String getMCMethodSelected() 
    {
        return (String)MethodComboBox.getSelectedItem();
    }
    
    public String getStepSizeText()
    {
    	return stepSizeTextField.getText();
    }
    
    public String getNumberStepsText()
    {
    	return numberStepsTextField.getText();
    }
    
    public String getTemperatureText()
    {
    	return tempTextField.getText();
    }
    
    public String getBoxLengthText()
    {
    	return boxLengthTextField.getText();
    }
    
    public String getNumDimensions()
    {
    	return (String)dimensionComboBox.getSelectedItem();
    }
    
    private JPanel getContentPanel() {     
        
        Font titleFont = new Font("Arial", Font.PLAIN, 18);
        Font labelFont = new Font("Arial", Font.BOLD, 16);
        Font labelFont2 = new Font("Arial", Font.PLAIN, 16);
        
        JPanel contentPanel1 = new JPanel();
        welcomeTitle = new JLabel();
        blankSpace = new JLabel();
		jPanel1 = new javax.swing.JPanel();

       	MethodLabel = new javax.swing.JLabel();
        stepSizeLabel = new javax.swing.JLabel();
        numberStepsLabel = new javax.swing.JLabel();
        stepSizeTextField = new javax.swing.JTextField();
        numberStepsTextField = new javax.swing.JTextField();
        stepSizeLabel2 = new javax.swing.JLabel();
        numberStepsLabel2 = new javax.swing.JLabel();
        tempLabel1 = new javax.swing.JLabel();
        tempTextField = new javax.swing.JTextField();
        tempLabel2 = new javax.swing.JLabel();
        MethodComboBox = new javax.swing.JComboBox();
        boxLengthLabel = new javax.swing.JLabel();
        boxLengthLabel2 = new javax.swing.JLabel();
        boxLengthTextField = new javax.swing.JTextField();
        dimensionComboBox = new javax.swing.JComboBox();
        dimensionLabel = new javax.swing.JLabel();

        MethodLabel.setFont(labelFont);
        MethodLabel.setText("MC Method");

        stepSizeLabel.setFont(labelFont);
        stepSizeLabel.setText("Initial Max Displacement");

        numberStepsLabel.setFont(labelFont);
        numberStepsLabel.setText("Number of Steps");

        stepSizeTextField.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        stepSizeTextField.setText("0.01");

        numberStepsTextField.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        numberStepsTextField.setText("100000");

        stepSizeLabel2.setText("(unitless)");

        numberStepsLabel2.setText("steps");

        tempLabel1.setFont(labelFont);
        tempLabel1.setText("Initial Temperature");

        tempTextField.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        tempTextField.setText("300.0");

        tempLabel2.setText("K");

        MethodComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Atomic" }));
        MethodComboBox.setSelectedIndex(0);

		boxLengthLabel.setFont(labelFont);
        boxLengthLabel.setText("Box Side Length");
        
        boxLengthLabel2.setText("angstroms");
        
        boxLengthTextField.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        boxLengthTextField.setText("0");
        
        boxLengthLabel2.setFont(labelFont2);
        boxLengthTextField.setFont(labelFont2);
        MethodComboBox.setFont(labelFont2);
        tempLabel2.setFont(labelFont2);
        tempTextField.setFont(labelFont2);
        stepSizeLabel2.setFont(labelFont2);
        numberStepsLabel2.setFont(labelFont2);
        numberStepsTextField.setFont(labelFont2);
        stepSizeTextField.setFont(labelFont2);
        
        dimensionComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "1", "2", "3" }));
        dimensionComboBox.setSelectedIndex(1);
        dimensionComboBox.setFont( labelFont2 );
        
        dimensionLabel.setFont( labelFont );
        dimensionLabel.setText( "Number of Dimensions" );

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(tempLabel1)
                    .add(MethodLabel)
                    .add(stepSizeLabel)
                    .add(numberStepsLabel)
                    .add(boxLengthLabel)
                    .add(dimensionLabel))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createSequentialGroup()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, numberStepsTextField)
                            .add(stepSizeTextField)
                            .add(tempTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 132, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(boxLengthTextField)
                            .add(dimensionComboBox))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(numberStepsLabel2)
                            .add(stepSizeLabel2)
                            .add(tempLabel2)
                            .add(boxLengthLabel2)))
                    .add(MethodComboBox, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 251, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(MethodLabel)
                    .add(MethodComboBox, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(stepSizeLabel)
                    .add(stepSizeLabel2)
                    .add(stepSizeTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(numberStepsLabel)
                    .add(numberStepsTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(numberStepsLabel2))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(tempLabel1)
                    .add(tempLabel2)
                    .add(tempTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
              	.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
              	.add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(boxLengthLabel)
                    .add(boxLengthTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(boxLengthLabel2))
               	.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
               	
             	.add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(dimensionLabel)
                    .add(dimensionComboBox, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(42, Short.MAX_VALUE))
        );
        
        contentPanel1.setLayout(new java.awt.BorderLayout());

        jPanel1.add(blankSpace);
		jPanel1.setBackground( Color.decode("0xD0E6FF") );

        contentPanel1.add(jPanel1, java.awt.BorderLayout.CENTER);
        contentPanel1.setBackground( Color.decode("0xD0E6FF") );
        
        return contentPanel1;
    }
    
    private ImageIcon getImageIcon() {
        
        //  Icon to be placed in the upper right corner.
        
        return null;
    }
    

	private JComboBox MethodComboBox;
    private JLabel MethodLabel;
    private JLabel tempLabel1;
    private JLabel tempLabel2;
    private JTextField tempTextField;
    private JLabel numberStepsLabel;
    private JLabel numberStepsLabel2;
    private JTextField numberStepsTextField;
    private JLabel stepSizeLabel;
    private JLabel stepSizeLabel2;
    private JTextField stepSizeTextField;
	private JLabel boxLengthLabel;
    private JLabel boxLengthLabel2;
    private JTextField boxLengthTextField;
    private javax.swing.JComboBox dimensionComboBox;
	private javax.swing.JLabel dimensionLabel;

    private JLabel welcomeTitle;
    private JPanel contentPanel;
    private JLabel iconLabel;
    private JSeparator separator;
    private JLabel textLabel;
    private JPanel titlePanel;  
    private JPanel jPanel1;
    private JLabel blankSpace;


}
