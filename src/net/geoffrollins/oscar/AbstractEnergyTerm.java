/*****************************************************************************
    AbstractEnergyTerm

    Last Updated   - January 15, 2008
    Updated By     - Geoff Rollins

*****************************************************************************/

/*****************************************************************************
    Package and import statments
*/

package net.geoffrollins.oscar;


/*****************************************************************************
    Class Definitions
*/

/*============================================================================
    Framework for potential energy terms
 ............................................................................

    Each energy term has
    1) A list of the atoms that it governs
    2) A method for calculating the distance or angle between its atoms
    3) A method for calculating the interaction energy
    4) A method for calculating the force on each of its atoms

============================================================================*/
public abstract class AbstractEnergyTerm 
{

	static final double CEU_CONVERSION_FACTOR = 418.0;

    /*====================================================================
        Public Member Functions
    */
	
	abstract public double calculateDeltaX(); 
    abstract public double calculateDeltaY();
   	abstract public double calculateDeltaZ();

	/**-----------------------------------------------------------------
    *	Calculate the separation between this energy term's constituent atoms
	*
	*	@return			current separation
	--------------------------------------------------------------------*/
    abstract public double
    calculateSeparation();

	/**-----------------------------------------------------------------
    *	Calculate the energy of this term based on the current
    *	positions of its constituent atoms
	*
	*	@return			energy
	--------------------------------------------------------------------*/
    abstract public double
    calculateEnergy();

	/**-----------------------------------------------------------------
    *	Calculate the force of this term based on the current
    *	positions of its constituent atoms. Add this force to
    *	the atoms' current force values. Return the value of
    *	the virial coefficient.
	*
	--------------------------------------------------------------------*/
    abstract public double
    calculateForce();


	/**-----------------------------------------------------------------
	*	Accessors
	*--------------------------------------------------------------------*/
	
    public Atom[]
    getAtoms( )
        {
			return myAtoms;
        }

	public void
    setAtoms( Atom[] newAtoms )
        {
			myAtoms = newAtoms;
        }

	public String
	getName( )
	{
		return myName;
	}
	
	public void
	setName( String newName )
	{
		myName = newName;
	}

    /*====================================================================
        Private Member Functions
    */
	

    /*====================================================================
        Private Data Members
    */

    private Atom[] myAtoms; 				// the atoms governed by this energy term
	private String myName;

    }

