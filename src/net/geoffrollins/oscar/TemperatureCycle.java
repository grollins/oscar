/*****************************************************************************
    TemperatureCycle.java

    Last Updated   - January 28, 2008
    Updated By     - Geoff Rollins

    A cycle in a temperature protocol knows 
    	1. ID Name 
    	2. the temperature control method it should use (rescale, reassign, bath)
    	3. thermalization interval (how many steps between rescaling or reassignment)
    	4. % of simulation (what fraction of the total simulation time should be devoted to this cycle)
    	5. initial temperature (at start of cycle)
    	6. target temperature (at end of cycle)
    
    Sample temperature protocol:
    	Cycle 1. Heat from 300K to 500K (the first 20% of simulation)
    	Cycle 2. Hold at 500K 			( the next 30% of simulation)
    	Cycle 3. Cool to 0K 			(the remaining 50% of simulation)

*****************************************************************************/

/*****************************************************************************
    Package and import statments
*/

package net.geoffrollins.oscar;


/*****************************************************************************
    Class Definitions
*/

/*============================================================================
    One-line summary of class goal
 ............................................................................

    Description of class

============================================================================*/
public class TemperatureCycle 
    {

    /*--------------------------------------------------------------------
        Constructors
    --------------------------------------------------------------------*/

    /** 
    *	Default class constructor.
    */
    public TemperatureCycle( )
        {
        	setID( 0 );
			setThermMethod( "Off" );
			setThermInterval( 0 );
			setPercentSim( 0 );
			setInitialTemp( 0.0 );
			setTargetTemp( 0.0 );
        	
        }

    /** 
    *	Class constructor.
    */
    public TemperatureCycle( int ID, String thermMethod, int thermInterval, int percentSim, double initTemp, double targetTemp )
        {
        	setID( ID );
			setThermMethod( thermMethod );
			setThermInterval( thermInterval );
			setPercentSim( percentSim );
			setInitialTemp( initTemp );
			setTargetTemp( targetTemp );
			        
		}


    /*====================================================================
        Public Member Functions
    */

	/*-----------------------------------------------------------------
	*	Accessors
	*--------------------------------------------------------------------*/
    public int
    getID( )
        {
			return myID;
        }

	public void
    setID( int newID )
        {
			myID = newID;
        }
    
    public String
    getThermMethod( )
        {
			return myThermMethod;
        }

	public void
    setThermMethod( String newThermMethod )
        {
			myThermMethod = newThermMethod;
        }

	public int
    getThermInterval( )
        {
			return myThermInterval;
        }

	public void
    setThermInterval( int newThermInterval )
        {
			myThermInterval = newThermInterval;
        }

	public int
    getPercentSim( )
        {
			return myPercentSim;
        }

	public void
    setPercentSim( int newPercentSim )
        {
			myPercentSim = newPercentSim;
        }
    
    public double
    getInitialTemp( )
        {
			return myInitialTemp;
        }

	public void
    setInitialTemp( double newInitialTemp )
        {
			myInitialTemp = newInitialTemp;
        } 
        
    public double
    getTargetTemp( )
        {
			return myTargetTemp;
        }

	public void
    setTargetTemp( double newTargetTemp )
        {
			myTargetTemp = newTargetTemp;
        }  
        
	public String
	toString( )
	{
		return String.valueOf( getID() ) + "\t" + getThermMethod() + "\t" + String.valueOf( getThermInterval() ) + "\t" + String.valueOf( getPercentSim() ) + "\t" + String.valueOf( getInitialTemp() ) + "\t" + String.valueOf( getTargetTemp() ) + "\n";
	}
        
    /*====================================================================
        Private Member Functions
    */


    /*====================================================================
        Private Data Members
    */

	private int myID;
    private String myThermMethod;
    private int	myThermInterval;
    private int myPercentSim;
    private double myInitialTemp;    
    private double myTargetTemp;

    }

