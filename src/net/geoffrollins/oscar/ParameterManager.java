/*****************************************************************************
    ParameterManager.java

    Last Updated   - March 11, 2008
    Updated By     - Geoff Rollins

    A holding place for Oscar simulation parameters that have been entered
    into the Oscar wizard. When the user starts the simulation, an Oscar
    simulation is built and executed based on the parameters stored here.

*****************************************************************************/

/*****************************************************************************
    Package and import statments
*/

package net.geoffrollins.oscar;

import java.util.LinkedList;

/*****************************************************************************
    Class Definitions
*/

/*============================================================================
    One-line summary of class goal
 ............................................................................

    Description of class

============================================================================*/
public class ParameterManager 
{

    /*--------------------------------------------------------------------
        Constructors
    --------------------------------------------------------------------*/

    /** 
    *	Default class constructor.
    */
    public ParameterManager( )
    {
		 temperatureProtocol = new TemperatureProtocol();
		 numDimensions = 2;
	}

    /*====================================================================
        Public Member Functions
    */

	public void
	addCycle( String ID, String thermMethod, String thermInterval, String percentSim, String initialTemp, String targetTemp )
	{
		getTemperatureProtocol().saveCycle( Integer.valueOf(ID), thermMethod, Integer.valueOf( thermInterval), Integer.valueOf( percentSim ), Double.valueOf( initialTemp ), Double.valueOf( targetTemp ) );
	}

	/*-----------------------------------------------------------------
	*	Accessors
	*--------------------------------------------------------------------*/
    public String
    getCoordinateFileName( )
        {
			return coordinateFileName;
        }

	public void
    setCoordinateFileName( String newCoordinateFileName )
        {
			coordinateFileName = newCoordinateFileName;
        }

	public String
    getStructureFileName( )
        {
			return structureFileName;
        }

	public void
    setStructureFileName( String newStructureFileName )
        {
			structureFileName = newStructureFileName;
        }
        
	public String
    getOutputFolderName( )
        {
			return outputFolderName;
        }

	public void
    setOutputFolderName( String newOutputFolderName )
        {
			outputFolderName = newOutputFolderName;
        }
        
	public String
    getJobName( )
        {
			return jobName;
        }

	public void
    setJobName( String newJobName )
        {
			jobName = newJobName;
        }

	public int
    getOutputInterval( )
        {
			return outputInterval;
        }

	public void
    setOutputInterval( int newOutputInterval )
        {
			outputInterval = newOutputInterval;
        }
       
    public boolean
    getPrintRawCoords( )
        {
			return printRawCoords;
        }

	public void
    setPrintRawCoords( boolean newPrintRawCoords )
        {
			printRawCoords = newPrintRawCoords;
        }
       
    public String
    getAlgorithm( )
        {
			return algorithm;
        }

	public void
    setAlgorithm( String newAlgorithm )
        {
			algorithm = newAlgorithm;
        }
        
	public String
    getIntegrationMethod( )
        {
			return integrationMethod;
        }

	public void
    setIntegrationMethod( String newIntegrationMethod )
        {
			integrationMethod = newIntegrationMethod;
        }        
    
    public double
    getTimeStep( )
        {
			return timeStep;
        }

	public void
    setTimeStep( double newTimeStep )
        {
			timeStep = newTimeStep;
        }
        
	public int
    getNumSteps( )
        {
			return numSteps;
        }

	public void
    setNumSteps( int newNumSteps )
        {
			numSteps = newNumSteps;
        }
        
    public double
    getTemperature( )
        {
			return temperature;
        }

	public void
    setTemperature( double newTemperature )
        {
			temperature = newTemperature;
        }
        
    public double
    getBoxLength( )
        {
			return boxLength;
        }

	public void
    setBoxLength( double newBoxLength )
        {
			boxLength = newBoxLength;
        }

	public TemperatureProtocol
    getTemperatureProtocol( )
        {
			return temperatureProtocol;
        }

	public void
    setTemperatureProtocol( TemperatureProtocol newTemperatureProtocol )
        {
			temperatureProtocol = newTemperatureProtocol;
        }

	public int
    getNumDimensions( )
        {
			return numDimensions;
        }

	public void
    setNumDimensions( int newNumDimensions )
        {
			numDimensions = newNumDimensions;
        }

	public String
    getMinimizationMethod( )
        {
			return minimizationMethod;
        }

	public void
    setMinimizationMethod( String newMinimizationMethod )
        {
			minimizationMethod = newMinimizationMethod;
        }
        
	public double
    getStepSize( )
        {
			return stepSize;
        }

	public void
    setStepSize( double newStepSize )
        {
			stepSize = newStepSize;
        }
        
    public double
    getConvCrit( )
        {
			return convCrit;
        }

	public void
    setConvCrit( double newConvCrit )
        {
			convCrit = newConvCrit;
        }
        
    public String
    getMCMethod( )
        {
			return mcMethod;
        }

	public void
    setMCMethod( String newMCMethod )
        {
			mcMethod = newMCMethod;
        }

	public double
    getMaxDisplacement( )
        {
			return maxDisplacement;
        }

	public void
    setMaxDisplacement( double newMaxDisplacement )
        {
			maxDisplacement = newMaxDisplacement;
        }

    /*====================================================================
        Private Member Functions
    */


    /*====================================================================
        Private Data Members
    */

    // FilePanel variables
	private String coordinateFileName;
	private String structureFileName;
	private String outputFolderName;
	private String jobName;
	private int outputInterval;
	private	boolean	printRawCoords;
	
	// AlgorithmPanel variable
	private String algorithm;
	
	// MD variables
	private String integrationMethod;
	private double timeStep;
	private int numSteps;
	private double temperature;
	private double boxLength;
    private TemperatureProtocol temperatureProtocol;
    private int numDimensions;
    
    // Minimization variables
    private String minimizationMethod;
    private double stepSize;
    private double convCrit;
    
    // MC variables
    private String mcMethod;
    private double maxDisplacement;
    
}

