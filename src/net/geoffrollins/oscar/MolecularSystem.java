/*****************************************************************************
    MolecularSystem.java

    Last Updated   - January 16, 2008
    Updated By     - Geoff Rollins

    All of the atoms in an Oscar simulation.

*****************************************************************************/

/*****************************************************************************
    Package and import statments
*/

package net.geoffrollins.oscar;
import java.util.LinkedList;
import java.util.ListIterator;
import com.braju.format.*;


/*****************************************************************************
    Class Definitions
*/

/*============================================================================
    One-line summary of class goal
 ............................................................................

    Description of class

============================================================================*/
public class MolecularSystem
{

    /*--------------------------------------------------------------------
        Constructors
    --------------------------------------------------------------------*/

    /** 
    *	Default class constructor.
    */
    public MolecularSystem( )
    {
		setAtoms( new LinkedList() );		
	}


    /*====================================================================
        Public Member Functions
    */

	/** 
    *	Add an atom to the system
    *
    *	@param	newAtom
    */
    public void
    addAtom( Atom newAtom )
    {
    	getAtoms().add( newAtom );
    }

	/** 
    *	Retrieve an atom from the system.
    *
    *	@param	serialNum
    *	@return The desired atom or null if no match is found.
    */
	public Atom
	getAtom( int serialNum )
	{
		Atom atom = null;
		ListIterator atomList = iterateAtoms();
		while( atomList.hasNext() )
		{
			Atom thisAtom = (Atom)atomList.next();
			if( thisAtom.getSerialNumber() == serialNum )
			{
				atom = thisAtom;
			}
			
		}
		
		return atom;
	}

	/** 
    *	Print a list of the atoms in the system
    *
    */
    public String
    toString( )
    {
    	String molecularSystemString = "";
    	ListIterator atomList = iterateAtoms();
    	while ( atomList.hasNext() )
    	{
    		molecularSystemString += atomList.next().toString();
    	}
    	
    	return molecularSystemString;
    }
    
    /*
    *	Make a string of the current xyz coordinates of every atom in the
    *	system.
    */
    public String
	getCoordString( int stepNumber )
	{	
		String rawCoordString = "%8.3d     ";
		Parameters coordStringParameters = new Parameters( stepNumber );
		
		ListIterator atomList = iterateAtoms();
		while( atomList.hasNext() )
		{
			Atom thisAtom = (Atom)atomList.next();
			
			rawCoordString += "%10.6f     ";
			coordStringParameters = coordStringParameters.add( thisAtom.getX());
			rawCoordString += "%10.6f     ";
			coordStringParameters = coordStringParameters.add( thisAtom.getY());
			rawCoordString += "%10.6f     ";
			coordStringParameters = coordStringParameters.add( thisAtom.getZ());
			
		}
		
		rawCoordString = rawCoordString.concat( "\n" );
		
		return Format.sprintf( rawCoordString, coordStringParameters );
	}
    
    public String
	getCoordBanner( )
	{
		String bannerString = "%8s     ";
		Parameters bannerStringParameters = new Parameters( "Step" );
		
		for( int i = 0; i < numAtoms(); i++ )
		{
			bannerString += "%10s     ";
			bannerStringParameters = bannerStringParameters.add( "x" + String.valueOf(i) );
			bannerString += "%10s     ";
			bannerStringParameters = bannerStringParameters.add( "y" + String.valueOf(i) );
			bannerString += "%10s     ";
			bannerStringParameters = bannerStringParameters.add( "z" + String.valueOf(i) );
		}
		
		bannerString += "\n";
		
		return Format.sprintf( bannerString, bannerStringParameters );
	}
    
    /** 
    *	Generate an iterator for the list of atoms in the system.
    *
    */
    public ListIterator
    iterateAtoms( )
    {
    	return getAtoms().listIterator( 0 );
    }
    
    public int
    numAtoms( )
    {
    	return getAtoms().size();
    }

	/*-----------------------------------------------------------------
	*	Accessors
	*--------------------------------------------------------------------*/
    public LinkedList
    getAtoms( )
      	{
			return myAtoms;
        }

	public void
    setAtoms( LinkedList newAtoms )
        {
			myAtoms = newAtoms;
        }


    /*====================================================================
        Private Member Functions
    */


    /*====================================================================
        Private Data Members
    */

    private LinkedList myAtoms;

    }

