/*****************************************************************************
    TemperatureProtocol.java

    Last Updated   - January 28, 2008
    Updated By     - Geoff Rollins

    A set of temperature cycles that are used to control the temperature
    during an MD simulation

*****************************************************************************/

/*****************************************************************************
    Package and import statments
*/

package net.geoffrollins.oscar;

import java.util.LinkedList;
import java.util.ListIterator;


/*****************************************************************************
    Class Definitions
*/

/*============================================================================
    One-line summary of class goal
 ............................................................................

    Description of class

============================================================================*/
public class TemperatureProtocol 
	{

    /*--------------------------------------------------------------------
        Constructors
    --------------------------------------------------------------------*/

    /** 
    *	Default class constructor.
    */
    public TemperatureProtocol( )
        {
			myCycles = new LinkedList();
        }
        
   	public TemperatureProtocol( int totalNumSteps )
        {
			myCycles = new LinkedList();
			setTotalNumSteps( totalNumSteps );
        }


    /*====================================================================
        Public Member Functions
    */

	/**
	*	Creates a new temperature cycle based on the given parameters and
	*	stores it at position <ID>.
	*
	*/
	public void
	saveCycle( int ID, String thermMethod, int thermInterval, int percentSim,  double initTemp, double targetTemp )
	{
		TemperatureCycle newCycle = new TemperatureCycle( ID, thermMethod, thermInterval, percentSim, initTemp, targetTemp );
		
		LinkedList cycleList = getCycles();
		
		try
		{
			cycleList.set( ID, newCycle );
		}
		catch( IndexOutOfBoundsException e)
		{
			cycleList.add( newCycle );
		}
		
		/* DEBUG
		System.out.print( "Save cycle: " );
		System.out.print( ID );
		System.out.print( " " );
		System.out.print( thermMethod );
		System.out.print( " " );
		System.out.print( thermInterval ); 
		System.out.print( " " );
		System.out.print( percentSim );
		System.out.print( " " );
		System.out.print( initTemp );
		System.out.print( " " );
		System.out.println( targetTemp );
		*/
	}

	/** 
    *	Determines whether the given step is a step during which a
    *	a temperature correction (rescaling or reassignment) should be carried out
    *
    *	@param	thisStep
    *	@return true if a temperature correction should be carried out
    */
    public boolean
    isThermStep( int currentStep )
    {
    	TemperatureCycle currentCycle = getCurrentCycle( currentStep );
    	if( currentCycle == null )
    	{
    		return false;
    	}
    	else if( currentStep % currentCycle.getThermInterval() == 0 )
    	{
    		return true;
    	}
    	else
    	{
    		return false;
    	}
    }
   
   	public String
	getCurrentThermMethod( int currentStep )
	throws NullPointerException
    {
    	TemperatureCycle thisCycle = getCurrentCycle( currentStep );
    	if( thisCycle == null )
    	{
    		// no temperature cycle defined for currentStep
    		throw new NullPointerException();
    	}
    	else
    	{
    		return thisCycle.getThermMethod();
    	}
	}
	
	public double
	getCurrentTargetTemp( int currentStep )
	throws NullPointerException
	{
		TemperatureCycle thisCycle = getCurrentCycle( currentStep );
		
		if( thisCycle == null )
		{
			// no temperature cycle defined for currentStep
			throw new NullPointerException();
		}
		
		// we started the cycle at this temp
		double initialTemp = thisCycle.getInitialTemp();
		// we want to be at this temp by the end of the cycle
		double targetTemp = thisCycle.getTargetTemp();
		
		/* In order to get from the initial temp to the target temp by the end of the
		 	cycle, we will linearly interpolate between the two temps. At each therm.
		 	interval, we will change the temperature by some amount. That amount is
		 	equal to 
		 		(target - initial) / number of therm intervals
		*/
		
		double cycleSteps = thisCycle.getPercentSim() / 100.0 * getTotalNumSteps();
		double numThermEvents = cycleSteps / thisCycle.getThermInterval();
		double thermStep = (targetTemp - initialTemp) / numThermEvents;
		
		double firstCycleStep = getFirstCycleStep( currentStep );
		double currentCycleStep = ( currentStep - firstCycleStep ) / thisCycle.getThermInterval();
		
		return initialTemp + (thermStep * currentCycleStep);
	}
	
	public TemperatureCycle
	getCurrentCycle( int currentStep )
	{
		double percentComplete = Double.valueOf( currentStep ) / getTotalNumSteps() * 100.0;
		double totalPercent = 0.0;
		ListIterator cycleIterator = iterateCycles();
		TemperatureCycle thisCycle = null;
		TemperatureCycle currentCycle = null;
		
		while( cycleIterator.hasNext() )
		{
			thisCycle = (TemperatureCycle)cycleIterator.next();
			totalPercent += thisCycle.getPercentSim();
			
			// if the current step is part of this cycle's fraction of the simulation time...
			if( percentComplete <= totalPercent )
			{
				//...then this cycle is the current cycle
				currentCycle = thisCycle;
				break;
			}
			
		}
		
		/*
		if( currentCycle == null )
    	{
    		System.out.print( "Failed to find a valid temperature cycle for step " );
    		System.out.println( currentStep );
    	}
		*/
		
		return currentCycle;
	}

	/*-----------------------------------------------------------------
	*	Accessors
	*--------------------------------------------------------------------*/
    public LinkedList
    getCycles( )
        {
			return myCycles;
        }

	public void
    setCycles( LinkedList newCycles )
        {
			myCycles = newCycles;
        }


	public int
    getTotalNumSteps( )
        {
			return myTotalNumSteps;
        }

	public void
    setTotalNumSteps( int newTotalNumSteps )
        {
			myTotalNumSteps = newTotalNumSteps;
        }

    /*====================================================================
        Private Member Functions
    */
    private ListIterator
    iterateCycles( )
    {
    	return getCycles().listIterator();
    }

	private double    
    getFirstCycleStep( int currentStep )
    {
    	double percentComplete = Double.valueOf( currentStep ) / getTotalNumSteps() * 100.0;
    	ListIterator cycleIterator = iterateCycles();
		TemperatureCycle thisCycle = null;
		TemperatureCycle currentCycle = null;
		int totalPercent = 0;
		
		while( cycleIterator.hasNext() )
		{
			thisCycle = (TemperatureCycle)cycleIterator.next();
			totalPercent += thisCycle.getPercentSim();
			
			// if the current step is part of this cycle's fraction of the simulation time...
			if( percentComplete <= totalPercent )
			{
				//...then this cycle is the current cycle
				currentCycle = thisCycle;
				totalPercent -= thisCycle.getPercentSim();
				break;
			}
			
		}
    	
    	return totalPercent / 100.0 * getTotalNumSteps();
    }

	public String
	toString( )
	{
		ListIterator cycleIterator = iterateCycles();
		String tempProtocolString = "";
		
		while( cycleIterator.hasNext() )
		{
			TemperatureCycle thisCycle = (TemperatureCycle)cycleIterator.next();
			tempProtocolString = tempProtocolString.concat( thisCycle.toString() );
		}
		
		tempProtocolString = tempProtocolString.concat( "\n\nTotal Steps = " );
		tempProtocolString = tempProtocolString.concat( String.valueOf(getTotalNumSteps()) );
		
		return tempProtocolString;
	}

    /*====================================================================
        Private Data Members
    */

    private LinkedList myCycles;
    private int myTotalNumSteps;

    }

