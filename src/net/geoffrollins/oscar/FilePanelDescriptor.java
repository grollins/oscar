package net.geoffrollins.oscar;

import com.nexes.wizard.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;


public class FilePanelDescriptor 
extends WizardPanelDescriptor
{
    
    public static final String IDENTIFIER = "FILE_SELECTION_PANEL";
    
    FilePanel filePanel;
    
    public FilePanelDescriptor() 
    {    
        filePanel = new FilePanel();
        setPanelDescriptorIdentifier(IDENTIFIER);
        setPanelComponent(filePanel);
        
    }

    public Object getNextPanelDescriptor() {
        return AlgorithmPanelDescriptor.IDENTIFIER;
    }
    
    public Object getBackPanelDescriptor() {
        return null;
    }    
    
    public void aboutToHidePanel() 
    {
    	// save file names to parameter manager
    	ParameterManager parameterManager = getWizard().getParameterManager();
    	parameterManager.setCoordinateFileName( filePanel.getCoordText() );
    	parameterManager.setStructureFileName( filePanel.getStructureText() );
    	parameterManager.setJobName( filePanel.getJobNameText() );
    	parameterManager.setOutputInterval( Integer.valueOf(filePanel.getOutputIntervalText()) );
    	parameterManager.setPrintRawCoords( filePanel.getRawCoordValue() );
    	
    	String outputFolder = filePanel.getOutputFolderText();
    	// The folder name must end with a slash. The platform determines whether a forward or back slash is required.
    	String os_name = System.getProperty("os.name");
    	if( os_name.startsWith("Windows") )
    	{
    		if( outputFolder.charAt(outputFolder.length() - 1) != '\\' )
    		{
    			outputFolder = outputFolder.concat( "\\" );
    		}
    	
    	}
    	else
    	{
    		if( outputFolder.charAt(outputFolder.length() - 1) != '/' )
    		{
    			outputFolder = outputFolder.concat( "/" );
    		}
    	}
    	
    	System.out.println( outputFolder );
    	parameterManager.setOutputFolderName( outputFolder );
    	
    }   
            
}
