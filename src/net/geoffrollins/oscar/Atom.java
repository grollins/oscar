/*****************************************************************************
    atom.java

    Last Updated   - January 17, 2008
    Updated By     - Geoff Rollins

    Atom object for Oscar


*****************************************************************************/

/*****************************************************************************
    Package and import statments
*/

package net.geoffrollins.oscar;

import com.braju.format.*;


/*****************************************************************************
    Class Definitions
*/

/*============================================================================
    Atom data type
 ............................................................................

    An atom knows the following things about itself (in the order they appear in a pdb file):
   		1. Serial number
    	2. Name
    	3. The name of the residue to which it belongs
    	4. The ID of the chain to which it belongs
    	5. The sequence number of its residues
    	6. X coordinate
    	7. Y coordinate
    	8. Z coordinate
    	9. Occupancy
    	10. Temperature factor (beta)
    	11. Mass
    	12. Size (atomic radius)
    	13. Charge

============================================================================*/
public class Atom 
    {

    /*--------------------------------------------------------------------
        Constructors
     ...................................................................

       The default atom is lennard-jonesium. Alternatively, a neutral or 
       charged atom of known position, size, and mass can be created. 

    --------------------------------------------------------------------*/

    /** 
    *	Class constructor that makes a default atom of lennard-jonesium
    */
    public Atom( )
        {
			setSerialNumber( -1 );
			setName( "LJ" );
			setResidueName( "A" );
			setChainID( 'A' );
			setResidueSeqNum( 1 );
			setOccupancy( 1.00 );
			setBeta( 1.00 );
			setRadius( 1.75 ); //angstroms
			setMass( 20.0 ); 	//atomic mass units
			setCharge( 0.00 ); //elementary charge (charge of a proton)
			
			myPosition = new double[3];
			double[] position = { 0.00, 0.00, 0.00 };
			setPosition( position );
			myVelocity = new double[3];
			myAcceleration = new double[3];
			double[] velocity = { 0.00, 0.00, 0.00 };
			double[] acceleration = { 0.00, 0.00, 0.00 };
			setVelocity( velocity );
			setAcceleration( acceleration );
		
			myForce = new double[3];
			zeroForce( );
        }
    
	/** 
    *	Class constructor that makes a neutral atom with the specified coords size, and mass.
    */
    public Atom( int serial, String name, double x, double y, double z, double radius, double mass )
        {
        	setSerialNumber( serial );
        	setName( name );
        	setResidueName( "A" );
			setChainID( 'A' );
			setResidueSeqNum( 1 );
			setOccupancy( 1.00 );
			setBeta( 1.00 );
			setRadius( radius );
			setMass( mass );
			setCharge( 0.00 );
			
			myPosition = new double[3];
			double[] position = { x, y, z };
			setPosition( position );
			myVelocity = new double[3];
			myAcceleration = new double[3];
			double[] velocity = { 0.00, 0.00, 0.00 };
			double[] acceleration = { 0.00, 0.00, 0.00 };
			setVelocity( velocity );
			setAcceleration( acceleration );
			
			myForce = new double[3];
			zeroForce( );
        }
    
    /** 
    *	Class constructor that makes a charged atom with the specified coords size, mass, and charge.
    */
    public Atom( int serial, String name, double x, double y, double z, double radius, double mass, double charge )
        {
        	setSerialNumber( serial );
        	setName( name );
        	setResidueName( "A" );
			setChainID( 'A' );
			setResidueSeqNum( 1 );
			setOccupancy( 1.00 );
			setBeta( 1.00 );
			setRadius( radius );
			setMass( mass );
			setCharge( charge );
			
			myPosition = new double[3];
			double[] position = { x, y, z };
			setPosition( position );
			myVelocity = new double[3];
			myAcceleration = new double[3];
			double[] velocity = { 0.00, 0.00, 0.00 };
			double[] acceleration = { 0.00, 0.00, 0.00 };
			setVelocity( velocity );
			setAcceleration( acceleration );
			
			myForce = new double[3];
			zeroForce( );
        }

	/** 
    *	Class constructor that makes an atom with all parameters specified.
    */
    public Atom( String[] atomParameters )
        {
        	setSerialNumber( Integer.valueOf(atomParameters[0]) );
        	setName( atomParameters[1] );
        	setResidueName( atomParameters[2] );
			setChainID( atomParameters[3].charAt(0) );
			setResidueSeqNum( Integer.valueOf(atomParameters[4]) );
			setOccupancy( Double.valueOf(atomParameters[8]) );
			setBeta( Double.valueOf(atomParameters[9]) );
			setMass( Double.valueOf(atomParameters[10]) );
			setRadius( Double.valueOf(atomParameters[11]) );
			setCharge( Double.valueOf(atomParameters[12]) );
			
			myPosition = new double[3];
			double[] position = { Double.valueOf(atomParameters[5]), Double.valueOf(atomParameters[6]), Double.valueOf(atomParameters[7]) };
			setPosition( position );
			myVelocity = new double[3];
			myAcceleration = new double[3];
			double[] velocity = { 0.00, 0.00, 0.00 };
			double[] acceleration = { 0.00, 0.00, 0.00 };
			setVelocity( velocity );
			setAcceleration( acceleration );
			
			myForce = new double[3];
			zeroForce( );
        }

    /*====================================================================
        Public Member Functions
    */


	/**-----------------------------------------------------------------
    *	Resets the force on this atom to zero.
	*
	--------------------------------------------------------------------*/
	public void
	zeroForce( )
	{
		double[] force = { 0.00, 0.00, 0.00 };
		setForce( force );
	}

	/**-----------------------------------------------------------------
    *	Adds to the existing force exerted on this atom.
	*
	*	@param	newForce
	--------------------------------------------------------------------*/
	public void
	addForce( double[] newForce )
	{
		
		myForce[0] += newForce[0];
		myForce[1] += newForce[1];
		myForce[2] += newForce[2];
		
	}
	
	/**-----------------------------------------------------------------
    *	Calculate this atom's acceleration based on the force exerted on it.
	*	F = ma
	--------------------------------------------------------------------*/
	public void
	calculateAcceleration( )
	{
		double[] force = getForce();
		double mass = getMass();
		double[] acceleration = { force[0]/mass, force[1]/mass, force[2]/mass };
		
		/*
		System.out.print( "Acceleration = " );
		System.out.print( acceleration[0] );
		System.out.print( ", " );
		System.out.print( acceleration[1] );
		System.out.print( ", " );
		System.out.println( acceleration[2] );
		*/
		
		setAcceleration( acceleration );
	}
	
	/**-----------------------------------------------------------------
    *	Converts this atom into a printable string.
	*
	*	Note: I'm putting charge in the occupancy column because coloring 
	*	by charge in VMD does not seem to work, whereas coloring by occupancy 
	*	always works.
	*
	*	@return			stringized version of this atom
	--------------------------------------------------------------------*/
    public String
    toString( )
        {
        	Parameters p = new Parameters( );
        	return Format.sprintf( "ATOM  %5d %4s %3s %1c%4d    %8.3f%8.3f%8.3f%6.2f%6.2f%6.1f%6.2f%6.2f \n", 
        							p.add(getSerialNumber()).add(getName()).add(getResidueName()).
        							add(getChainID()).add(getResidueSeqNum()).add(getX()).add(getY()).
        							add(getZ()).add(getCharge()).add(getBeta()).add(getMass()).
        							add(getRadius()).add(getCharge()) 
        							); 
        }

    /**-----------------------------------------------------------------
    *	Gets the name of this atom.
	*
	*	@return			name of this atom
	--------------------------------------------------------------------*/
    public String
    getName( )
        {
			return myName;
        }


	public double[]
    getPosition( )
        {
			return myPosition;
        }

	
	/**-----------------------------------------------------------------
    *	Gets the x coordinate of this atom in angstroms.
	*
	*	@return			x coordinate of this atom
	--------------------------------------------------------------------*/
    public double
    getX( )
        {
			return myPosition[0];
        }

	/**-----------------------------------------------------------------
    *	Gets the y coordinate of this atom in angstroms.
	*
	*	@return			y coordinate of this atom
	--------------------------------------------------------------------*/
    public double
    getY( )
        {
			return myPosition[1];
        }
        
	/**-----------------------------------------------------------------
    *	Gets the z coordinate of this atom in angstroms.
	*
	*	@return			z coordinate of this atom
	--------------------------------------------------------------------*/
    public double
    getZ( )
        {
			return myPosition[2];
        }

	/**-----------------------------------------------------------------
    *	Gets the radius of this atom in angstroms.
	*
	*	@return			radius of this atom
	--------------------------------------------------------------------*/
    public double
    getRadius( )
        {
			return myRadius;
        }

	/**-----------------------------------------------------------------
    *	Gets the mass this atom in AMU.
	*
	*	@return			mass of this atom
	--------------------------------------------------------------------*/
    public double
    getMass( )
        {
			return myMass;
        }

	/**-----------------------------------------------------------------
    *	Gets the charge of this atom in elementary charge units.
	*
	*	@return			charge on this atom
	--------------------------------------------------------------------*/
    public double
    getCharge( )
        {
			return myCharge;
        }

	public int
    getSerialNumber( )
        {
			return mySerialNumber;
        }

	public String
    getResidueName( )
        {
			return myResidueName;
        }
        
   	public char
    getChainID( )
        {
			return myChainID;
        }

	public int
    getResidueSeqNum( )
        {
			return myResidueSeqNum;
        }
    
    public double
    getOccupancy( )
        {
			return myOccupancy;
        }
        
    public double
    getBeta( )
        {
			return myBeta;
        }
        
	public double[]
    getVelocity( )
        {
			return myVelocity;
        }
	
	public double[]
    getAcceleration( )
        {
			return myAcceleration;
        }
        
	public double[]
	getForce()
	{
		return myForce;
	}
	
	/**-----------------------------------------------------------------
    *	Sets the name of this atom.
	*
	*	@param	newName		new name for this atom
	--------------------------------------------------------------------*/
    public void
    setName( String newName )
        {
			myName = newName;
        }

	public void
    setPosition( double[] newPosition )
        {
			myPosition[0] = newPosition[0];
			myPosition[1] = newPosition[1];
			myPosition[2] = newPosition[2];
        }
	
	/**-----------------------------------------------------------------
    *	Sets the x coordinate of this atom.
	*
	*	@param	newX		new x coordinate for this atom
	--------------------------------------------------------------------*/
    public void
    setX( double newX )
        {
			myPosition[0] = newX;
        }

	/**-----------------------------------------------------------------
    *	Sets the y coordinate of this atom.
	*
	*	@param	newY		new y coordinate for this atom
	--------------------------------------------------------------------*/
    public void
    setY( double newY )
        {
			myPosition[1] = newY;
        }
        
    /**-----------------------------------------------------------------
    *	Sets the z coordinate of this atom.
	*
	*	@param	newZ		new z coordinate for this atom
	--------------------------------------------------------------------*/
    public void
    setZ( double newZ )
        {
			myPosition[2] = newZ;
        }

	/**-----------------------------------------------------------------
    *	Sets the radius of this atom.
	*
	*	@param	newRadius		new radius for this atom
	--------------------------------------------------------------------*/
    public void
    setRadius( double newRadius )
        {
			myRadius = newRadius;
        }        
      
	/**-----------------------------------------------------------------
    *	Sets the mass of this atom.
	*
	*	@param	newMass		new mass for this atom
	--------------------------------------------------------------------*/
    public void
    setMass( double newMass )
        {
			myMass = newMass;
        }        
    
    /**-----------------------------------------------------------------
    *	Sets the charge of this atom.
	*
	*	@param	newCharge		new charge for this atom
	--------------------------------------------------------------------*/
    public void
    setCharge( double newCharge )
        {
			myCharge = newCharge;
        }        
    
    public void
    setSerialNumber( int newSerialNumber )
        {
			mySerialNumber = newSerialNumber;
        } 
    
    public void
    setResidueName( String newResidueName )
        {
			myResidueName = newResidueName;
        }
        
	public void
    setChainID( char newChainID )
        {
			myChainID = newChainID;
        }        

	public void
    setResidueSeqNum( int newResidueSeqNum )
        {
			myResidueSeqNum = newResidueSeqNum;
        } 
        
	public void
    setOccupancy( double newOccupancy )
        {
			myOccupancy = newOccupancy;
        } 
        
	public void
    setBeta( double newBeta )
        {
			myBeta = newBeta;
        } 
        
	public void
    setVelocity( double[] newVelocity )
        {
			myVelocity[0] = newVelocity[0];
			myVelocity[1] = newVelocity[1];
			myVelocity[2] = newVelocity[2];
        } 

	public void
    setAcceleration( double[] newAcceleration )
        {
			myAcceleration[0] = newAcceleration[0];
			myAcceleration[1] = newAcceleration[1];
			myAcceleration[2] = newAcceleration[2];
        }         
        
	public void
	setForce( double[] newForce )
	{
		myForce[0] = newForce[0];
		myForce[1] = newForce[1];
		myForce[2] = newForce[2];
	}
    /*====================================================================
        Private Member Functions
    */
    

    /*====================================================================
        Private Data Members
    */

	private int mySerialNumber;
	private String myName;
	private String myResidueName;
	private char myChainID;
	private int myResidueSeqNum;
	private double[] myPosition;
	private double myOccupancy;
	private double myBeta;
    private double myRadius;
    private double myMass;
    private double myCharge;
    private double[] myVelocity;
    private double[] myAcceleration;
    private double[] myForce;

    } // end of public class Atom

