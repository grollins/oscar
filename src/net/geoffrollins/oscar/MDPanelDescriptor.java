package net.geoffrollins.oscar;
import com.nexes.wizard.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;


public class MDPanelDescriptor 
extends WizardPanelDescriptor
{
    
    public static final String IDENTIFIER = "MD_PANEL";
    
    MDPanel molecularDynamicsPanel;
    
    public MDPanelDescriptor() 
    {
        
        molecularDynamicsPanel = new MDPanel( );
        setPanelDescriptorIdentifier(IDENTIFIER);
        setPanelComponent(molecularDynamicsPanel);
        
    }
    
    public Object getNextPanelDescriptor() {
        return TempProtocolPanelDescriptor.IDENTIFIER;
    }
    
    public Object getBackPanelDescriptor() {
        return AlgorithmPanelDescriptor.IDENTIFIER;
    }
    
    public void aboutToHidePanel() 
    {
    	// save MD parameters to parameter manager
    	ParameterManager parameterManager = getWizard().getParameterManager();
    	parameterManager.setIntegrationMethod( molecularDynamicsPanel.getIntegrationMethodSelected() );
    	parameterManager.setTimeStep( Double.valueOf(molecularDynamicsPanel.getTimeStepText()) );
    	parameterManager.setNumSteps( Integer.valueOf(molecularDynamicsPanel.getNumberStepsText()) );
    	parameterManager.setTemperature( Double.valueOf(molecularDynamicsPanel.getTemperatureText()) );
    	parameterManager.setBoxLength( Double.valueOf(molecularDynamicsPanel.getBoxLengthText()) );
    	parameterManager.setNumDimensions( Integer.valueOf(molecularDynamicsPanel.getNumDimensions()) );
    	
    	// create a default temperature protocol for the user to see when the
    	// temperature protocol panel first appears
    	//double initialTemp = parameterManager.getTemperature();
    	//parameterManager.addCycle( "0", "Rescaling", "1", "100", String.valueOf( initialTemp ), String.valueOf( initialTemp ) );
    }      

}
