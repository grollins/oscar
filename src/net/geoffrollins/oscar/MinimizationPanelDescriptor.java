package net.geoffrollins.oscar;
import com.nexes.wizard.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;


public class MinimizationPanelDescriptor extends WizardPanelDescriptor {
    
    public static final String IDENTIFIER = "MINIMIZATION_PANEL";
    
    MinimizationPanel minimizationPanel;
    
    public MinimizationPanelDescriptor() 
    {
        
        minimizationPanel = new MinimizationPanel();
        setPanelDescriptorIdentifier(IDENTIFIER);
        setPanelComponent(minimizationPanel);    
    }
    
    public Object getNextPanelDescriptor() 
    {
    	return SimulationPanelDescriptor.IDENTIFIER;
    }
    
    public Object getBackPanelDescriptor() {
        return AlgorithmPanelDescriptor.IDENTIFIER;
    }
    
    public void aboutToHidePanel() 
    {
    	// save MD parameters to parameter manager
    	ParameterManager parameterManager = getWizard().getParameterManager();
    	parameterManager.setMinimizationMethod( minimizationPanel.getMinimizationMethodSelected() );
    	parameterManager.setStepSize( Double.valueOf(minimizationPanel.getStepSizeText()) );
    	parameterManager.setNumSteps( Integer.valueOf(minimizationPanel.getNumberStepsText()) );
    	parameterManager.setConvCrit( Double.valueOf(minimizationPanel.getConvCritText()) );
    }      

}
