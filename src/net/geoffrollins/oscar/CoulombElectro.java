/*****************************************************************************
    CoulombElectro.java

    Last Updated   - January 21, 2008
    Updated By     - Geoff Rollins

*****************************************************************************/

/*****************************************************************************
    Package and import statments
*/

package net.geoffrollins.oscar;

/*****************************************************************************
    Class Definitions
*/

/*============================================================================
    Electrostatic potential. Coulomb's law.
============================================================================*/
public class CoulombElectro 
extends AbstractEnergyTerm
{
	// C = 332 if energy is in kcal/mol or 332*CEU_CONVERSION_FACTOR if energy is in CEU
	static final double ELEC_CONST = 138776.0;
	static final String IDENTIFIER = "Electro";
	
	/** 
    *	Default class constructor.
    */
    public CoulombElectro( )
        {
			setAtoms( new Atom[2] ); //allocates memory for the two atoms joined by this term
			setDielectric( 1.0 );
			setName( IDENTIFIER );
        }

	/** 
    *	Class constructor.
    */
    public CoulombElectro( Atom[] atoms, double dielectric  )
        {
        	setAtoms( atoms );
			setDielectric( dielectric );
        	setName( IDENTIFIER );
        }

    
    /*====================================================================
        Public Member Functions
    */
    
    public double
    calculateDeltaX()
    {
    	Atom[] atoms = getAtoms();
    	return atoms[1].getX() - atoms[0].getX(); 
    }

	public double
	calculateDeltaY()
	{
		Atom[] atoms = getAtoms();
		return atoms[1].getY() - atoms[0].getY();
    }
    
    public double
    calculateDeltaZ()
    {
    	Atom[] atoms = getAtoms();
    	return atoms[1].getZ() - atoms[0].getZ();
    }

    
    /**-----------------------------------------------------------------
    *	Calculate the distance between the two atoms governed by this
    *	electrostatic term
	*
	*	@return			current separation
	--------------------------------------------------------------------*/
    public double
    calculateSeparation()
    {
    	Atom[] atoms = getAtoms();
    	double deltaX = atoms[1].getX() - atoms[0].getX(); 
    	double deltaY = atoms[1].getY() - atoms[0].getY();
    	double deltaZ = atoms[1].getZ() - atoms[0].getZ();
    	return Math.sqrt( Math.pow( deltaX, 2 ) + Math.pow( deltaY, 2 ) + Math.pow( deltaZ, 2 ) );
    }
    
	/**-----------------------------------------------------------------
    *	Calculate the energy of this electrostatic term.
	*	E = ELEC_CONST * ( q_0 * q_1 ) / (r*dielectricConstant)
	*
	*	@return			energy of this electrostatic term
	--------------------------------------------------------------------*/
    public double
    calculateEnergy( )
    {
  		Atom[] atoms = getAtoms();
    	
    	return ELEC_CONST * atoms[0].getCharge() * atoms[1].getCharge() / calculateSeparation() / getDielectric();
    }

	/**-----------------------------------------------------------------
    *	Calculate the force of this VDW term. Add this force to atoms' current
    *	force values. Return virial.
    *
	*	F = ELEC_CONST * ( q_0 * q_1 ) / (r^2 * dielectricConstant)
	--------------------------------------------------------------------*/
	public double
    calculateForce()
    {
    	double currentSeparation = calculateSeparation();
    	Atom[] atoms = getAtoms();
    	
    	double totalForce = ELEC_CONST * atoms[0].getCharge() * atoms[1].getCharge() / Math.pow(calculateSeparation(),2) / getDielectric();
    	
    	double dx = calculateDeltaX(); 
    	double dy = calculateDeltaY();
    	double dz = calculateDeltaZ();
    	
    	double xForce = totalForce * dx/currentSeparation; 
    	double yForce = totalForce * dy/currentSeparation;
    	double zForce = totalForce * dz/currentSeparation;
    
    	double[] forceOnAtom0 = {-xForce, -yForce, -zForce};
    	double[] forceOnAtom1 = {xForce, yForce, zForce};
    	
    	atoms[0].addForce( forceOnAtom0 );
    	atoms[1].addForce( forceOnAtom1 );
    	
    	double virial = (dx * xForce) + (dy * yForce) + (dz * zForce);
    	
    	return virial;
    	
    }

	public String
    toString( )
    {
	    Atom[] atoms = getAtoms();
	    return atoms[0].getSerialNumber() + "\t" + atoms[1].getSerialNumber() + "\t" + getDielectric();
    }

	/**-----------------------------------------------------------------
	*	Accessors
	*--------------------------------------------------------------------*/
	
	public double
    getDielectric( )
        {
			return myDielectric;
        }

    public void
    setDielectric( double newDielectric )
        {
			myDielectric = newDielectric;
        }     


    /*====================================================================
        Private Member Functions
    */
	

    /*====================================================================
        Private Data Members
    */
    private double myDielectric;	// the dielectric constant
	
    }

