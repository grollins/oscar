/*****************************************************************************
    VelocityVerletMD.java

    Last Updated   - March 17, 2008
    Updated By     - Geoff Rollins

    Molecular dynamics, velocity Verlet algorithm
    
    The velocity Verlet algorithm uses the positions, velocities, and accelerations at time t to calculate the position at time t+dt. It uses the velocities and accelerations at time t and the accelerations at time t+dt to calculate the velocities at time t+dt.
    
    x(t+dt) = x(t) + v(t)*dt + (1/2)*a(t)*dt^2
    v(t+dt) = v(t) + (1/2)*dt*[a(t) + a(t+dt)]

*****************************************************************************/

/*****************************************************************************
    Package and import statments
*/
package net.geoffrollins.oscar;

import java.util.LinkedList;
import java.util.ListIterator;
import com.braju.format.*;

/*****************************************************************************
    Class Definitions
*/

/*============================================================================
    One-line summary of class goal
 ............................................................................

    Description of class

============================================================================*/
public class VelocityVerletMD 
    extends AbstractSimAlgorithm 
    {

	public static final String MD_IDENTIFIER = "Molecular Dynamics";

    /*--------------------------------------------------------------------
        Constructors
    --------------------------------------------------------------------*/

    /** 
    *	Default class constructor.
    */
    public VelocityVerletMD( )
        {
			setTimeStep( 1.0 );
			setKineticEnergy( 0.00 );
			setTemperature( 300.00 );
			setID( MD_IDENTIFIER );
        }

    /** 
    *	Class constructor.
    */
    public VelocityVerletMD( double timeStep )
        {
			setTimeStep( timeStep );
			setKineticEnergy( 0.00 );
			setTemperature( 300.00 );
			setID( MD_IDENTIFIER );

        }


    /*====================================================================
        Public Member Functions
    */

	/** 
    *	Performs one step of molecular dynamics
    *
    *	@param	molSystem
    *	@param	hamiltonian
    *	@param	stepNumber
    *	@return the energy of the system at this step
    */
    public String 
    simStep( MolecularSystem molSystem, Hamiltonian hamiltonian, int stepNumber, int numDimensions )
    {
    	ListIterator atomList;
    	Atom thisAtom;
    	double[] currentVelocity, currentPosition, currentAcceleration;
    	
    	// 1. Initialize force on each atom to zero
    	atomList = molSystem.iterateAtoms();
    	while( atomList.hasNext() )
    	{
    		thisAtom = (Atom)atomList.next();
    		thisAtom.zeroForce();
    	}
    	
    	// 2. Calculate force on each atom
    	hamiltonian.calculateForce();
    	
    	// 3. Calculate accelerations
    	atomList = molSystem.iterateAtoms(); //reset iterator to the start of the atom list
    	while( atomList.hasNext() )
    	{
    		thisAtom = (Atom)atomList.next();
    		thisAtom.calculateAcceleration();
    	}
    	
    	// 4. Calculate kinetic energy and temperature
    	double kinetic = 0.00;
    	int numAtoms = 0;
    	atomList = molSystem.iterateAtoms();
    	while( atomList.hasNext() )
    	{
    		thisAtom = (Atom)atomList.next();
    		currentVelocity = thisAtom.getVelocity();
    		
    		kinetic += 0.5 * thisAtom.getMass() * ( Math.pow(currentVelocity[0],2) + Math.pow(currentVelocity[1],2) + Math.pow(currentVelocity[2],2) );
    		numAtoms++;
    		
    	}
    
    	setKineticEnergy( kinetic );
    	double averageKinetic = kinetic / numAtoms;
    	setTemperature( (2 * averageKinetic) / (numDimensions * OscarSimulation.GAS_CONSTANT) );
    	
    	// 5. Calculate energies and create energy output string
    	String outputString = formatEnergy( molSystem, hamiltonian, stepNumber );
    	
    	// 6. Advance atom positions
		double[] newPosition = new double[3];
		atomList = molSystem.iterateAtoms(); //reset iterator to the start of the atom list
    	while( atomList.hasNext() )
    	{
    		thisAtom = (Atom)atomList.next();
    		currentVelocity = thisAtom.getVelocity();
    		currentPosition = thisAtom.getPosition();
    		currentAcceleration = thisAtom.getAcceleration();
    		
    		newPosition[0] = currentPosition[0] + currentVelocity[0] * getTimeStep() + 0.5*currentAcceleration[0]*Math.pow(getTimeStep(),2);
    		newPosition[1] = currentPosition[1] + currentVelocity[1] * getTimeStep() + 0.5*currentAcceleration[1]*Math.pow(getTimeStep(),2);
    		newPosition[2] = currentPosition[2] + currentVelocity[2] * getTimeStep() + 0.5*currentAcceleration[2]*Math.pow(getTimeStep(),2);
    		thisAtom.setPosition( newPosition );
    	}
    
    	// 7. Calculate velocities at t + dt/2
    	double[] newVelocity = new double[3];
    	
    	atomList = molSystem.iterateAtoms();
    	while( atomList.hasNext() )
    	{
    		thisAtom = (Atom)atomList.next();
    		currentAcceleration = thisAtom.getAcceleration();
    		currentVelocity = thisAtom.getVelocity();
    		
    		newVelocity[0] = currentVelocity[0] + 0.5 * currentAcceleration[0] * getTimeStep();
    		newVelocity[1] = currentVelocity[1] + 0.5 * currentAcceleration[1] * getTimeStep();
    		newVelocity[2] = currentVelocity[2] + 0.5 * currentAcceleration[2] * getTimeStep();
    		
    		thisAtom.setVelocity( newVelocity );
    		
    	}
    
    	// 8. Calculate force and acceleration for new positions
    	//Initialize force on each atom to zero
    	atomList = molSystem.iterateAtoms();
    	while( atomList.hasNext() )
    	{
    		thisAtom = (Atom)atomList.next();
    		thisAtom.zeroForce();
    	}
    	//Calculate force on each atom
    	hamiltonian.calculateForce();
    	//Calculate accelerations
    	atomList = molSystem.iterateAtoms(); //reset iterator to the start of the atom list
    	while( atomList.hasNext() )
    	{
    		thisAtom = (Atom)atomList.next();
    		thisAtom.calculateAcceleration();
    	}
    	
    	// 9. Calculate velocities at t + dt
    	atomList = molSystem.iterateAtoms();
    	while( atomList.hasNext() )
    	{
    		thisAtom = (Atom)atomList.next();
    		currentAcceleration = thisAtom.getAcceleration();
    		currentVelocity = thisAtom.getVelocity();
    		
    		newVelocity[0] = currentVelocity[0] + 0.5 * currentAcceleration[0] * getTimeStep();
    		newVelocity[1] = currentVelocity[1] + 0.5 * currentAcceleration[1] * getTimeStep();
    		newVelocity[2] = currentVelocity[2] + 0.5 * currentAcceleration[2] * getTimeStep();
    		
    		thisAtom.setVelocity( newVelocity );
    		
    	}
    
    
    	// Return energy output string
    	return outputString;
    }

	/*-----------------------------------------------------------------
	*	Accessors
	*--------------------------------------------------------------------*/
    public double
    getTimeStep( )
        {
			return myTimeStep;
        }

	public void
    setTimeStep( double newTimeStep )
        {
			myTimeStep = newTimeStep;
        }

	public double
    getTemperature( )
        {
			return myTemperature;
        }

	public void
    setTemperature( double newTemperature )
        {
			myTemperature = newTemperature;
        }
        
	public double
    getKineticEnergy( )
        {
			return myKineticEnergy;
        }

	public void
    setKineticEnergy( double newKineticEnergy )
        {
			myKineticEnergy = newKineticEnergy;
        }


	public String formatEnergy( MolecularSystem molSystem, Hamiltonian hamiltonian, int stepNumber )
	{
		String outputString = "%8.3d     ";
		Parameters outputStringParameters = new Parameters( stepNumber );
		double potentialEnergy = 0.00;
		
		// Sum potential energy terms
		LinkedList energyList = hamiltonian.calculateEnergy( );
		ListIterator energyIterator = energyList.listIterator( 0 );
		double thisEnergy;
		while( energyIterator.hasNext() )
		{
			thisEnergy = (Double)energyIterator.next();
			// For output, convert energy from CEU to kcal/mol
			thisEnergy = thisEnergy / AbstractEnergyTerm.CEU_CONVERSION_FACTOR;
			
			
			outputString += "%10.6f     ";
			outputStringParameters = outputStringParameters.add( thisEnergy );
			
			potentialEnergy += thisEnergy;
		}
		
		// Convert kinetic energy to kcal/mol for output
		double kineticEnergy = getKineticEnergy() / AbstractEnergyTerm.CEU_CONVERSION_FACTOR;
		
		// Calculate total energy
		double totalEnergy = potentialEnergy + kineticEnergy;
		
		// Add potential, kinetic, total, and temp to the output string
		outputString += "%10.6f     %10.6f     %10.6f     %9.3f     %9.3f \n";
		// Add potential, kinetic, total, and temp to the list of output parameters
		
		LinkedList boxTerms = hamiltonian.getEnergyTerms().getInteractionList( "Box" );
		double pressure = 0.0;
		if( boxTerms == null )
		{
			pressure = 0.0;
		}
		else
		{
			BoundingBox box = (BoundingBox)boxTerms.get( 0 );
			double boxLength = box.getBoxLength();
			pressure = calculatePressure(hamiltonian.getCurrentVirial(), molSystem.numAtoms(), boxLength);
		}

		outputStringParameters.add( potentialEnergy ).add( kineticEnergy ).add( totalEnergy ).add( getTemperature() ).add( pressure );

		//System.out.println( outputString );
		return Format.sprintf( outputString, outputStringParameters );		
		
	}

	public String
	getEnergyBanner( Hamiltonian hamiltonian )
	{
		String bannerString = "%8s     ";
		Parameters bannerStringParameters = new Parameters( "Step" );
		
		LinkedList interactionList = hamiltonian.getInteractionNames( );
		ListIterator interactionIterator = interactionList.listIterator( 0 );
		String thisInteraction;
		while( interactionIterator.hasNext() )
		{
			thisInteraction = (String)interactionIterator.next();
			
			
			bannerString += "%10s     ";
			bannerStringParameters = bannerStringParameters.add( thisInteraction );
		}
		
		// Add potential, kinetic, total, and temp to the banner string
		bannerString += "%10s     %10s     %10s     %9s     %9s \n";
		bannerStringParameters.add( "Potential" ).add( "Kinetic" ).add( "Total" ).add( "Temperature" ).add( "Pressure" );

		return Format.sprintf( bannerString, bannerStringParameters );	
	}
	
	public boolean isTimeToQuit()
	{
		return false;
	}

    /*====================================================================
        Private Member Functions
    */
	private double
    calculatePressure( double virial, int numAtoms, double boxLength )
    {
    	double PV =  (numAtoms * OscarSimulation.GAS_CONSTANT * getTemperature()) + (0.5 * virial);
    	double pressureCEU = PV / (boxLength * boxLength * boxLength);
    	return pressureCEU * OscarSimulation.BAR_CONVERSION_FACTOR;
    }

    /*====================================================================
        Private Data Members
    */

    private double myTimeStep; //femtoseconds
	private double myTemperature;
	private double myKineticEnergy;

    }

