package net.geoffrollins.oscar;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;


public class MinimizationPanel extends JPanel {
        
    public MinimizationPanel() {
     
        super();
         
        contentPanel = getContentPanel();
        contentPanel.setBorder(new EmptyBorder(new Insets(10, 10, 10, 10)));
		contentPanel.setBackground( Color.decode("0xD0E6FF") );
		Font headerFont = new Font("Arial", Font.BOLD, 24);

        ImageIcon icon = getImageIcon();
        
        titlePanel = new javax.swing.JPanel();
        textLabel = new javax.swing.JLabel();
        iconLabel = new javax.swing.JLabel();
        separator = new javax.swing.JSeparator();

        setLayout(new java.awt.BorderLayout());


        titlePanel.setLayout(new java.awt.BorderLayout());
        titlePanel.setBackground(Color.decode("0x3C8CE4"));
        
        textLabel.setBackground(Color.decode("0x3C8CE4"));
        textLabel.setForeground(Color.white);
        textLabel.setFont(headerFont);
        textLabel.setText("Energy Minimization Setup");
        textLabel.setBorder(new EmptyBorder(new Insets(10, 10, 10, 10)));
        textLabel.setOpaque(true);

        iconLabel.setBackground(Color.white);
        if (icon != null)
            iconLabel.setIcon(icon);
        
        titlePanel.add(textLabel, BorderLayout.CENTER);
        titlePanel.add(iconLabel, BorderLayout.EAST);
        titlePanel.add(separator, BorderLayout.SOUTH);
        
        add(titlePanel, BorderLayout.NORTH);
        JPanel secondaryPanel = new JPanel();
        secondaryPanel.add(contentPanel, BorderLayout.NORTH);
        secondaryPanel.setBackground(Color.decode("0xD0E6FF"));
        add(secondaryPanel, BorderLayout.CENTER);

    }  
    
    public String getMinimizationMethodSelected() 
    {
        return (String)MethodComboBox.getSelectedItem();
    }
    
    public String getStepSizeText()
    {
    	return stepSizeTextField.getText();
    }
    
    public String getNumberStepsText()
    {
    	return numberStepsTextField.getText();
    }
    
    public String getConvCritText()
    {
    	return convCritTextField.getText();
    }
    
    private JPanel getContentPanel() {     
        
        Font titleFont = new Font("Arial", Font.PLAIN, 18);
        Font labelFont = new Font("Arial", Font.PLAIN, 16);
        
        JPanel contentPanel1 = new JPanel();
        welcomeTitle = new JLabel();
        blankSpace = new JLabel();
		jPanel1 = new javax.swing.JPanel();

       	MethodLabel = new javax.swing.JLabel();
        stepSizeLabel = new javax.swing.JLabel();
        numberStepsLabel = new javax.swing.JLabel();
        stepSizeTextField = new javax.swing.JTextField();
        numberStepsTextField = new javax.swing.JTextField();
        stepSizeLabel2 = new javax.swing.JLabel();
        numberStepsLabel2 = new javax.swing.JLabel();
        convCritLabel1 = new javax.swing.JLabel();
        convCritTextField = new javax.swing.JTextField();
        convCritLabel2 = new javax.swing.JLabel();
        MethodComboBox = new javax.swing.JComboBox();

        MethodLabel.setFont(labelFont);
        MethodLabel.setText("Minimization Method");

        stepSizeLabel.setFont(labelFont);
        stepSizeLabel.setText("Initial Step Size");

        numberStepsLabel.setFont(labelFont);
        numberStepsLabel.setText("Number of Steps");

        stepSizeTextField.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        stepSizeTextField.setText("0.0001");
		stepSizeTextField.setFont(labelFont);
        numberStepsTextField.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        numberStepsTextField.setText("10000");
        numberStepsTextField.setFont(labelFont);

        stepSizeLabel2.setText("(unitless)");
        stepSizeLabel2.setFont(labelFont);
        

        numberStepsLabel2.setText("steps");
		numberStepsLabel2.setFont(labelFont);

        convCritLabel1.setFont(labelFont);
        convCritLabel1.setText("Convergence Criterion");

        convCritTextField.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        convCritTextField.setText("0.00001");
        convCritTextField.setFont(labelFont);

        convCritLabel2.setText("(unitless)");
        convCritLabel2.setFont(labelFont);

        MethodComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Steepest Descent", "Conjugate Gradient" }));
        MethodComboBox.setSelectedIndex(0);
        MethodComboBox.setFont(labelFont);

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(convCritLabel1)
                    .add(MethodLabel)
                    .add(stepSizeLabel)
                    .add(numberStepsLabel))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createSequentialGroup()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, numberStepsTextField)
                            .add(stepSizeTextField)
                            .add(convCritTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 132, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(numberStepsLabel2)
                            .add(stepSizeLabel2)
                            .add(convCritLabel2)))
                    .add(MethodComboBox, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 251, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(MethodLabel)
                    .add(MethodComboBox, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(stepSizeLabel)
                    .add(stepSizeLabel2)
                    .add(stepSizeTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(numberStepsLabel)
                    .add(numberStepsTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(numberStepsLabel2))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(convCritLabel1)
                    .add(convCritLabel2)
                    .add(convCritTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(42, Short.MAX_VALUE))
        );
        
        contentPanel1.setLayout(new java.awt.BorderLayout());

        jPanel1.add(blankSpace);
		jPanel1.setBackground( Color.decode("0xD0E6FF") );       

        contentPanel1.add(jPanel1, java.awt.BorderLayout.CENTER);
        contentPanel1.setBackground( Color.decode("0xD0E6FF") );
        
        return contentPanel1;
    }
    
    private ImageIcon getImageIcon() {
        
        //  Icon to be placed in the upper right corner.
        
        return null;
    }
    

	private JComboBox MethodComboBox;
    private JLabel MethodLabel;
    private JLabel convCritLabel1;
    private JLabel convCritLabel2;
    private JTextField convCritTextField;
    private JLabel numberStepsLabel;
    private JLabel numberStepsLabel2;
    private JTextField numberStepsTextField;
    private JLabel stepSizeLabel;
    private JLabel stepSizeLabel2;
    private JTextField stepSizeTextField;

    private JLabel welcomeTitle;
    private JPanel contentPanel;
    private JLabel iconLabel;
    private JSeparator separator;
    private JLabel textLabel;
    private JPanel titlePanel;  
    private JPanel jPanel1;
    private JLabel blankSpace;


}
