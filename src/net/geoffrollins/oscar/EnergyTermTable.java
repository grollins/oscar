/*****************************************************************************
    EnergyTermTable.java

    Last Updated   - January 14, 2008
    Updated By     - Geoff Rollins

    Table of energy terms of a molecular simulation

*****************************************************************************/

/*****************************************************************************
    Package and import statments
*/

package net.geoffrollins.oscar;

import java.util.LinkedList;
import java.util.HashMap;

/*****************************************************************************
    Class Definitions
*/

/*============================================================================
    Stores all of the potential energy terms from an Oscar simulation. 
    
    Each potential energy term is placed in a list according to its 
    interaction type. For example, a HarmonicBond energy term would
    reside in a list with all of the other HarmonicBond terms. The
    interaction lists are grouped together in a table, which is
    currently (January 2008) implemented as a HashMap.
============================================================================*/
public class EnergyTermTable    
{

    /*--------------------------------------------------------------------
        Constructors
       --------------------------------------------------------------------*/

    /** 
    *	Default class constructor.
    */
    public EnergyTermTable( )
        {
			myTable = new HashMap( );
			myInteractionNames = new LinkedList();
        }

    /** 
    *	Class constructor. Creates a table to store energy terms in.
    *	@param	numInteractions
    */
    public EnergyTermTable( int numInteractions)
        {
        	myTable = new HashMap( numInteractions );
        	myInteractionNames = new LinkedList();
			
        }


    /*====================================================================
        Public Member Functions
    */
    
    public Object
    addInteraction( String interactionName )
    {
    	getInteractionNames().add( interactionName );
    	return myTable.put( interactionName, new LinkedList() );
    }
    
    public LinkedList
    getInteractionList( String interactionName )
    {
    	return (LinkedList)myTable.get( interactionName );
    }
    
    public int
    getNumInteractions( )
    {
    	return myTable.size();
    }
    
	/**-----------------------------------------------------------------
	*	Accessors
	*--------------------------------------------------------------------*/

    public HashMap
    getTable( )
        {
			return myTable;
        }

    public void
    setTable( HashMap newTable )
        {
			myTable = newTable;
        }

	public LinkedList
    getInteractionNames( )
        {
			return myInteractionNames;
        }

    public void
    setInteractionNames( LinkedList newInteractionNames )
        {
			myInteractionNames = newInteractionNames;
        }


    /*====================================================================
        Private Member Functions
    */
    

    /*====================================================================
        Private Data Members
    */

    private HashMap myTable;
    private LinkedList myInteractionNames;

}

