/*****************************************************************************
    DefaultMD.java

    Last Updated   - January 17, 2008
    Updated By     - Geoff Rollins

    Molecular dynamics, default Leapfrog algorithm implemented by Steve Harvey in the original Perl version of this program.

*****************************************************************************/

/*****************************************************************************
    Package and import statments
*/

package net.geoffrollins.oscar;

import java.util.LinkedList;
import java.util.ListIterator;
import com.braju.format.*;

/*****************************************************************************
    Class Definitions
*/

/*============================================================================
    One-line summary of class goal
 ............................................................................

    Description of class

============================================================================*/
public class DefaultMD 
    extends AbstractSimAlgorithm 
    {
    
    /*====================================================================
        Constants
    */
    public static double GAS_CONSTANT = 0.830936; //gas constant = 2 cal/mol*K  =  0.830936 CEU/K
    public static double AVOGADRO_NUMBER = Math.pow( 6.0221, 23 ); //6.0221 * 10^23/mol
    public static double BOLTZMANN_CONSTANT = GAS_CONSTANT / AVOGADRO_NUMBER; //1.3887 * 10^-24 CEU/K
 	
 	public static double BAR_CONVERSION_FACTOR = 165.525; //1 CEU/cubic angstrom = 165.525 bar
 	
 	public static final String MD_IDENTIFIER = "Molecular Dynamics";
 
 	/*--------------------------------------------------------------------
        Constructors
    --------------------------------------------------------------------*/

    /** 
    *	Default class constructor.
    */
    public DefaultMD( )
        {
			setTimeStep( 1.0 );
			setKineticEnergy( 0.00 );
			setTemperature( 300.00 );
			setID( MD_IDENTIFIER );
        }

    /** 
    *	Class constructor.
    */
    public DefaultMD( double timeStep )
        {
			setTimeStep( timeStep );
			setKineticEnergy( 0.00 );
			setTemperature( 300.00 );
			setID( MD_IDENTIFIER );
        }


    /*====================================================================
        Public Member Functions
    */

	/** 
    *	Performs one step of molecular dynamics
    *
    *	@param	molSystem
    *	@param	hamiltonian
    *	@param	stepNumber
    *	@return the energy of the system at this step
    */
    public String 
    simStep( MolecularSystem molSystem, Hamiltonian hamiltonian, int stepNumber, int numDimensions )
    {
    	ListIterator atomList;
    	Atom thisAtom;
    	
    	// 1. Initialize force on each atom to zero
    	atomList = molSystem.iterateAtoms();
    	while( atomList.hasNext() )
    	{
    		thisAtom = (Atom)atomList.next();
    		thisAtom.zeroForce();
    	}
    	
    	// 2. Calculate force on each atom
    	hamiltonian.calculateForce();
    	
    	// 3. Calculate accelerations
    	atomList = molSystem.iterateAtoms(); //reset iterator to the start of the atom list
    	while( atomList.hasNext() )
    	{
    		thisAtom = (Atom)atomList.next();
    		thisAtom.calculateAcceleration();
    	}
    	
		// 4. Advance velocities by the timestep   	
    	double[] newVelocity = new double[3];
    	double[] thisAcceleration;
    	double[] thisVelocity;
    	double dt = getTimeStep();
    	
    	if( stepNumber == 1 )
    	{
    		dt = dt / 2.0;
    	}
    	
    	atomList = molSystem.iterateAtoms();
    	while( atomList.hasNext() )
    	{
    		thisAtom = (Atom)atomList.next();
    		thisAcceleration = thisAtom.getAcceleration();
    		thisVelocity = thisAtom.getVelocity();
    		
    		newVelocity[0] = thisVelocity[0] + thisAcceleration[0] * dt;
    		newVelocity[1] = thisVelocity[1] + thisAcceleration[1] * dt;
    		newVelocity[2] = thisVelocity[2] + thisAcceleration[2] * dt;
    		
    		thisAtom.setVelocity( newVelocity );
    		
    	}
    		
    	// 5. Calculate kinetic energy and temperature
    	//Calculate velocities at half time step so that temperature and kinetic
    	//energy are calculated at the same point as potential energy
    	double kinetic = 0.00;
    	double[] halfStepVelocity = new double[3];
    	int numAtoms = 0;
    	atomList = molSystem.iterateAtoms();
    	while( atomList.hasNext() )
    	{
    		thisAtom = (Atom)atomList.next();
    		thisAcceleration = thisAtom.getAcceleration();
    		thisVelocity = thisAtom.getVelocity();
    		
    		halfStepVelocity[0] = thisVelocity[0] - thisAcceleration[0] * (getTimeStep()/2);
    		halfStepVelocity[1] = thisVelocity[1] - thisAcceleration[1] * (getTimeStep()/2);
    		halfStepVelocity[2] = thisVelocity[2] - thisAcceleration[2] * (getTimeStep()/2);
    		
    		kinetic += 0.5 * thisAtom.getMass() * ( Math.pow(halfStepVelocity[0],2) + Math.pow(halfStepVelocity[1],2) + Math.pow(halfStepVelocity[2],2) );
    		numAtoms++;
    		
    	}
    
    	setKineticEnergy( kinetic );
    	double averageKinetic = kinetic / numAtoms;
    	setTemperature( (2 * averageKinetic) / (numDimensions * GAS_CONSTANT) );
    	
    	// Calculate energies and create energy output string
    	String outputString = formatEnergy( molSystem, hamiltonian, stepNumber );
    	
    	// Advance atom positions
		double[] currentVelocity, currentPosition;
		double[] newPosition = new double[3];
		atomList = molSystem.iterateAtoms(); //reset iterator to the start of the atom list
    	while( atomList.hasNext() )
    	{
    		thisAtom = (Atom)atomList.next();
    		currentVelocity = thisAtom.getVelocity();
    		currentPosition = thisAtom.getPosition();
    		newPosition[0] = currentPosition[0] + currentVelocity[0] * getTimeStep();
    		newPosition[1] = currentPosition[1] + currentVelocity[1] * getTimeStep();
    		newPosition[2] = currentPosition[2] + currentVelocity[2] * getTimeStep();
    		thisAtom.setPosition( newPosition );
    	}
    	
    	// Return energy output string
    	return outputString;
    }

	/*-----------------------------------------------------------------
	*	Accessors
	*--------------------------------------------------------------------*/
    public double
    getTimeStep( )
        {
			return myTimeStep;
        }

	public void
    setTimeStep( double newTimeStep )
        {
			myTimeStep = newTimeStep;
        }

	public double
    getTemperature( )
        {
			return myTemperature;
        }

	public void
    setTemperature( double newTemperature )
        {
			myTemperature = newTemperature;
        }
        
	public double
    getKineticEnergy( )
        {
			return myKineticEnergy;
        }

	public void
    setKineticEnergy( double newKineticEnergy )
        {
			myKineticEnergy = newKineticEnergy;
        }
        
	public String formatEnergy( MolecularSystem molSystem, Hamiltonian hamiltonian, int stepNumber )
	{
		String outputString = "%8.3d     ";
		Parameters outputStringParameters = new Parameters( stepNumber );
		double potentialEnergy = 0.00;
		
		// Sum potential energy terms
		LinkedList energyList = hamiltonian.calculateEnergy( );
		ListIterator energyIterator = energyList.listIterator( 0 );
		double thisEnergy;
		while( energyIterator.hasNext() )
		{
			thisEnergy = (Double)energyIterator.next();
			// For output, convert energy from CEU to kcal/mol
			thisEnergy = thisEnergy / AbstractEnergyTerm.CEU_CONVERSION_FACTOR;
			
			
			outputString += "%10.6f     ";
			outputStringParameters = outputStringParameters.add( thisEnergy );
			
			potentialEnergy += thisEnergy;
		}
		
		// Convert kinetic energy to kcal/mol for output
		double kineticEnergy = getKineticEnergy() / AbstractEnergyTerm.CEU_CONVERSION_FACTOR;
		
		// Calculate total energy
		double totalEnergy = potentialEnergy + kineticEnergy;
		
		// Add potential, kinetic, total, and temp to the output string
		outputString += "%10.6f     %10.6f     %10.6f     %9.3f     %9.3f \n";
		// Add potential, kinetic, total, and temp to the list of output parameters
		
		LinkedList boxTerms = hamiltonian.getEnergyTerms().getInteractionList( "Box" );
		double pressure = 0.0;
		if( boxTerms == null )
		{
			pressure = 0.0;
		}
		else
		{
			BoundingBox box = (BoundingBox)boxTerms.get( 0 );
			double boxLength = box.getBoxLength();
			pressure = calculatePressure(hamiltonian.getCurrentVirial(), molSystem.numAtoms(), boxLength);
		}

		outputStringParameters.add( potentialEnergy ).add( kineticEnergy ).add( totalEnergy ).add( getTemperature() ).add( pressure );

		//System.out.println( outputString );
		return Format.sprintf( outputString, outputStringParameters );		
		
	}
	
	public String
	getEnergyBanner( Hamiltonian hamiltonian )
	{
		String bannerString = "%8s     ";
		Parameters bannerStringParameters = new Parameters( "Step" );
		
		LinkedList interactionList = hamiltonian.getInteractionNames( );
		ListIterator interactionIterator = interactionList.listIterator( 0 );
		String thisInteraction;
		while( interactionIterator.hasNext() )
		{
			thisInteraction = (String)interactionIterator.next();
			
			
			bannerString += "%10s     ";
			bannerStringParameters = bannerStringParameters.add( thisInteraction );
		}
		
		// Add potential, kinetic, total, and temp to the banner string
		bannerString += "%10s     %10s     %10s     %9s     %9s \n";
		bannerStringParameters.add( "Potential" ).add( "Kinetic" ).add( "Total" ).add( "Temperature" ).add( "Pressure" );

		return Format.sprintf( bannerString, bannerStringParameters );	
	}
	
	public boolean isTimeToQuit()
	{
		return false;
	}
	
	/*====================================================================
        Private Member Functions
    */
    private double
    calculatePressure( double virial, int numAtoms, double boxLength )
    {
    	double PV =  (numAtoms * GAS_CONSTANT * getTemperature()) + (0.5 * virial);
    	double pressureCEU = PV / (boxLength * boxLength * boxLength);
    	return pressureCEU * BAR_CONVERSION_FACTOR;
    }
    

    /*====================================================================
        Private Data Members
    */

    private double myTimeStep; //femtoseconds
	private double myTemperature;
	private double myKineticEnergy;
    }

