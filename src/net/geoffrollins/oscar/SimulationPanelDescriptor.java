package net.geoffrollins.oscar;
import com.nexes.wizard.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.io.*;


public class SimulationPanelDescriptor extends WizardPanelDescriptor {
    
    public static final String IDENTIFIER = "SIMULATION_PANEL";
    
    SimulationPanel simPanel;
    
    public SimulationPanelDescriptor() {
        
        simPanel = new SimulationPanel();
        setPanelDescriptorIdentifier(IDENTIFIER);
        setPanelComponent(simPanel);
        
    }

    public Object getNextPanelDescriptor() {
        return FINISH;
    }
    
    public Object getBackPanelDescriptor() {
        return FilePanelDescriptor.IDENTIFIER;
    }
    
    
    public void aboutToDisplayPanel() {
        
        simPanel.setProgressValue(0);
        getWizard().setNextFinishButtonEnabled(false);
        getWizard().setBackButtonEnabled(false);
        
    }
    
    public void displayingPanel() 
    {

            Thread oscarSim = new Thread() 
            {

				public void run() 
				{
					ParameterManager oscarParameters = getWizard().getParameterManager();
					OscarSimulation thisSimulation = new OscarSimulation();
					
					simPanel.setTextLabel( oscarParameters.getJobName() );
					
					try
					{
						thisSimulation.readCoordinates( oscarParameters.getCoordinateFileName() );
					}
					catch( Exception e )
					{			
						System.err.println( "File error" );
						getWizard().setBackButtonEnabled(true);
					}
					
					try
					{
						thisSimulation.readStructure( oscarParameters.getStructureFileName() );
					}
					catch( Exception e )
					{			
						System.err.println( "File or bond error" );
						System.out.println( e.getMessage() );
			
						StackTraceElement elements[] = e.getStackTrace();
						for (int i = 0, n = elements.length; i < n; i++) 
						{       
							System.err.println(elements[i].getFileName() + ":" 
									  + elements[i].getLineNumber() 
									  + ">> " 
									  + elements[i].getMethodName() + "()");
						}
						getWizard().setBackButtonEnabled(true);
					}
					
					String algorithm = oscarParameters.getAlgorithm();
					AbstractSimAlgorithm simAlgorithm;
					
					if( algorithm.equalsIgnoreCase( "Energy Minimization" ) )
					{
						String minMethod = oscarParameters.getMinimizationMethod();
						if(  minMethod.equalsIgnoreCase( "Steepest Descent" ) )
						{
							System.out.println( "Steepest Descent" );
							simAlgorithm = new SteepestDescentMinimization( oscarParameters.getStepSize(), oscarParameters.getConvCrit() );
						}
						else
						{
							System.out.println( "Conjugate Gradient" );
							simAlgorithm = new ConjugateGradientMinimization( oscarParameters.getStepSize(), oscarParameters.getConvCrit() );
						}
						thisSimulation.addBox( 0.0 );
					}
					else if( algorithm.equalsIgnoreCase( "Molecular Dynamics" ) )
					{
						String integrationMethod = oscarParameters.getIntegrationMethod();
						
						if( integrationMethod.equalsIgnoreCase( "Velocity Verlet" ) )
						{
							simAlgorithm = new VelocityVerletMD(oscarParameters.getTimeStep());
						}
						else
						{
							simAlgorithm = new DefaultMD(oscarParameters.getTimeStep());
						}
						
						simAlgorithm.setTemperature( oscarParameters.getTemperature());
						thisSimulation.addBox( oscarParameters.getBoxLength());
						oscarParameters.getTemperatureProtocol().setTotalNumSteps( oscarParameters.getNumSteps() );
					}
					else if( algorithm.equalsIgnoreCase( "Monte Carlo" ) )
					{
						if( oscarParameters.getMCMethod().equalsIgnoreCase( "Torsional" ) )
						{
							simAlgorithm = new TorsionalMonteCarlo(oscarParameters.getMaxDisplacement() );
						}
						else
						{
							simAlgorithm = new MetropolisMonteCarlo(oscarParameters.getMaxDisplacement() );
						}
						
						simAlgorithm.setTemperature( oscarParameters.getTemperature());
						thisSimulation.addBox( oscarParameters.getBoxLength());
						oscarParameters.getTemperatureProtocol().setTotalNumSteps( oscarParameters.getNumSteps() );
					}
					else
					{
						simAlgorithm = new DefaultMD(oscarParameters.getTimeStep());
						simAlgorithm.setTemperature( oscarParameters.getTemperature());
						thisSimulation.addBox( oscarParameters.getBoxLength());
						oscarParameters.getTemperatureProtocol().setTotalNumSteps( oscarParameters.getNumSteps() );
					}
					
					thisSimulation.setSimAlgorithm( simAlgorithm );
					
					
					try 
					{	
						thisSimulation.runSimulation( oscarParameters.getNumSteps(), oscarParameters.getNumDimensions(), oscarParameters.getOutputInterval(), oscarParameters.getOutputFolderName(), oscarParameters.getJobName(), simPanel.getProgressBar(), oscarParameters.getTemperatureProtocol(), oscarParameters.getPrintRawCoords() );
	
						getWizard().setNextFinishButtonEnabled(true);
						getWizard().setBackButtonEnabled(true);
	
					} 
					catch (Exception e) 
					{
						
						simPanel.setProgressValue(0);
						simPanel.setProgressText("An Error Has Occurred");
						
						File errorFile = new File( oscarParameters.getJobName() + "_error.log" );
						
						// Write the error to a log file if possible...
						try
						{
							PrintWriter errorLog = new PrintWriter( errorFile );
						
							errorLog.println( e.getMessage() );
				
							StackTraceElement elements[] = e.getStackTrace();
							for (int i = 0, n = elements.length; i < n; i++) 
							{       
								errorLog.println(elements[i].getFileName() + ":" 
										  + elements[i].getLineNumber() 
										  + ">> " 
										  + elements[i].getMethodName() + "()");
							}
							errorLog.close();
						
						}
						// ...otherwise write the error to the system error stream
						catch( Exception logError )
						{
							System.err.println( e.getMessage() );
							StackTraceElement elements[] = e.getStackTrace();
							for (int i = 0, n = elements.length; i < n; i++) 
							{       
								System.err.println(elements[i].getFileName() + ":" 
										  + elements[i].getLineNumber() 
										  + ">> " 
										  + elements[i].getMethodName() + "()");
							}
						
						}
						
						getWizard().setBackButtonEnabled(true);
					}

            }
        };

        oscarSim.start();
    }   
            
}
