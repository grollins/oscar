package net.geoffrollins.oscar;
import com.nexes.wizard.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;


public class MDPanel extends JPanel {
   
    
    public MDPanel( ) {
     
        super();
                 
        contentPanel = getContentPanel();
        contentPanel.setBorder(new EmptyBorder(new Insets(10, 10, 10, 10)));
		contentPanel.setBackground( Color.decode("0xD0E6FF") );
		Font headerFont = new Font("Arial", Font.BOLD, 24);

        ImageIcon icon = getImageIcon();
        
        titlePanel = new javax.swing.JPanel();
        textLabel = new javax.swing.JLabel();
        iconLabel = new javax.swing.JLabel();
        separator = new javax.swing.JSeparator();

        setLayout(new java.awt.BorderLayout());

        titlePanel.setLayout(new java.awt.BorderLayout());
        titlePanel.setBackground(Color.decode("0x3C8CE4"));
        
        textLabel.setBackground(Color.decode("0x3C8CE4"));
        textLabel.setForeground(Color.white);
        textLabel.setFont(headerFont);
        textLabel.setText("MD Simulation Setup");
        textLabel.setBorder(new EmptyBorder(new Insets(10, 10, 10, 10)));
        textLabel.setOpaque(true);

        iconLabel.setBackground(Color.white);
        if (icon != null)
            iconLabel.setIcon(icon);
        
        titlePanel.add(textLabel, BorderLayout.CENTER);
        titlePanel.add(iconLabel, BorderLayout.EAST);
        titlePanel.add(separator, BorderLayout.SOUTH);
        
        add(titlePanel, BorderLayout.NORTH);
        JPanel secondaryPanel = new JPanel();
        secondaryPanel.add(contentPanel, BorderLayout.NORTH);
        secondaryPanel.setBackground(Color.decode("0xD0E6FF"));
        add(secondaryPanel, BorderLayout.CENTER);

    }  
    
    
    public String 
    getIntegrationMethodSelected() 
    {
        return (String)integrationComboBox.getSelectedItem();
    }
    
    public String 
    getTimeStepText()
    {
    	return timeStepTextField.getText();
    }
    
    public String 
    getNumberStepsText()
    {
    	return numberStepsTextField.getText();
    }
    
    public String 
    getTemperatureText()
    {
    	return temperatureTextField.getText();
    }
    
    public String getBoxLengthText()
    {
    	return boxLengthTextField.getText();
    }
    
    public String getNumDimensions()
    {
    	return (String)dimensionComboBox.getSelectedItem();
    }
    
    private JPanel 
    getContentPanel() 
    {     
        Font titleFont = new Font("Arial", Font.PLAIN, 18);
        Font labelFont = new Font("Arial", Font.BOLD, 16);
        Font labelFont2 = new Font("Arial", Font.PLAIN, 16);
        
        JPanel contentPanel1 = new JPanel();
        welcomeTitle = new JLabel();
        blankSpace = new JLabel();
		jPanel1 = new javax.swing.JPanel();

        integrationLabel = new javax.swing.JLabel();
        timeStepLabel = new javax.swing.JLabel();
        numberStepsLabel = new javax.swing.JLabel();
        temperatureLabel = new javax.swing.JLabel();
        boxLengthLabel = new javax.swing.JLabel();
        timeStepTextField = new javax.swing.JTextField();
        numberStepsTextField = new javax.swing.JTextField();
        temperatureTextField = new javax.swing.JTextField();
        boxLengthTextField = new javax.swing.JTextField();
        timeStepLabel2 = new javax.swing.JLabel();
        numberStepsLabel2 = new javax.swing.JLabel();
        temperatureLabel2 = new javax.swing.JLabel();
        boxLengthLabel2 = new javax.swing.JLabel();	
        dimensionComboBox = new javax.swing.JComboBox();
        dimensionLabel = new javax.swing.JLabel();
        integrationComboBox = new javax.swing.JComboBox();
		
        integrationLabel.setFont(labelFont);
        integrationLabel.setText("Integration Method");

        timeStepLabel.setFont(labelFont);
        timeStepLabel.setText("Time Step");

        numberStepsLabel.setFont(labelFont);
        numberStepsLabel.setText("Number of Steps");

        temperatureLabel.setFont(labelFont);
        temperatureLabel.setText("Initial Temperature");

        boxLengthLabel.setFont(labelFont);
        boxLengthLabel.setText("Box Side Length");

        timeStepTextField.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        timeStepTextField.setText("0.001");
		timeStepTextField.setFont(labelFont2);
        numberStepsTextField.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        numberStepsTextField.setText("100000");
		numberStepsTextField.setFont(labelFont2);
        temperatureTextField.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        temperatureTextField.setText("300.0");
		temperatureTextField.setFont(labelFont2);

        boxLengthTextField.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        boxLengthTextField.setText("0");
		boxLengthTextField.setFont(labelFont2);

		integrationComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Default", "Velocity Verlet" }));
        integrationComboBox.setSelectedIndex(0);
        integrationComboBox.setFont( labelFont2 );

        timeStepLabel2.setText("picoseconds");
        numberStepsLabel2.setText("steps");
        temperatureLabel2.setText("K");
        boxLengthLabel2.setText("angstroms");
        
        timeStepLabel2.setFont( labelFont2 );
		numberStepsLabel2.setFont( labelFont2 );
        temperatureLabel2.setFont( labelFont2 );
        boxLengthLabel2.setFont( labelFont2 );

		dimensionComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "1", "2", "3" }));
        dimensionComboBox.setSelectedIndex(1);
        dimensionComboBox.setFont( labelFont2 );
        
        dimensionLabel.setFont( labelFont );
        dimensionLabel.setText( "Number of Dimensions" );

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, integrationLabel)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, timeStepLabel)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, numberStepsLabel)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, temperatureLabel)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, boxLengthLabel)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING,
                    dimensionLabel))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, boxLengthTextField)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, temperatureTextField)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, numberStepsTextField)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, integrationComboBox)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, timeStepTextField)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, dimensionComboBox))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(numberStepsLabel2)
                    .add(timeStepLabel2)
                    .add(boxLengthLabel2)
                    .add(temperatureLabel2)
                                )
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(integrationLabel)
                    .add(integrationComboBox))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(timeStepLabel)
                    .add(timeStepLabel2)
                    .add(timeStepTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(numberStepsLabel)
                    .add(numberStepsTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(numberStepsLabel2))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(temperatureLabel)
                    .add(temperatureTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(temperatureLabel2))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(boxLengthLabel)
                    .add(boxLengthTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(boxLengthLabel2))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)    
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(dimensionLabel)
                    .add(dimensionComboBox, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        
        contentPanel1.setLayout(new java.awt.BorderLayout());

        jPanel1.add(blankSpace);
		jPanel1.setBackground( Color.decode("0xD0E6FF") );

        contentPanel1.add(jPanel1, java.awt.BorderLayout.CENTER);
        contentPanel1.setBackground( Color.decode("0xD0E6FF") );
        
        return contentPanel1;
    }
    
    private ImageIcon 
    getImageIcon() 
    {
        
        //  Icon to be placed in the upper right corner.
        
        return null;
    }
    

	private javax.swing.JLabel boxLengthLabel;
    private javax.swing.JLabel boxLengthLabel2;
    private javax.swing.JTextField boxLengthTextField;
    private javax.swing.JLabel integrationLabel;
    private javax.swing.JTextField timeStepTextField;
    private javax.swing.JLabel numberStepsLabel;
    private javax.swing.JLabel numberStepsLabel2;
    private javax.swing.JTextField numberStepsTextField;
    private javax.swing.JLabel temperatureLabel;
    private javax.swing.JLabel temperatureLabel2;
    private javax.swing.JTextField temperatureTextField;
    private javax.swing.JLabel timeStepLabel;
    private javax.swing.JLabel timeStepLabel2;
    private javax.swing.JLabel blankSpace;
	private javax.swing.JPanel jPanel1;
	private javax.swing.JComboBox dimensionComboBox;
	private javax.swing.JLabel dimensionLabel;
	private javax.swing.JComboBox integrationComboBox;

    private javax.swing.JLabel welcomeTitle;
    private JPanel contentPanel;
    private JLabel iconLabel;
    private JSeparator separator;
    private JLabel textLabel;
    private JPanel titlePanel;    
    
}
