/*****************************************************************************
    ConjugateGradientMinimization.java

    Last Updated   - January 23, 2008
    Updated By     - Geoff Rollins

    Energy minimization, conjugate gradient algorithm
    
    Conjugate Gradient:
    0. The search vector in the first step is the negative gradient of potential
    	energy (i.e. force), the same as it is in steepest descent
    1. Compute scalar constant, gamma
    	gamma = (-current force DOT -current force)/(-previous Force DOT -previous Force)
    2. Compute search vector, V
    	V = current force + gamma * previous V
    3. Compute new position for atom
    	new position = current position + V_k
    
    								

*****************************************************************************/

/*****************************************************************************
    Package and import statments
*/

package net.geoffrollins.oscar;

import java.util.LinkedList;
import java.util.ListIterator;
import java.util.HashMap;
import com.braju.format.*;

/*****************************************************************************
    Class Definitions
*/

/*============================================================================
    One-line summary of class goal
 ............................................................................

    Description of class

============================================================================*/
public class ConjugateGradientMinimization
    extends AbstractSimAlgorithm 
    {
    
    /*====================================================================
        Constants
    */
    public static double GAS_CONSTANT = 0.836; //gas constant = 2 cal/mol*K  =  0.836 CEU/mol*K
    /*--------------------------------------------------------------------
        Constructors
    --------------------------------------------------------------------*/

    /** 
    *	Default class constructor.
    */
    public ConjugateGradientMinimization( )
        {
        	setOldAtomPositions( new HashMap() );
        	setOldForces( new HashMap() );
        	setOldSearchVectors( new HashMap() );
			setStepSize( 0.1 );
			setConvergenceCriterion( 0.0001 );
			setID( SteepestDescentMinimization.EM_IDENTIFIER );

        }

    /** 
    *	Class constructor.
    */
    public ConjugateGradientMinimization( double stepSize, double convCrit )
        {
        	setOldAtomPositions( new HashMap() );
        	setOldForces( new HashMap() );
        	setOldSearchVectors( new HashMap() );
        	setStepSize( stepSize );
			setConvergenceCriterion( convCrit );
			setID( SteepestDescentMinimization.EM_IDENTIFIER );

		}


    /*====================================================================
        Public Member Functions
    */

	/** 
    *	Performs one step of minimization
    *
    *	@param	molSystem
    *	@param	hamiltonian
    *	@param	stepNumber
    *	@return the energy of the system at this step
    */
    public String 
    simStep( MolecularSystem molSystem, Hamiltonian hamiltonian, int stepNumber, int numDimensions )
    {
    	ListIterator atomList;
    	Atom thisAtom = null;
    	
    	// 1. Initialize force on each atom to zero
    	atomList = molSystem.iterateAtoms();
    	while( atomList.hasNext() )
    	{
    		thisAtom = (Atom)atomList.next();
    		thisAtom.zeroForce();
    	}
    	
    	// 2. Calculate force on each atom and save 
    	hamiltonian.calculateForce();
    	saveForces( molSystem );
    	
    	// 3. Save atomic coordinates
    	saveAtomPositions( molSystem );
    	
    	// 4. Calculate energy of current configuration
    	double oldPotentialEnergy = calculatePotentialEnergy( hamiltonian );
    	
    	//Gamma will be zero if this is the first step
    	boolean isFirstStep = false;
    	if( stepNumber == 1 )
    	{
    		isFirstStep = true;
    	}
    	else
    	{
    		isFirstStep = false;
    	}
    	
    	// 5. Advance atoms to trial positions
    	atomList = molSystem.iterateAtoms(); //reset iterator to the start of the atom list
    	double[] searchVector = null;
    	
    	if( stepNumber % calculateDegreesOfFreedom(molSystem.numAtoms(), numDimensions) == 0 )
    	{
    		//System.out.print( "Ran SD at step " );
    		//System.out.println( stepNumber );
    		
    		//Do one step of Steepest Descent every <degrees of freedom> steps
    		while( atomList.hasNext() )
			{
				thisAtom = (Atom)atomList.next();
				
				double[] thisForce = thisAtom.getForce();
				double[] thisPosition = thisAtom.getPosition();
				
				double xTrial = thisPosition[0] + thisForce[0] * getStepSize(); // x
				double yTrial = thisPosition[1] + thisForce[1] * getStepSize(); // y
				double zTrial = thisPosition[2] + thisForce[2] * getStepSize(); // z
				double[] trialPosition = { xTrial, yTrial, zTrial };
				
				thisAtom.setPosition( trialPosition );
				double[] newSearchVector = { thisForce[0], thisForce[1], thisForce[2] };
				
				searchVector = newSearchVector;
			}
    	}
    	
    	else
    	{
    		//Conjugate Gradient
			while( atomList.hasNext() )
			{
				thisAtom = (Atom)atomList.next();
				
				double[] oldForce = getOldForce( thisAtom );
				double[] oldSearchVector = getOldSearchVector( thisAtom );
				double[] thisForce = thisAtom.getForce();
				double[] thisPosition = thisAtom.getPosition();
				
				double gamma = computeGamma( isFirstStep, thisForce, oldForce );
				searchVector = computeSearchVector( gamma, oldSearchVector, thisForce );
				
				double xTrial = thisPosition[0] + searchVector[0] * getStepSize(); // x
				double yTrial = thisPosition[1] + searchVector[1] * getStepSize(); // y
				double zTrial = thisPosition[2] + searchVector[2] * getStepSize(); // z
				double[] trialPosition = { xTrial, yTrial, zTrial };
				
				thisAtom.setPosition( trialPosition );
			}
    	}
    	// 6. Calculate energy of the trial configuration
    	double trialPotentialEnergy = calculatePotentialEnergy( hamiltonian );
    	
    	// 7. Compare energy of the trial configuration with that of the previous configuration
    	if( trialPotentialEnergy <= oldPotentialEnergy )
    	{
    		//Keep trial positions
    			//nothing extra needs to be done since the atoms are already in the trial positions 
    		
    		//Check for convergence
    		if( Math.abs((oldPotentialEnergy - trialPotentialEnergy) / oldPotentialEnergy) < getConvergenceCriterion() )
    		{
    			setIsConverged( true );
    		}
    		
    		saveSearchVector( thisAtom, searchVector );
    		setStepSize( getStepSize() * 1.2 );
    	}
    	else
    	{
    		//Restore old positions
    		restoreAtomPositions( molSystem );
    		//Cut the step size in half for the next step
    		setStepSize( getStepSize()/2 );
    	}
    
    	// Calculate energies and create energy output string
    	String outputString = formatEnergy( molSystem, hamiltonian, stepNumber );
    	
    	// Return energy output string
    	return outputString;
    
    }

	/*-----------------------------------------------------------------
	*	Accessors
	*--------------------------------------------------------------------*/
    public HashMap
    getOldAtomPositions(  )
    {
    	return myOldAtomPositions;
    	
    }
    
    public void
    setOldAtomPositions( HashMap newOldAtomPositions )
        {
			myOldAtomPositions = newOldAtomPositions;
        }
    
    public HashMap
    getOldForces(  )
    {
    	return myOldForces;
    	
    }
    
    public void
    setOldForces( HashMap newOldForces )
        {
			myOldForces = newOldForces;
        }
    
    public HashMap
    getOldSearchVectors(  )
    {
    	return myOldSearchVectors;
    	
    }
    
    public void
    setOldSearchVectors( HashMap newOldSearchVectors )
        {
			myOldSearchVectors = newOldSearchVectors;
        }
    
    public double
    getStepSize( )
        {
			return myStepSize;
        }

	public void
    setStepSize( double newStepSize )
        {
			myStepSize = newStepSize;
        }

	public double
    getConvergenceCriterion( )
        {
			return myConvergenceCriterion;
        }

	public void
    setConvergenceCriterion( double newConvergenceCriterion )
        {
			myConvergenceCriterion = newConvergenceCriterion;
        }
			
	public double
    getTemperature( )
    {
    	//temperature is not defined in energy minimization
    	return 0.0;
    }
	public void
    setTemperature( double newTemperature )
        {
        	//no need for temperature in minimization but we have to define
        	//this stub function in order to satisfy the requirements of the
        	//abstract parent class (AbstractSimAlgorithm)
			return;
        }

	public boolean
    getIsConverged( )
    {
    	return myIsConverged;
    }
    
	public void
    setIsConverged( boolean newIsConverged )
        {
			myIsConverged = newIsConverged;
        }

	public String formatEnergy( MolecularSystem molSystem, Hamiltonian hamiltonian, int stepNumber )
	{
		String outputString = "%8d     ";
		Parameters outputStringParameters = new Parameters( stepNumber );
		double potentialEnergy = 0.00;
		
		// Sum potential energy terms
		LinkedList energyList = hamiltonian.calculateEnergy( );
		ListIterator energyIterator = energyList.listIterator( 0 );
		double thisEnergy;
		while( energyIterator.hasNext() )
		{
			thisEnergy = (Double)energyIterator.next();
			// For output, convert energy from CEU to kcal/mol
			thisEnergy = thisEnergy / AbstractEnergyTerm.CEU_CONVERSION_FACTOR;
			
			
			outputString += "%10.4f     ";
			outputStringParameters = outputStringParameters.add( thisEnergy );
			
			potentialEnergy += thisEnergy;
		}
		
		// Add potential and step size to the output string
		outputString += "%10.4f     %9.12f \n";
		// Add potential and step size to the list of output parameters
		outputStringParameters.add( potentialEnergy ).add( getStepSize() );

		//System.out.println( outputString );
		return Format.sprintf( outputString, outputStringParameters );		
		
	}
	
	public String
	getEnergyBanner( Hamiltonian hamiltonian )
	{
		String bannerString = "%8s     ";
		Parameters bannerStringParameters = new Parameters( "Step" );
		
		LinkedList interactionList = hamiltonian.getInteractionNames( );
		ListIterator interactionIterator = interactionList.listIterator( 0 );
		String thisInteraction;
		while( interactionIterator.hasNext() )
		{
			thisInteraction = (String)interactionIterator.next();
			
			
			bannerString += "%10s     ";
			bannerStringParameters = bannerStringParameters.add( thisInteraction );
		}
		
		// Add potential, kinetic, total, and temp to the banner string
		bannerString += "%10s     %9s \n";
		bannerStringParameters.add( "Potential" ).add( "Step Size" );

		return Format.sprintf( bannerString, bannerStringParameters );	
	}
	
	/**
	*	If the convergence criterion has been satisfied, we can
	*	quit the simulation early.
	*
	*/
	public boolean isTimeToQuit()
	{
		if( getConvergenceCriterion() > 0.0 )
		{
			return getIsConverged();
		}
		// if the convergence criterion is not greater than zero, we're going to ignore it and let the minimization continue for the full step count specified by the user
		else
		{
			return false;
		}
	}
	
	/*====================================================================
        Private Member Functions
    */
    
    private void
	saveAtomPositions( MolecularSystem molSystem )
	{
		ListIterator atomList = molSystem.iterateAtoms();
		while( atomList.hasNext() )
		{
			Atom thisAtom = (Atom)atomList.next();
			double oldX = thisAtom.getX();
			double oldY = thisAtom.getY();
			double oldZ = thisAtom.getZ();
			double[] savedCoords = { oldX, oldY, oldZ };
			getOldAtomPositions().put( thisAtom, savedCoords );
		}
		
	}
	
	/** 
    *	Restore the atoms to their previous postitions
    */
	private void
	restoreAtomPositions( MolecularSystem molSystem )
	{
		ListIterator atomList = molSystem.iterateAtoms();
		Atom thisAtom;
		while( atomList.hasNext() )
		{
			thisAtom = (Atom)atomList.next();
			double[] oldPosition = (double[])getOldAtomPositions().get( thisAtom );
			thisAtom.setPosition( oldPosition );
		}
	}
    
    /** 
    *	Save current atomic positions
    */
	private void
	saveForces( MolecularSystem molSystem )
	{
		ListIterator atomList = molSystem.iterateAtoms();
		while( atomList.hasNext() )
		{
			Atom thisAtom = (Atom)atomList.next();
			double[] thisForce = thisAtom.getForce();
			double xForce = thisForce[0];
			double yForce = thisForce[1];
			double zForce = thisForce[2];
			double[] savedForce = { xForce, yForce, zForce };
			getOldForces().put( thisAtom, savedForce );
		}
		
	}

	private double[]
	getOldForce( Atom thisAtom )
	{
		return (double[])getOldForces().get( thisAtom );
	}

	private void
	saveSearchVector( Atom thisAtom, double[] searchVector )
	{
		double xSearch = searchVector[0];
		double ySearch = searchVector[1];
		double zSearch = searchVector[2];
		double[] savedSearchVector = { xSearch, ySearch, zSearch };
		getOldSearchVectors().put( thisAtom, savedSearchVector );
	}
	
	private double[]
	getOldSearchVector( Atom thisAtom )
	{
		double[] searchVector = (double[])getOldSearchVectors().get( thisAtom );
		return searchVector;
	}

	private double
	calculatePotentialEnergy( Hamiltonian hamiltonian )
	{
		LinkedList energyList = hamiltonian.calculateEnergy( );
		ListIterator energyIterator = energyList.listIterator( 0 );
		double potentialEnergy = 0.0;
		double thisEnergy;
		while( energyIterator.hasNext() )
		{
			thisEnergy = (Double)energyIterator.next();
			// For output, convert energy from CEU to kcal/mol
			thisEnergy = thisEnergy / AbstractEnergyTerm.CEU_CONVERSION_FACTOR;			potentialEnergy += thisEnergy;
		}
		
		return potentialEnergy;
	}
	
	/** 
    *	gamma = (-current force DOT -current force) / 
    *			(-previous Force DOT -previous Force)
    *
    *		  = ||-current force|| / ||-previous force ||
    *
    *	gamma = 0 if this is the first step
    */
	private double
	computeGamma( boolean isFirstStep, double[] currentForce, double[] oldForce )
	{
		if( (isFirstStep == true) || (oldForce == null) )
		{
			return 0.0;
		}
		
		else
		{
			double magnitudeCurrentForce = ( Math.pow(currentForce[0],2) + Math.pow(currentForce[1],2) + Math.pow(currentForce[2],2) );
			
			double magnitudeOldForce = ( Math.pow(oldForce[0],2) + Math.pow(oldForce[1],2) + Math.pow(oldForce[2],2) );
			
			return magnitudeCurrentForce / magnitudeOldForce;
		}
	}
	
	/** 
    *	V = current force + gamma * previous V
    */
	private double[] 
	computeSearchVector( double gamma, double[] previousSearchVector, double[] currentForce )
	{
		double xSearch = currentForce[0];
		double ySearch = currentForce[1];
		double zSearch = currentForce[2];
	
		if( previousSearchVector != null )
		{
			xSearch += (gamma * previousSearchVector[0]);
			ySearch += (gamma * previousSearchVector[1]);
			zSearch += (gamma * previousSearchVector[2]);
		}
	
		double[] searchVector = { xSearch, ySearch, zSearch };
		return searchVector;
	}
	
	private int
	calculateDegreesOfFreedom( int numAtoms, int numDimensions )
	{
		return numAtoms * numDimensions;
	}
	
    /*====================================================================
        Private Data Members
    */
    private HashMap myOldAtomPositions;
	private HashMap myOldForces;
	private HashMap myOldSearchVectors;
    private double myStepSize;
    private double myConvergenceCriterion;
    private boolean myIsConverged;
    }

