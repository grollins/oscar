/*****************************************************************************
    BoundingBox.java

    Last Updated   - January 21, 2008
    Updated By     - Geoff Rollins

*****************************************************************************/

/*****************************************************************************
    Package and import statments
*/

package net.geoffrollins.oscar;

/*****************************************************************************
    Class Definitions
*/

/*============================================================================
    Bounding box. A restraining force prevents atoms from penetrating the walls of the box.
============================================================================*/
public class BoundingBox 
extends AbstractEnergyTerm
{
	static final double BOX_FORCE_CONST = 4180.0; // 10 kcal/(mol*A^2) = 4180 CEU/(mol*A^2)
	
	/** 
    *	Default class constructor.
    */
    public BoundingBox( )
        {
			setAtoms( new Atom[1] ); //allocates memory for the two atoms joined by this term
			setBoxLength( 10000.0 );
			setName( "Box" );
        }

	/** 
    *	Class constructor.
    */
    public BoundingBox( Atom[] atoms, double boxLength  )
        {
        	setAtoms( atoms );
        	setBoxLength( boxLength );
        	setName( "Box" );
        }

    
    /*====================================================================
        Public Member Functions
    */
    
    public double
    calculateDeltaX()
    {
    	Atom[] atoms = getAtoms();
    	return Math.abs(atoms[0].getX()) - (getBoxLength()/2); 
    }

	public double
	calculateDeltaY()
	{
		Atom[] atoms = getAtoms();
		return Math.abs(atoms[0].getY()) - (getBoxLength()/2);
    }
    
    public double
    calculateDeltaZ()
    {
    	Atom[] atoms = getAtoms();
    	return Math.abs(atoms[0].getZ()) - (getBoxLength()/2);
    }

    
    /**-----------------------------------------------------------------
    *	Stub function
	*
	*	@return
	--------------------------------------------------------------------*/
    public double
    calculateSeparation()
    {
    	return 0.0;
    	
    }
    
	/**-----------------------------------------------------------------
    *	Calculate the energy of this bounding box potential.
	*	E = 0.5 * BOX_FORCE_CONST * dx^2 
	*		where dx is the magnitude of the penetration past the box wall
	*		in the x direction
	*
	*	@return			energy of this box term
	--------------------------------------------------------------------*/
    public double
    calculateEnergy( )
    {
  		double deltaX = calculateDeltaX();
    	double deltaY = calculateDeltaY();
    	double deltaZ = calculateDeltaZ();
    	double boxEnergy = 0.0;
    	
    	if( deltaX > 0.0 )
    	{
    		boxEnergy += 0.5 * BOX_FORCE_CONST * Math.pow(deltaX,2);	
    	}
    	
    	if( deltaY > 0.0 )
    	{
    		boxEnergy += 0.5 * BOX_FORCE_CONST * Math.pow(deltaY,2);	
    	}
    	
    	if( deltaZ > 0.0 )
    	{
    		boxEnergy += 0.5 * BOX_FORCE_CONST * Math.pow(deltaZ,2);	
    	}
    	
    	return boxEnergy;
    }

	/**-----------------------------------------------------------------
    *	Calculate the force of this box term. Add this force to atoms' current
    *	force values.
    *
	*	F = -sign * BOX_FORCE_CONST * dx
	*		where dx is the magnitude of the penetration past the box wall
	*		in the x direction and sign is 1 if the atom's x coordinate
	*		is greater than or equal to 0 or -1 if less than 0
	--------------------------------------------------------------------*/
	public double
    calculateForce()
    {
    	Atom[] atoms = getAtoms();
    	
    	double deltaX = calculateDeltaX();
    	double deltaY = calculateDeltaY();
    	double deltaZ = calculateDeltaZ();
    	double xForce = 0.0;
    	double yForce = 0.0;
    	double zForce = 0.0;
    	
    	if( deltaX > 0.0 )
    	{
    		xForce = -getSign(atoms[0].getX()) * BOX_FORCE_CONST * deltaX;	
    	}
    	
    	if( deltaY > 0.0 )
    	{
    		yForce = -getSign(atoms[0].getY()) * BOX_FORCE_CONST * deltaY;
    	}
    	
    	if( deltaZ > 0.0 )
    	{
    		zForce = -getSign(atoms[0].getZ()) * BOX_FORCE_CONST * deltaZ;
    	}
    
    	double[] forceOnAtom0 = {xForce, yForce, zForce};

    	
    	atoms[0].addForce( forceOnAtom0 );
    	
    	return 0.0;
    	
    }

	public String
    toString( )
    {
	    Atom[] atoms = getAtoms();
	    return atoms[0].getSerialNumber() + "\t" + getBoxLength();
    }

	/**-----------------------------------------------------------------
	*	Accessors
	*--------------------------------------------------------------------*/
	public double
    getBoxLength( )
        {
			return myBoxLength;
        }

    public void
    setBoxLength( double newBoxLength )
        {
			myBoxLength = newBoxLength;
        } 

    /*====================================================================
        Private Member Functions
    */
    private int
    getSign( double coord )
    {
    	if( coord < 0.0)
    		return -1;
    	else
    		return 1;
    	
    }
	

    /*====================================================================
        Private Data Members
    */
	private double myBoxLength;
	
    }

