/*****************************************************************************
    MetropolisMonteCarlo.java

    Last Updated   - January 31, 2008
    Updated By     - Geoff Rollins

    Metropolis Monte Carlo
    
    1. Randomly select an atom.
   	2. Make a random change to the selected atom's coordinates 
   		(up to the maximum displacement)
    3. If the trial coordinates reduce the energy of the system...
    	a. save the trial coordinates
    4. If the trial coordinates increase the energy of the system...
    	a. compute Boltzmann factor ( e^(-(trial energy - old energy)/kT) )
    	b. if the Boltmann factor is greater than or equal to a randomly
    		selected number between zero and one, accept the trial coordinates
    	c. if the Boltzmann factor is less than the random number, restore the
    		old coordinates
    5. Check if too many or too few moves are being accepted and change the
    	maximum displacement if necessary
    

*****************************************************************************/

/*****************************************************************************
    Package and import statments
*/

package net.geoffrollins.oscar;

import java.util.LinkedList;
import java.util.ListIterator;
import java.util.HashMap;
import java.util.Random;
import com.braju.format.*;

/*****************************************************************************
    Class Definitions
*/

/*============================================================================
    One-line summary of class goal
 ............................................................................

    Description of class

============================================================================*/
public class MetropolisMonteCarlo
    extends AbstractSimAlgorithm 
    {
    
    /*====================================================================
        Constants
    */
    public static double GAS_CONSTANT = 0.830936; //gas constant = 2 cal/mol*K  =  0.830936 CEU/mol*K
    public static double GAS_CONSTANT_KCAL = 0.001987; // kcal/mol*K
    public static final String MC_IDENTIFIER = "Monte Carlo";
    /*--------------------------------------------------------------------
        Constructors
    --------------------------------------------------------------------*/

    /** 
    *	Default class constructor.
    */
    public MetropolisMonteCarlo( )
        {
        	myOldAtomPosition = new double[3];
			setMaxDisplacement( 1.0 );
			setNumMoves( 0 );
			setNumAccepted( 0 );
			setTemperature( 0.0 );
			setAcceptanceInterval( 100 );
			setRandomGenerator( new Random() );
			setID( MC_IDENTIFIER );
        }

    /** 
    *	Class constructor.
    */
    public MetropolisMonteCarlo( double maxDisplacement )
        {
			myOldAtomPosition = new double[3];
        	setMaxDisplacement( maxDisplacement );
			setNumMoves( 0 );
			setNumAccepted( 0 );
			setTemperature( 0.0 );
			setAcceptanceInterval( 100 );
			setRandomGenerator( new Random() );
			setID( MC_IDENTIFIER );
		}


    /*====================================================================
        Public Member Functions
    */

	/** 
    *	Performs one step of minimization
    *
    *	@param	molSystem
    *	@param	hamiltonian
    *	@param	stepNumber
    *	@return the energy of the system at this step
    */
    public String 
    simStep( MolecularSystem molSystem, Hamiltonian hamiltonian, int stepNumber, int numDimensions )
    {
    	LinkedList atomList = molSystem.getAtoms();
    	
    	// 1. Randomly choose an atom
    	Atom selectedAtom = (Atom)atomList.get( getRandomGenerator().nextInt( atomList.size()) );
    	
    	//DEBUG
    	//System.out.print( "Selected atom #" );
    	//System.out.print( selectedAtom.getSerialNumber() );
    	
    	// 2. Save atom's coordinates
    	saveAtomPosition( selectedAtom );
    	
    	// 3. Calculate energy of current configuration
    	double oldPotentialEnergy = calculatePotentialEnergy( hamiltonian );
    	
    	// 4. Advance atom to trial position
    	double[] thisPosition = selectedAtom.getPosition();
    	double xRand = getRandomGenerator().nextDouble();
    	// a value of 0.5 eliminates movement in the y direction
    	double yRand = 0.5;
    	// a value of 0.5 eliminates movement in the z direction
    	double zRand = 0.5;
    	
    	if( numDimensions > 1 )
    	{
    		yRand = getRandomGenerator().nextDouble();
    	}
    	if( numDimensions > 2 )
    	{
    		zRand = getRandomGenerator().nextDouble();
    	}
    	
		double xTrial = thisPosition[0] + (2 * xRand - 1)*getMaxDisplacement();
		double yTrial = thisPosition[1] + (2 * yRand - 1)*getMaxDisplacement();
		double zTrial = thisPosition[2] + (2 * zRand - 1)*getMaxDisplacement();
		double[] trialPosition = { xTrial, yTrial, zTrial };
		
		//DEBUG
		/*
		System.out.print( "  Changed coords to = {" );
		System.out.print( trialPosition[0] );
		System.out.print( ", " );
		System.out.print( trialPosition[1] );
		System.out.print( ", " );
		System.out.print( trialPosition[2] );
		System.out.print( "} at step " );
		System.out.println( stepNumber );		
		*/
		selectedAtom.setPosition( trialPosition );
    	
    	// 5. Calculate energy of the trial configuration
    	double trialPotentialEnergy = calculatePotentialEnergy( hamiltonian );
    	
    	// 6. Compare energy of the trial configuration with that of the previous configuration
    	if( trialPotentialEnergy <= oldPotentialEnergy )
    	{
    		//Keep trial positions
    			//nothing extra needs to be done since the atom is already in
    			//	the trial position 
    		setNumAccepted( getNumAccepted() + 1 );
    	}
    	else
    	{
    		//Compute Boltzmann factor
    		double boltzmannFactor = Math.exp( -(trialPotentialEnergy-oldPotentialEnergy)/(GAS_CONSTANT_KCAL * getTemperature()) );
    		
    		if( getRandomGenerator().nextDouble() < boltzmannFactor )
    		{
    			//accept move
    			setNumAccepted( getNumAccepted() + 1 );
    		}
    		else
    		{
    			//reject move
    			restoreAtomPosition( selectedAtom );
    		}
    		
    		
    	}
    	
    	setNumMoves( getNumMoves() + 1 );
    
    	// Check our acceptance rate
    	if( stepNumber % getAcceptanceInterval() == 0 )
    	{
    		calibrateMaxDisplacement();
    	}
    	
    	// Calculate force on each atom. We don't actually use forces during a Monte Carlo simulation. The only reason we're doing this calculation is to obtain the virial for calculating pressure.
    	hamiltonian.calculateForce();
    	
    	// Calculate energies and create energy output string
    	String outputString = formatEnergy( molSystem, hamiltonian, stepNumber );
    	
    	// Return energy output string
    	return outputString;
    }

	/*-----------------------------------------------------------------
	*	Accessors
	*--------------------------------------------------------------------*/
    public double[]
    getOldAtomPosition(  )
    {
    	return myOldAtomPosition;
    	
    }
    
    public void
    setOldAtomPosition( double[] newOldAtomPosition )
        {
			myOldAtomPosition[0] = newOldAtomPosition[0];
			myOldAtomPosition[1] = newOldAtomPosition[1];
			myOldAtomPosition[2] = newOldAtomPosition[2];
        }
    
    public double
    getMaxDisplacement( )
        {
			return myMaxDisplacement;
        }

	public void
    setMaxDisplacement( double newMaxDisplacement )
        {
			myMaxDisplacement = newMaxDisplacement;
        }

	public int
    getNumMoves( )
        {
			return myNumMoves;
        }

	public void
    setNumMoves( int newNumMoves )
        {
			myNumMoves = newNumMoves;
        }

	public int
    getNumAccepted( )
        {
			return myNumAccepted;
        }

	public void
    setNumAccepted( int newNumAccepted )
        {
			myNumAccepted = newNumAccepted;
        }
			
	public double
    getTemperature( )
        {
			return myTemperature;
        }		
	
	public void
    setTemperature( double newTemperature )
        {
			myTemperature = newTemperature;
        }
        
    public int
    getAcceptanceInterval( )
        {
			return myAcceptanceInterval;
        }		
	
	public void
    setAcceptanceInterval( int newAcceptanceInterval )
        {
			myAcceptanceInterval = newAcceptanceInterval;
        }
        
	public Random
    getRandomGenerator( )
        {
			return myRandomGenerator;
        }		
	
	public void
    setRandomGenerator( Random newRandomGenerator )
        {
			myRandomGenerator = newRandomGenerator;
        }

	public String formatEnergy( MolecularSystem molSystem, Hamiltonian hamiltonian, int stepNumber )
	{
		String outputString = "%8f     ";
		Parameters outputStringParameters = new Parameters( stepNumber );
		double potentialEnergy = 0.00;
		
		// Sum potential energy terms
		LinkedList energyList = hamiltonian.calculateEnergy( );
		ListIterator energyIterator = energyList.listIterator( 0 );
		double thisEnergy;
		while( energyIterator.hasNext() )
		{
			thisEnergy = (Double)energyIterator.next();
			// For output, convert energy from CEU to kcal/mol
			thisEnergy = thisEnergy / AbstractEnergyTerm.CEU_CONVERSION_FACTOR;
			
			
			outputString += "%10.4f     ";
			outputStringParameters = outputStringParameters.add( thisEnergy );
			
			potentialEnergy += thisEnergy;
		}
		
		// Add potential and step size to the output string
		outputString += "%10.4f     %9.3f     %9.3f     %9.5f \n";
		
		LinkedList boxTerms = hamiltonian.getEnergyTerms().getInteractionList( "Box" );
		double pressure = 0.0;
		if( boxTerms == null )
		{
			pressure = 0.0;
		}
		else
		{
			BoundingBox box = (BoundingBox)boxTerms.get( 0 );
			double boxLength = box.getBoxLength();
			pressure = calculatePressure(hamiltonian.getCurrentVirial(), molSystem.numAtoms(), boxLength);
		}
		
		// Add potential, temperature, pressure and step size to the list of output parameters
		outputStringParameters.add( potentialEnergy ).add( getTemperature() ).add( pressure ).add( getMaxDisplacement() );

		//System.out.println( outputString );
		return Format.sprintf( outputString, outputStringParameters );		
		
	}
	
	public String
	getEnergyBanner( Hamiltonian hamiltonian )
	{
		String bannerString = "%8s     ";
		Parameters bannerStringParameters = new Parameters( "Step" );
		
		LinkedList interactionList = hamiltonian.getInteractionNames( );
		ListIterator interactionIterator = interactionList.listIterator( 0 );
		String thisInteraction;
		while( interactionIterator.hasNext() )
		{
			thisInteraction = (String)interactionIterator.next();
			
			
			bannerString += "%10s     ";
			bannerStringParameters = bannerStringParameters.add( thisInteraction );
		}
		
		// Add potential, kinetic, total, and temp to the banner string
		bannerString += "%10s     %9s     %9s     %9s \n";
		bannerStringParameters.add( "Potential" ).add( "Temperature" ).add( "Pressure" ).add( "Max Disp." );

		return Format.sprintf( bannerString, bannerStringParameters );	
	}
	
	public boolean isTimeToQuit()
	{
		return false;
	}
	
	/*====================================================================
        Private Member Functions
    */
    private void
    calibrateMaxDisplacement()
   	{
   		double acceptanceRate = (double)getNumAccepted() / (double)getNumMoves();
   		double currentMaxDisplacement = getMaxDisplacement();
   		
   		if( acceptanceRate < 0.40 )
   		{	// we're not accepting enough moves, maybe our moves are too large
   			setMaxDisplacement( currentMaxDisplacement * 0.5 );
   			
   			//DEBUG
   			//System.out.println( "Changed Max Displacement to " );
   			//System.out.println( currentMaxDisplacement * 0.5 );
   		}
   		else if( acceptanceRate > 0.60 )
   		{
   			// we're accepting too many moves, maybe our moves are too small
   			setMaxDisplacement( currentMaxDisplacement * 1.2 );
   			
   			//DEBUG
   			//System.out.println( "Changed Max Displacement to " );
   			//System.out.println( currentMaxDisplacement * 1.2 );
   		}
   		else
   		{
   			// we're accepting somewhere between 40% and 60% of the moves we make; that's good, so we won't change anything
   			;
   		}
   	
   	}
    
    
    /** 
    *	Save current atom position
    */
	private void
	saveAtomPosition( Atom selectedAtom )
	{	
		double oldX = selectedAtom.getX();
		double oldY = selectedAtom.getY();
		double oldZ = selectedAtom.getZ();
		double[] savedCoords = { oldX, oldY, oldZ };
		setOldAtomPosition( savedCoords );
	}
	
	/** 
    *	Restore the atoms to their previous postitions
    */
	private void
	restoreAtomPosition( Atom selectedAtom )
	{
		selectedAtom.setPosition( getOldAtomPosition() );
	}

	private double
	calculatePotentialEnergy( Hamiltonian hamiltonian )
	{
		LinkedList energyList = hamiltonian.calculateEnergy( );
		ListIterator energyIterator = energyList.listIterator( 0 );
		double potentialEnergy = 0.0;
		double thisEnergy;
		while( energyIterator.hasNext() )
		{
			thisEnergy = (Double)energyIterator.next();
			// For output, convert energy from CEU to kcal/mol
			thisEnergy = thisEnergy / AbstractEnergyTerm.CEU_CONVERSION_FACTOR;			potentialEnergy += thisEnergy;
		}
		
		return potentialEnergy;
	}
	
	private double
    calculatePressure( double virial, int numAtoms, double boxLength )
    {
    	double PV =  (numAtoms * GAS_CONSTANT * getTemperature()) + (0.5 * virial);
    	double pressureCEU = PV / (boxLength * boxLength * boxLength);
    	return pressureCEU * DefaultMD.BAR_CONVERSION_FACTOR;
    }
	
    /*====================================================================
        Private Data Members
    */
	private double[] myOldAtomPosition;
    private double myMaxDisplacement;
	private int myNumMoves;
	private int myNumAccepted;
    private double myTemperature;
    private int myAcceptanceInterval;
    private Random myRandomGenerator;
    }

