/*****************************************************************************
    OscarSimulation.java

    Last Updated   - March 11, 2008
    Updated By     - Geoff Rollins

    An Oscar simulation consists of atoms & molecules, an energy function (Hamiltonian), 
    and an algorithm for advancing the system in time, conformational space, or otherwise

*****************************************************************************/

/*****************************************************************************
    Package and import statments
*/

package net.geoffrollins.oscar;

import java.io.*;
import java.util.Scanner;
import javax.swing.JProgressBar;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Random;
import com.braju.format.*;
import com.jamonapi.*;



/*****************************************************************************
    Class Definitions
*/

/*============================================================================
    One-line summary of class goal
 ............................................................................

    Description of class

============================================================================*/
public class OscarSimulation    
{

	public static final double CEU_CONVERSION_FACTOR = 418.0;
	public static double GAS_CONSTANT = 0.830936; //gas constant = 2 cal/*K  =  0.830936 CEU/mol*K
	public static double AVOGADRO_NUMBER = Math.pow( 6.0221, 23 ); //6.0221 * 10^23/mol
    public static double BOLTZMANN_CONSTANT = GAS_CONSTANT / AVOGADRO_NUMBER; //1.3887 * 10^-24 CEU/K
	public static double COUPLING_CONST = 0.4; //picoseconds
	public static double BAR_CONVERSION_FACTOR = 165.525; //1 CEU/cubic angstrom = 165.525 bar

	/**
	*	Relates a random number (normalDistributionValue) from the normal
	*	distribution to its counterpart from the Maxwell-Boltzmann distribution.
	*
	*	MB Value = (MB average) + (MB std. dev) * (normal distribution value)
	*			 =	0 + sqrt(RT/m) * (normal distribution value)
	*/
	static double
	sampleMaxwellBoltzmann( double normalDistributionValue, double mass, double targetTemperature )
	{
		return normalDistributionValue * Math.sqrt( GAS_CONSTANT*targetTemperature/mass );
	}

    /*--------------------------------------------------------------------
        Constructors
     ...................................................................

       Description of constructors

    --------------------------------------------------------------------*/

    /** 
    *	Default Class constructor.
    */
    public OscarSimulation( )
        {
			setMolecularSystem( new MolecularSystem() );
			setHamiltonian( new Hamiltonian() );
			setSimAlgorithm( null );
        }


    /*====================================================================
        Public Member Functions
    */
	
	/** 
    *	Open a coordinate file (pdb format) and add each of the ATOM records
    *	within to the MolecularSystem
    *
    *	@param	coordinateFileName
    */
	public void
	readCoordinates( String coordinateFileName ) throws Exception
	{
		//1. open file for reading
		File coordinateFile = new File( coordinateFileName );
		if( !coordinateFile.exists() )
		{
			System.err.println( "Coordinate file " + coordinateFileName + " does not exist" );
			System.exit(0);
		}
		
		Scanner coordinateInput = new Scanner(coordinateFile);
		
		//2. read ATOM records
		while( coordinateInput.hasNext() )
		{
			String thisLine = coordinateInput.nextLine();
			Scanner lineReader = new Scanner( thisLine );
			lineReader.useDelimiter( "[\\s]+" ); // one or more spaces
			
			String label = lineReader.next().trim();
			if( !label.equalsIgnoreCase( "ATOM" ) )
			{
				// Skip this line if it doesn't begin with "ATOM"
				continue;
			}
			else
			{
				int i = 0;
				String[] atomParams = new String[13];
				while( lineReader.hasNext() )
				{
					atomParams[i] = lineReader.next().trim();
					i++;
				}
				
				//3. create a new Atom object based on this record
				Atom newAtom = new Atom( atomParams );
				//4. add the new Atom to the MolecularSystem
				getMolecularSystem().addAtom( newAtom );
				
			}
		
		}
		
		//5. close the input file
		coordinateInput.close();
		
	}
	
	/** 
    *	Open a structure file (sf format) and add each of the bonded terms
    *	to the Hamiltonian
    *
    *	@param	structureFileName
    */
	public void
	readStructure( String structureFileName ) throws Exception
	{
		// default vdw parameters
		double VDW_Radius = 3.5; // angstroms
		double wellDepth = 41.8; // CEU
		
		//1. open file for reading
		File structureFile = new File( structureFileName );
		if( !structureFile.exists() )
		{
			//System.out.println( "Structure file " + structureFileName + " does not exist" );
			
			//Add non-bonded energy terms
			addNonBondedTerms(VDW_Radius, wellDepth);
			return;
		}
		
		//Search for VDW entry
		Scanner vdwInput = new Scanner(structureFile);
		while( vdwInput.hasNext() )
		{
			String thisLine = vdwInput.nextLine();
			Scanner lineReader = new Scanner( thisLine );
			lineReader.useDelimiter( "[\\s]+" ); // one or more spaces
			
			String label = lineReader.next().trim();
			if( label.equalsIgnoreCase( "VDW" ) )
			{
					int i = 0;
					String[] vdwParams = new String[2];
					while( lineReader.hasNext() )
					{
						vdwParams[i] = lineReader.next().trim();
						i++;
					}
					
					VDW_Radius = Double.valueOf( vdwParams[0] ); // angstroms
					wellDepth = Double.valueOf( vdwParams[1] ) * CEU_CONVERSION_FACTOR; // CEU
					
			}
			else
			{
				// Skip this line if it's not a VDW entry
				continue;
			}
		}
		// add non-bonded interactions
		addNonBondedTerms(VDW_Radius, wellDepth);
		
		Scanner structureInput = new Scanner(structureFile);
		//2. read a BOND, ANGLE, or TORSION record
		while( structureInput.hasNext() )
		{
			String thisLine = structureInput.nextLine();
			Scanner lineReader = new Scanner( thisLine );
			lineReader.useDelimiter( "[\\s]+" ); // one or more spaces
			
			String label = lineReader.next().trim();
			if( label.equalsIgnoreCase( "BOND" ) )
			{
				int i = 0;
				String[] bondParams = new String[4];
				while( lineReader.hasNext() )
				{
					bondParams[i] = lineReader.next().trim();
					i++;
				}
				
				Atom atom1 = getMolecularSystem().getAtom( Integer.valueOf(bondParams[0]) );
				if( atom1 == null )
				{
					System.out.println( "Can't make bond: No atom with serial number = " + Integer.valueOf(bondParams[0]) + " in the system."	);
					throw new NullPointerException();
				}
				Atom atom2 = getMolecularSystem().getAtom( Integer.valueOf(bondParams[1]) );
				if( atom2 == null )
				{
					System.out.println( "Can't make bond: No atom with serial number = " + Integer.valueOf(bondParams[1]) + " in the system."	);
					throw new NullPointerException();
				}
				
				Atom[] bondedAtoms = { atom1, atom2 };
				Double forceConst = Double.valueOf( bondParams[2] );
				Double equilLength = Double.valueOf( bondParams[3] );
				
				//3. create a new bond based on this bond record
				HarmonicBond newBond = new HarmonicBond( bondedAtoms, forceConst, equilLength );
				//4. add the new energy term to the Hamiltonian
				getHamiltonian().addEnergyTerm( newBond );
				//5. remove non-bonded interactions between the bonded terms
				removeNonBond( atom1, atom2 );
				
			}
			
			else if( label.equalsIgnoreCase( "ANGLE" ) )
			{
				int i = 0;
				String[] angleParams = new String[5];
				while( lineReader.hasNext() )
				{
					angleParams[i] = lineReader.next().trim();
					i++;
				}
				
				Atom atom1 = getMolecularSystem().getAtom( Integer.valueOf(angleParams[0]) );
				if( atom1 == null )
				{
					System.out.println( "Can't make angle: No atom with serial number = " + Integer.valueOf(angleParams[0]) + " in the system."	);
					throw new NullPointerException();
				}
				Atom atom2 = getMolecularSystem().getAtom( Integer.valueOf(angleParams[1]) );
				if( atom2 == null )
				{
					System.out.println( "Can't make angle: No atom with serial number = " + Integer.valueOf(angleParams[1]) + " in the system."	);
					throw new NullPointerException();
				}
				Atom atom3 = getMolecularSystem().getAtom( Integer.valueOf(angleParams[2]) );
				if( atom3 == null )
				{
					System.out.println( "Can't make angle: No atom with serial number = " + Integer.valueOf(angleParams[2]) + " in the system."	);
					throw new NullPointerException();
				}
				
				Atom[] angleAtoms = { atom1, atom2, atom3 };
				Double forceConst = Double.valueOf( angleParams[3] );
				Double equilAngle = Double.valueOf( angleParams[4] );
				
				
				//3. create a new angle based on this record
				HarmonicAngle newAngle = new HarmonicAngle( angleAtoms, forceConst, equilAngle );
				//4. add the new energy term to the Hamiltonian
				getHamiltonian().addEnergyTerm( newAngle );	
				//5. remove non-bonded interactions between the bonded terms
				removeNonBond( atom1, atom3 );
				
				removeNonBond( atom1, atom2 );
				removeNonBond( atom2, atom3 );
				
			}
			
			else if( label.equalsIgnoreCase( "TORSION" ) )
			{
				int i = 0;
				String[] torsionParams = new String[7];
				while( lineReader.hasNext() )
				{
					torsionParams[i] = lineReader.next().trim();
					i++;
				}
				
				Atom atom1 = getMolecularSystem().getAtom( Integer.valueOf(torsionParams[0]) );
				if( atom1 == null )
				{
					System.out.println( "Can't make torsion: No atom with serial number = " + Integer.valueOf(torsionParams[0]) + " in the system."	);
					throw new NullPointerException();
				}
				Atom atom2 = getMolecularSystem().getAtom( Integer.valueOf(torsionParams[1]) );
				if( atom2 == null )
				{
					System.out.println( "Can't make torsion: No atom with serial number = " + Integer.valueOf(torsionParams[1]) + " in the system."	);
					throw new NullPointerException();
				}
				Atom atom3 = getMolecularSystem().getAtom( Integer.valueOf(torsionParams[2]) );
				if( atom3 == null )
				{
					System.out.println( "Can't make torsion: No atom with serial number = " + Integer.valueOf(torsionParams[2]) + " in the system."	);
					throw new NullPointerException();
				}
				Atom atom4 = getMolecularSystem().getAtom( Integer.valueOf(torsionParams[3]) );
				if( atom4 == null )
				{
					System.out.println( "Can't make torsion: No atom with serial number = " + Integer.valueOf(torsionParams[3]) + " in the system."	);
					throw new NullPointerException();
				}
				
				Atom[] torsionAtoms = { atom1, atom2, atom3, atom4 };
				
				double force_const = Double.valueOf( torsionParams[4] );
				int multiplicity = Integer.valueOf( torsionParams[5] );
				double delta = Double.valueOf( torsionParams[6] );
				
				//3. create a new torsion based on this record
				Torsion newTorsion = new Torsion( torsionAtoms, force_const, multiplicity, delta );
				//4. add the new energy term to the Hamiltonian
				getHamiltonian().addEnergyTerm( newTorsion );
				//5. remove non-bonded interactions between the bonded terms
				removeNonBond( atom1, atom4 );
				
				removeNonBond( atom1, atom2 );
				removeNonBond( atom2, atom3 );
				removeNonBond( atom1, atom3 );
				removeNonBond( atom2, atom4 );
				removeNonBond( atom3, atom4 );
			} 
			else
			{
				// Skip this line if it's not a recognized energy term
				continue;
			}
		
		}
		
		//5. close the input file
		structureInput.close();
		
	}
	
	/** 
    *	Add a bounding box; a cube with a side length of <boxSideLength> 
    *	
    *
    *	@param	boxSideLength
    */
	public void
	addBox( double boxSideLength )
	{
		if( boxSideLength <= 0.0 )
		{
			return;
		}
		
		else
		{		
			LinkedList atomList = getMolecularSystem().getAtoms();
			// add box interaction for every atom
			for( int i = 0; i < atomList.size(); i++ )
			{
				Atom[] atom = { (Atom)atomList.get(i) };
				BoundingBox newBox = new BoundingBox( atom, boxSideLength );
				getHamiltonian().addEnergyTerm( newBox );	
			}
		}
	}
	
	/** 
    *	Run 
    *	
    *
    *	@param	structureFileName
    */
	public void
	runSimulation( int numSteps, int numDimensions, int printInterval, String outputFolder, String outputName, JProgressBar progressBar, TemperatureProtocol temperatureProtocol, boolean printRawCoords ) throws Exception
	{
		Monitor simStepMon = null;
		Monitor outputMon = null;
	
		/* DEBUG
		Monitor mon = MonitorFactory.start("oscarMonitor");
		File monitorFile = new File( outputFolder + outputName + "_monitor.txt" );
		if( monitorFile.exists() )
		{
			// overwrite file
			;
		}
		PrintWriter monitorOutput = new PrintWriter( monitorFile );
		// END DEBUG
		*/
	
		// We can't run a simulation if we don't have an algorithm
		if( getSimAlgorithm() == null )
		{
			System.err.println( "Algorithm undefined" );
			throw new NullPointerException();
		}
		
		//1. open output files
		File trajectoryFile = new File( outputFolder + outputName + "_trajectory.pdb" );
		if( trajectoryFile.exists() )
		{
			// overwrite file
			;
		}
		
		File energyFile = new File( outputFolder + outputName + "_energy.txt" );
		if( energyFile.exists() )
		{
			// overwrite file
			;
		}
		
		//File logFile = new File( outputFolder + outputName + "_log.txt" );
		//if( logFile.exists() )
		//{
			// overwrite file
		//	;
		//}
		
		File coordStreamFile = new File( outputFolder + outputName + "_raw_coords.txt" );
		if( coordStreamFile.exists() )
		{
			//overwrite file
			;
		}
		
		PrintWriter trajectoryOutput = new PrintWriter( trajectoryFile );
		PrintWriter energyOutput = new PrintWriter( energyFile );
		PrintWriter coordStream = new PrintWriter( coordStreamFile );
		
		//PrintWriter logOutput = new PrintWriter( logFile );
		
		//writeLog( logOutput );
		//logOutput.close();
		
		//Write banner (column headings) for the energy file and coordinate file
		energyOutput.print( getSimAlgorithm().getEnergyBanner( getHamiltonian() ) );
		
		if( printRawCoords == true )
		{
			coordStream.print( getMolecularSystem().getCoordBanner( ) );
		}
		
		// Write intial coordinates and energies
		trajectoryOutput.println( "HEADER\tCoordinates at Step " + 0 );
		trajectoryOutput.print( getMolecularSystem() );
		trajectoryOutput.print( getConnectRecords() );
		trajectoryOutput.println( "END" );
		energyOutput.print( getSimAlgorithm().formatEnergy( getMolecularSystem(), getHamiltonian(), 0 ) );
		if( printRawCoords == true )
		{
			coordStream.print( getMolecularSystem().getCoordString( 0 ) );
		}
		
		// Initial random velocities
		adjustTemperature( "Reassignment", getSimAlgorithm().getTemperature(), getSimAlgorithm().getTemperature(), 1.0, numDimensions );
		
		/* Store output until the end of the simulation
		LinkedList trajectoryOutputList = new LinkedList();
		LinkedList energyOutputList = new LinkedList();
		LinkedList coordStreamList = new LinkedList();
		*/
		
		//2. loop over number of simulation steps
		for( int stepIndex = 1; stepIndex <= numSteps; stepIndex++ )
		{
			// DEBUG
			//simStepMon = MonitorFactory.start("simStepMonitor");
			// END DEBUG
			
			//3. run a step
			String energyString = getSimAlgorithm().simStep(  getMolecularSystem(), getHamiltonian(), stepIndex, numDimensions );
			
			// DEBUG
			//simStepMon.stop();
			// END DEBUG
			
			// DEBUG
			//outputMon = MonitorFactory.start( "outputMonitor" );
			// END DEBUG
			
			//4. update output files if we've reached a printInterval step
			if( printInterval > 0 && stepIndex % printInterval == 0 )
			{
				/* Save output until the end
				trajectoryOutputList.addLast( "HEADER\tCoordinates at Step " + String.valueOf(stepIndex) + "\n" );
				trajectoryOutputList.addLast( getMolecularSystem().toString() );
				trajectoryOutputList.addLast( "END" + "\n" );
				energyOutputList.addLast( energyString );
				coordStreamList.addLast( getMolecularSystem().getCoordString( stepIndex ) );
				*/
				
				trajectoryOutput.println( "HEADER\tCoordinates at Step " + String.valueOf(stepIndex) );
				trajectoryOutput.print( getMolecularSystem() );
				trajectoryOutput.println( "END" );
				energyOutput.print( energyString );
				if( printRawCoords == true )
				{
					coordStream.print(getMolecularSystem().getCoordString( stepIndex ));
				}
			}
			
			// DEBUG
			//outputMon.stop();
			// END DEBUG
			
			//5. update the progress bar
			progressBar.setValue( (int)(stepIndex * 100.0 / numSteps) );
			//6. Adjust temperature if we've reached a thermInterval step
			if( temperatureProtocol.isThermStep( stepIndex ) == true )
			{
				double timeStep = 0.0;
				if( getSimAlgorithm().getID().equalsIgnoreCase( DefaultMD.MD_IDENTIFIER ) )
				{
					DefaultMD md = (DefaultMD)getSimAlgorithm();
					timeStep = md.getTimeStep();
				}
				
				try
				{
					String thermMethod = temperatureProtocol.getCurrentThermMethod( stepIndex );
					double targetTemp = temperatureProtocol.getCurrentTargetTemp( stepIndex );
					
					adjustTemperature( thermMethod, getSimAlgorithm().getTemperature(), targetTemp, 
										timeStep, numDimensions );
				}
				catch( Exception e )
				{
					// Either an error has occurred or there's no temperature protocol defined for the current simulation step. We'll forget about adjusting the temperature and continue with the simulation.
					System.out.print( "Caught therm. error at step " );
					System.out.println( stepIndex );
					;
				}//end catch 
			}//end if
			
			// 6. Check for stop condition (e.g. minimization convergence)
			if( getSimAlgorithm().isTimeToQuit() == true )
			{
				progressBar.setValue( 100 );
				
				// print energy at convergence
				//trajectoryOutputList.addLast( getSimAlgorithm().formatEnergy( getMolecularSystem(), getHamiltonian(), stepIndex ) );
				
				//coordStreamList.addLast( getMolecularSystem().getCoordString( stepIndex) );
				
				trajectoryOutput.print( getSimAlgorithm().formatEnergy( getMolecularSystem(), getHamiltonian(), stepIndex ) );
				
				if( printRawCoords == true )
				{
					coordStream.print( getMolecularSystem().getCoordString( stepIndex) );
				}
				
				break;
			}
			
			
		}//end for loop
			
		/* Output
		ListIterator listIter = trajectoryOutputList.listIterator( 0 );
		while( listIter.hasNext() )
		{
			trajectoryOutput.print( listIter.next() );
		}
		
		progressBar.setValue( 97 );
		
		listIter = energyOutputList.listIterator( 0 );
		while( listIter.hasNext() )
		{
			energyOutput.print( listIter.next() );
		}
		
		progressBar.setValue( 99 );
		
		listIter = coordStreamList.listIterator( 0 );
		while( listIter.hasNext() )
		{
			coordStream.print( listIter.next() );
		}
		*/
		
		//7. close output files
		trajectoryOutput.close();
		energyOutput.close();
		coordStream.close();
		
		progressBar.setValue( 100 );
		
		if( printRawCoords != true )
		{
			// the raw coord file is empty, so let's remove it
			coordStreamFile.delete();
		}
		
		/* DEBUG
		mon.stop();
		monitorOutput.println( simStepMon );
		monitorOutput.println( outputMon );
		monitorOutput.println( mon );
		monitorOutput.close();
		// END DEBUG
		*/
	}//end runSimulation
	
    public MolecularSystem
    getMolecularSystem( )
        {
			return myMolecularSystem;
        }

	public void
    setMolecularSystem( MolecularSystem newMolecularSystem )
        {
			myMolecularSystem = newMolecularSystem;
        }

    public Hamiltonian
    getHamiltonian( )
        {
			return myHamiltonian;
        }

	public void
    setHamiltonian( Hamiltonian newHamiltonian )
        {
			myHamiltonian = newHamiltonian;
        }
        
	public AbstractSimAlgorithm
    getSimAlgorithm( )
        {
			return mySimAlgorithm;
        }

	public void
    setSimAlgorithm( AbstractSimAlgorithm newSimAlgorithm )
        {
			mySimAlgorithm = newSimAlgorithm;
        }        

    /*====================================================================
        Private Member Functions
    */
    
    /** 
    *	Generates non-bonded interactions between each pair of atoms in the
    *	system.
    *	
    */
	private void
	addNonBondedTerms(double VDW_Radius, double wellDepth)
	{
		LinkedList atomList = getMolecularSystem().getAtoms();
		for( int i = 0; i < atomList.size() - 1; i++ )
		{
			Atom atom1 = (Atom)atomList.get( i );
			
			for( int j = i + 1; j < atomList.size(); j++ )
			{
				Atom atom2 = (Atom)atomList.get( j );
				
				Atom[] nonbondAtoms = { atom1, atom2 };
				
				// create vdw interaction
				VanDerWaals newVDW = new VanDerWaals( nonbondAtoms, VDW_Radius, wellDepth );
				// add the new vdw term to the Hamiltonian
				getHamiltonian().addEnergyTerm( newVDW );	
				
				double dielectricConstant = 1.0;
				// create electrostatic interaction
				CoulombElectro newElec = new CoulombElectro( nonbondAtoms, dielectricConstant );
				// add the new electrostatic term to the Hamiltonian
				getHamiltonian().addEnergyTerm( newElec );	
			}
		}
		
		
	}
	
	private void	
	removeNonBond( Atom atom1, Atom atom2 )
	{
	
		getHamiltonian().removeTerm( VanDerWaals.IDENTIFIER, atom1, atom2 );
		getHamiltonian().removeTerm( CoulombElectro.IDENTIFIER, atom1, atom2 );
	
	}
	
	/** 
    *	Reassign or rescale atomic velocities to match the target temperature
   	*	of the system.
    *	
    *	@param	thermMethod		Rescale, Reassign, or Temperature Bath
    *	@param	currentTemperature
    *	@param	targetTemperature
    *	@param	timeStep		Needed for Berendsen bath
    *
    *	"Reassign" - chooses random velocities from the Maxwell-Boltzmann
    *	distribution. Velocities are adjusted so that the total momentum of the *	system is zero.
    */
	private void
	adjustTemperature( String thermMethod, double currentTemperature, double targetTemperature, double timeStep, int numDimensions )
	{
	
		/* DEBUG
		System.out.print( "Adjust temperature from " );
		System.out.print( currentTemperature );
		System.out.print( " to " );
		System.out.print( targetTemperature );
		System.out.print( " using " );
		System.out.println( thermMethod );
		*/
	
		ListIterator atomList = getMolecularSystem().iterateAtoms();
    	Atom thisAtom;
    	double[] thisVelocity;
    	double newX, newY, newZ;
    	double[] totalMomentum = {0.0, 0.0, 0.0};
		double totalMass = 0.0;
    	while( atomList.hasNext() )
    	{
    		thisAtom = (Atom)atomList.next();
    		thisVelocity = thisAtom.getVelocity();	
    		
    		if( thermMethod.equalsIgnoreCase( "Rescaling" ) )
    		{
    			double scaleFactor = 0.0;
    			if( currentTemperature != 0.0 )
    			{
    				scaleFactor = Math.sqrt( targetTemperature/currentTemperature );
    			}
    		
    			newX = thisVelocity[0] * scaleFactor;
    			newY = thisVelocity[1] * scaleFactor;
    			newZ = thisVelocity[2] * scaleFactor;
    			/*
    			System.out.print( "Atom " );
				System.out.print( thisAtom.getSerialNumber() );
				System.out.print( "  {" );
				System.out.print( newX );
				System.out.print( ", " );
				System.out.print( newY );
				System.out.print( ", " );
				System.out.print( newZ );
				System.out.println( "}" );
				System.out.println( "" );
    			*/
    			double[] rescaledVelo = { newX, newY, newZ };
    			thisAtom.setVelocity( rescaledVelo );

    		}
			else if( thermMethod.equalsIgnoreCase( "Reassignment" ) )
			{
				Random randomGenerator = new Random();
				
				newX = sampleMaxwellBoltzmann( randomGenerator.nextGaussian(), thisAtom.getMass(), targetTemperature );
				newY = 0.0;
				newZ = 0.0;
				
				if( numDimensions > 1 )
				{	
					newY = sampleMaxwellBoltzmann( randomGenerator.nextGaussian(), thisAtom.getMass(), targetTemperature );
				}
				if( numDimensions > 2 )
				{
					newZ = sampleMaxwellBoltzmann( randomGenerator.nextGaussian(), thisAtom.getMass(), targetTemperature );
				}
				
				totalMomentum[0] += newX * thisAtom.getMass();
				totalMomentum[1] += newY * thisAtom.getMass();
				totalMomentum[2] += newZ * thisAtom.getMass();
				totalMass += thisAtom.getMass();
				/*
				System.out.print( "Atom " );
				System.out.print( thisAtom.getSerialNumber() );
				System.out.print( "  {" );
				System.out.print( newX );
				System.out.print( ", " );
				System.out.print( newY );
				System.out.print( ", " );
				System.out.print( newZ );
				System.out.println( "}" );
				System.out.println( "" );
				*/
				double[] reassignedVelo = { newX, newY, newZ };
    			thisAtom.setVelocity( reassignedVelo );
			}
			else if( thermMethod.equalsIgnoreCase( "Berendsen Bath" ) )
			{
				double scaleFactor = 0.0;
    			if( currentTemperature != 0.0 )
    			{
    				scaleFactor = Math.sqrt( 1 + (timeStep/COUPLING_CONST) * ((targetTemperature/currentTemperature) - 1) );
    			}
    		
    			newX = thisVelocity[0] * scaleFactor;
    			newY = thisVelocity[1] * scaleFactor;
    			newZ = thisVelocity[2] * scaleFactor;
    			
    			double[] rescaledVelo = { newX, newY, newZ };
    			thisAtom.setVelocity( rescaledVelo );
			}
			else if( thermMethod.equalsIgnoreCase( "Monte Carlo" ) )
			{
				// In Monte Carlo, we directly set the temperature which will alter the probability of accepting a move
				getSimAlgorithm().setTemperature( targetTemperature );
			}
			
			else
			{
				;
			}
		} //end while loop
		
		// Adjust velocities so total system momentum is zero
		if( thermMethod.equalsIgnoreCase( "Reassignment" ) )
		{
			atomList = getMolecularSystem().iterateAtoms();
			while( atomList.hasNext() )
			{
				thisAtom = (Atom)atomList.next();
    			thisVelocity = thisAtom.getVelocity();
    			
    			newX = thisVelocity[0] - (totalMomentum[0]/totalMass);
    			newY = thisVelocity[1] - (totalMomentum[1]/totalMass);
    			newZ = thisVelocity[2] - (totalMomentum[2]/totalMass);
    			
    			double[] correctedVelo = { newX, newY, newZ };
    			thisAtom.setVelocity( correctedVelo );
			}
		}
		
	}
	
	/**
	*	Make a connect record string of the following format:
	*	CONECT     serial1     serial2     serial3     ...     serialM
	*	.
	*	.
	*	.
	*	CONECT     serialN     serial1     serial3     ...     serialM
	*
	*	In each line, the first atom is bonded to each of the following atoms.
	*/
	private String
	getConnectRecords()
	{
		int numAtoms = getMolecularSystem().numAtoms();
		boolean[][] connectionTable = new boolean[numAtoms+1][numAtoms+1];
		LinkedList bondList = getHamiltonian().getEnergyTerms().getInteractionList( HarmonicBond.IDENTIFIER );
    	
    	if( bondList == null )
    	{
    		return "";
    	}
    	
    	ListIterator bondIterator = bondList.listIterator(0);
    	
    	// initialize table
    	for( int i = 0; i <= numAtoms; i++ )
    	{
    		for( int j = 0; j <= numAtoms; j++ )
    		{
    			connectionTable[i][j] = false;
    		}
    	}
    	
    	// Each pair of bonded atoms receives a value of "true" in the connection table. For example, if atoms 1 & 2 are bonded, then the values of table[1][2] and table[2][1] are true. 
    	while( bondIterator.hasNext() )
    	{
    		HarmonicBond thisBond = (HarmonicBond)bondIterator.next();
    		Atom[] atoms = thisBond.getAtoms();
    		
    		int serial1 = atoms[0].getSerialNumber();
    		int serial2 = atoms[1].getSerialNumber();
    		
    		connectionTable[serial1][serial2] = true;
    		connectionTable[serial2][serial1] = true;
    		
    	}
    	
    	String connectRecordString = "";
    	Parameters connectRecordParameters = new Parameters();
    	
    	for( int i = 0; i <= numAtoms; i++ )
    	{
    		boolean isConnected = false;
    		
    		for( int j = 0; j <= numAtoms; j++ )
    		{
    			if( connectionTable[i][j] == true )
    			{
    				isConnected = true;
    			}
    		}
    		
    		// if this atom is connected to any other atoms, then create a connect record for it
    		if( isConnected )
    		{
    			connectRecordString += "%6s%5d";
    			connectRecordParameters.add( "CONECT" ).add( i );
    				
    			for( int j = 0; j <= numAtoms; j++ )
    			{
    				if( connectionTable[i][j] == true )
    				{
    					connectRecordString += "%5d";
    					connectRecordParameters.add( j );
    				}
    			}
    			
    			connectRecordString += "\n";
    		}
    	}
	
		return Format.sprintf( connectRecordString, connectRecordParameters );
	
	}

	
    /*====================================================================
        Private Data Members
    */
	private MolecularSystem myMolecularSystem;
    private Hamiltonian myHamiltonian;
    private AbstractSimAlgorithm mySimAlgorithm;
}

