package net.geoffrollins.oscar;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;


public class AlgorithmPanel extends JPanel {
 
    private javax.swing.JLabel anotherBlankSpace;
    private javax.swing.JLabel blankSpace;
    private javax.swing.ButtonGroup algorithmGroup;
    private javax.swing.JRadioButton minimizationRadioButton;
    private javax.swing.JRadioButton molecularDynamicsRadioButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JRadioButton monteCarloRadioButton;
    private javax.swing.JLabel welcomeTitle;
    private javax.swing.JLabel yetAnotherBlankSpace1;
    
    private JPanel contentPanel;
    private JLabel iconLabel;
    private JSeparator separator;
    private JLabel textLabel;
    private JPanel titlePanel;
        
    public AlgorithmPanel() {
     
        super();
                
        contentPanel = getContentPanel();
        contentPanel.setBorder(new EmptyBorder(new Insets(10, 10, 10, 10)));
		contentPanel.setBackground( Color.decode("0xD0E6FF") );
		Font headerFont = new Font("Arial", Font.BOLD, 24);

        ImageIcon icon = getImageIcon();
        
        titlePanel = new javax.swing.JPanel();
        textLabel = new javax.swing.JLabel();
        iconLabel = new javax.swing.JLabel();
        separator = new javax.swing.JSeparator();

        setLayout(new java.awt.BorderLayout());


        titlePanel.setLayout(new java.awt.BorderLayout());
        titlePanel.setBackground(Color.decode("0x3C8CE4"));
        
        textLabel.setBackground(Color.decode("0x3C8CE4"));
        textLabel.setForeground(Color.white);
        textLabel.setFont( headerFont );
        textLabel.setText("Simulation Type");
        textLabel.setBorder(new EmptyBorder(new Insets(10, 10, 10, 10)));
        textLabel.setOpaque(true);

        iconLabel.setBackground(Color.white);
        if (icon != null)
            iconLabel.setIcon(icon);   
        
        titlePanel.add(textLabel, BorderLayout.CENTER);
        titlePanel.add(iconLabel, BorderLayout.EAST);
        titlePanel.add(separator, BorderLayout.SOUTH);
        
        add(titlePanel, BorderLayout.NORTH);
        JPanel secondaryPanel = new JPanel();
        secondaryPanel.add(contentPanel, BorderLayout.NORTH);
		secondaryPanel.setBackground(Color.decode("0xD0E6FF"));        add(secondaryPanel, BorderLayout.CENTER);

		 

    }  
    
    public String getAlgorithmSelected() {
        return algorithmGroup.getSelection().getActionCommand();
    }
    
    private JPanel getContentPanel() {     
        
        JPanel contentPanel1 = new JPanel();
        
        Font titleFont = new Font("Arial", Font.PLAIN, 18);
        Font radioButtonFont = new Font("Arial", Font.PLAIN, 18);
        
        
        algorithmGroup = new javax.swing.ButtonGroup();
        welcomeTitle = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        blankSpace = new javax.swing.JLabel();
        minimizationRadioButton = new javax.swing.JRadioButton();
        molecularDynamicsRadioButton = new javax.swing.JRadioButton();
        monteCarloRadioButton = new javax.swing.JRadioButton();
        
        minimizationRadioButton.setActionCommand("Energy Minimization");
        molecularDynamicsRadioButton.setActionCommand("Molecular Dynamics");
        monteCarloRadioButton.setActionCommand("Monte Carlo");
        
        anotherBlankSpace = new javax.swing.JLabel();
        yetAnotherBlankSpace1 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        minimizationRadioButton.setSelected(true);
        
        contentPanel1.setLayout(new java.awt.BorderLayout());

        welcomeTitle.setText("Please select an algorithm:");
        welcomeTitle.setFont( titleFont );
        contentPanel1.add(welcomeTitle, java.awt.BorderLayout.NORTH);

        jPanel1.setLayout(new java.awt.GridLayout(0, 1));

        jPanel1.add(blankSpace);
        
        minimizationRadioButton.setText("Energy Minimization");
        minimizationRadioButton.setFont( radioButtonFont );
        algorithmGroup.add(minimizationRadioButton);
        jPanel1.add(minimizationRadioButton);

        molecularDynamicsRadioButton.setText("Molecular Dynamics");
        molecularDynamicsRadioButton.setFont( radioButtonFont );
        algorithmGroup.add(molecularDynamicsRadioButton);
        jPanel1.add(molecularDynamicsRadioButton);

        monteCarloRadioButton.setText("Monte Carlo");
        monteCarloRadioButton.setFont( radioButtonFont );
        algorithmGroup.add(monteCarloRadioButton);
        jPanel1.add(monteCarloRadioButton);

        jPanel1.add(anotherBlankSpace);

  

        jPanel1.add(yetAnotherBlankSpace1);
		jPanel1.setBackground( Color.decode("0xD0E6FF") ); 

        contentPanel1.add(jPanel1, java.awt.BorderLayout.CENTER);

        contentPanel1.add(jLabel1, java.awt.BorderLayout.SOUTH);
        contentPanel1.setBackground( Color.decode("0xD0E6FF") ); 
        
        return contentPanel1;
    }
    
    private ImageIcon getImageIcon() {
        
        //  Icon to be placed in the upper right corner.
        return null;
    }
    
    

}
