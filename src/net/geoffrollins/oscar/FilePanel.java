package net.geoffrollins.oscar;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.net.*;


public class FilePanel extends JPanel 
{
	JFileChooser fc;
	
    public FilePanel() 
    {
    	super();
    	
    	Font headerFont = new Font("Arial", Font.BOLD, 24);
    
    	contentPanel = getContentPanel();
        contentPanel.setBorder(new EmptyBorder(new Insets(10, 10, 10, 10)));
        contentPanel.setBackground( Color.decode("0xD0E6FF") );
        fc = new JFileChooser();
        
        titlePanel = new javax.swing.JPanel();
        textLabel = new javax.swing.JLabel();
        separator = new javax.swing.JSeparator();
        
        setLayout(new java.awt.BorderLayout());

		titlePanel.setLayout(new java.awt.BorderLayout());
        titlePanel.setBackground(Color.decode("0x3C8CE4"));
        
        textLabel.setBackground(Color.decode("0x3C8CE4"));
        textLabel.setForeground(Color.white);
        textLabel.setFont( headerFont );
        textLabel.setText("Welcome to Oscar");
        textLabel.setBorder(new EmptyBorder(new Insets(10, 10, 10, 10)));
        textLabel.setOpaque(true);

		ImageIcon icon = getImageIcon();
		iconLabel = new javax.swing.JLabel();

		if (icon != null)
            iconLabel.setIcon(icon);

		titlePanel.add(textLabel, BorderLayout.CENTER);
        titlePanel.add(iconLabel, BorderLayout.EAST);
        titlePanel.add(separator, BorderLayout.SOUTH);
        
        add(titlePanel, BorderLayout.NORTH);
        JPanel secondaryPanel = new JPanel();
        secondaryPanel.add(contentPanel, BorderLayout.NORTH);
        secondaryPanel.setBackground(Color.decode("0xD0E6FF"));
        add(secondaryPanel, BorderLayout.CENTER);
        
    }
    
    public String getCoordText()
    {
    	return coordTextField.getText();
    }
    
    public String getStructureText()
    {
    	return structureTextField.getText();
    }
    
    public String getOutputFolderText()
    {
    	return outputFolderTextField.getText();
    }
    
    public String getJobNameText()
    {
    	return jobNameTextField.getText();
    }
    
    public String getOutputIntervalText()
    {
    	return outputIntervalTextField.getText();
    }
    
    public boolean getRawCoordValue()
    {
    	String rawCoordBoxString = String.valueOf(rawCoordBox.getSelectedItem());
    	
    	if( rawCoordBoxString.equalsIgnoreCase( "On" ) )
    	{
    		return true;
    	}
    	else
    	{
    		return false;
    	}
    }
    
    private JPanel getContentPanel()
    {
        Font titleFont = new Font("Arial", Font.BOLD, 18);
        Font labelFont = new Font("Arial", Font.BOLD, 16);
        Font labelFont2 = new Font("Arial", Font.PLAIN, 16);
        
        JPanel contentPanel1 = new JPanel();
        welcomeTitle = new JLabel();
        blankSpace = new JLabel();
        
        outputPanel = new javax.swing.JPanel();
        outputFolderLabel = new javax.swing.JLabel();
        outputFolderTextField = new javax.swing.JTextField();
        outputFolderButton = new javax.swing.JButton();
        jobNameLabel = new javax.swing.JLabel();
        jobNameTextField = new javax.swing.JTextField();
        inputPanel = new javax.swing.JPanel();
        coordLabel = new javax.swing.JLabel();
        coordTextField = new javax.swing.JTextField();
        coordButton = new javax.swing.JButton();
        structureLabel = new javax.swing.JLabel();
        structureTextField = new javax.swing.JTextField();
        structureButton = new javax.swing.JButton();
      	outputIntervalLabel = new javax.swing.JLabel();
        outputIntervalTextField = new javax.swing.JTextField();
        outputIntervalLabel2 = new javax.swing.JLabel();
        rawCoordLabel = new javax.swing.JLabel();
    	rawCoordBox = new javax.swing.JComboBox();
      
        contentPanel1.setLayout(new java.awt.BorderLayout());

		outputPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEmptyBorder(), "Output", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, titleFont ));
		
        outputFolderLabel.setText("Output Folder");
        outputFolderTextField.setText("**Required Field**");
        outputFolderButton.setText("Browse...");
        outputFolderButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                outputFolderButtonActionPerformed(evt);
            }
		});

        jobNameLabel.setText("Job Name");
        jobNameTextField.setText("test_simulation");

        outputIntervalLabel.setText("Output Interval");
        outputIntervalTextField.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        outputIntervalTextField.setText("100");
        outputIntervalLabel2.setText("steps");

		outputFolderLabel.setFont( labelFont );
		outputFolderTextField.setFont( labelFont2 );
		outputFolderButton.setFont( labelFont );
		jobNameLabel.setFont( labelFont );
		jobNameTextField.setFont( labelFont2 );
		outputIntervalLabel.setFont( labelFont );
		outputIntervalTextField.setFont( labelFont2 );
        outputIntervalLabel2.setFont( labelFont2 );
        
        rawCoordLabel.setText( "Raw Coordinates" );
        rawCoordLabel.setFont( labelFont );
        
        rawCoordBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "On", "Off" }));
       	rawCoordBox.setFont( labelFont2 );
       
        org.jdesktop.layout.GroupLayout outputPanelLayout = new org.jdesktop.layout.GroupLayout(outputPanel);
        outputPanel.setLayout(outputPanelLayout);
        outputPanelLayout.setHorizontalGroup(
            outputPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(outputPanelLayout.createSequentialGroup()
                .add(50, 50, 50)
                .add(outputPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(outputIntervalLabel)
                    .add(jobNameLabel)
                    .add(outputFolderLabel)
                    .add(rawCoordLabel))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(outputPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jobNameTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 195, Short.MAX_VALUE)
                    .add(outputFolderTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 195, Short.MAX_VALUE)
                    .add(rawCoordBox, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 64, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(outputPanelLayout.createSequentialGroup()
                        .add(outputIntervalTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 64, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(outputIntervalLabel2)))
                .add(9, 9, 9)
                .add(outputFolderButton)
                .addContainerGap())
        );
        outputPanelLayout.setVerticalGroup(
            outputPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(outputPanelLayout.createSequentialGroup()
                .add(outputPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(outputFolderButton)
                    .add(outputFolderTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(outputFolderLabel))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(outputPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jobNameLabel)
                    .add(jobNameTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(outputPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(outputIntervalLabel)
                    .add(outputIntervalTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(outputIntervalLabel2))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(outputPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                	.add(rawCoordLabel)
                	.add(rawCoordBox))
                .addContainerGap())
        );

        inputPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEmptyBorder(), "Input", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, titleFont));
        
        coordLabel.setText("Coordinate File (.pdb)");
        coordTextField.setText("**Required Field**");
        coordButton.setText("Browse...");
        coordButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                coordButtonActionPerformed(evt);
            }
        });

        structureLabel.setText("Structure File (.sf)");
        structureTextField.setText("<optional>");
        structureButton.setText("Browse...");
        structureButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                structureButtonActionPerformed(evt);
            }
		});
		
		coordLabel.setFont( labelFont );
		coordTextField.setFont( labelFont2 );
		coordButton.setFont( labelFont );
		structureLabel.setFont( labelFont );
		structureTextField.setFont( labelFont2 );
		structureButton.setFont( labelFont );
		
        org.jdesktop.layout.GroupLayout inputPanelLayout = new org.jdesktop.layout.GroupLayout(inputPanel);
        inputPanel.setLayout(inputPanelLayout);
        inputPanelLayout.setHorizontalGroup(
            inputPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(inputPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(inputPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, coordLabel)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, structureLabel))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(inputPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(structureTextField)
                    .add(coordTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 197, Short.MAX_VALUE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(inputPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(coordButton)
                    .add(structureButton))
                .addContainerGap(25, Short.MAX_VALUE))
        );
        inputPanelLayout.setVerticalGroup(
            inputPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(inputPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(inputPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(coordLabel)
                    .add(coordTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(coordButton))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(inputPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(structureLabel)
                    .add(structureTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(structureButton))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

		inputPanel.setBackground( Color.decode("0xD0E6FF") );
		outputPanel.setBackground( Color.decode("0xD0E6FF") );
		
		contentPanel1.add(inputPanel, java.awt.BorderLayout.NORTH);
		contentPanel1.add(outputPanel, java.awt.BorderLayout.SOUTH);
		contentPanel1.setBackground( Color.decode("0xD0E6FF") );

        return contentPanel1;
    }
    
    private void coordButtonActionPerformed(java.awt.event.ActionEvent evt)
    {
    	try
    	{
    		File f = new File(new File(".").getCanonicalPath());
    		fc.setCurrentDirectory( f );
    	}
    	catch( Exception e )
    	{
    		System.err.println( "Failed to open current directory" );
    	}
    	
		fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        int returnVal = fc.showOpenDialog(FilePanel.this);
        if (returnVal == JFileChooser.APPROVE_OPTION) 
        {
			File file = fc.getSelectedFile();
			coordTextField.setText(file.getAbsolutePath());
		}
    }
    
    private void structureButtonActionPerformed(java.awt.event.ActionEvent evt) 
    {
		fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        int returnVal = fc.showOpenDialog(FilePanel.this);
        if (returnVal == JFileChooser.APPROVE_OPTION) 
        {
			File file = fc.getSelectedFile();
			structureTextField.setText(file.getAbsolutePath());
		}
    }
    
    private void outputFolderButtonActionPerformed(java.awt.event.ActionEvent evt) 
    {
		fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int returnVal = fc.showOpenDialog(FilePanel.this);
        if (returnVal == JFileChooser.APPROVE_OPTION) 
        {
			File file = fc.getSelectedFile();
			outputFolderTextField.setText(file.getAbsolutePath());
		}
    }
    
    private ImageIcon getImageIcon() {        
        /*
        java.io.File imageFile = new java.io.File("/Users/geoff/Pictures/my_face.gif");
        try
        {
        	java.awt.image.BufferedImage newImage = javax.imageio.ImageIO.read( imageFile );
        	return new ImageIcon( newImage );
        }
        catch(Exception e )
        {
        	System.out.println( "Failed to read image" );
        	return null;
        }
        */
        
        return null;
    }
    
    private Object getResource(String key) {

        URL url = null;
        String name = key;

        if (name != null) {

            try {
                Class c = Class.forName("oscar.Main");
                url = c.getResource(name);
            } catch (ClassNotFoundException cnfe) {
                System.err.println("Unable to find Main class");
            }
            return url;
        } else
            return null;

    }
    
    private javax.swing.JButton coordButton;
    private javax.swing.JLabel coordLabel;
    private javax.swing.JTextField coordTextField;
    private javax.swing.JPanel inputPanel;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jobNameLabel;
    private javax.swing.JTextField jobNameTextField;
    private javax.swing.JButton outputFolderButton;
    private javax.swing.JLabel outputFolderLabel;
    private javax.swing.JTextField outputFolderTextField;
    private javax.swing.JPanel outputPanel;
    private javax.swing.JButton structureButton;
    private javax.swing.JLabel structureLabel;
    private javax.swing.JTextField structureTextField;
    private javax.swing.JLabel welcomeTitle;
    private javax.swing.JLabel blankSpace;
	private javax.swing.JLabel outputIntervalLabel;
    private javax.swing.JLabel outputIntervalLabel2;
    private javax.swing.JTextField outputIntervalTextField;
    private javax.swing.JLabel rawCoordLabel;
    private javax.swing.JComboBox rawCoordBox;
    
	private JPanel contentPanel;
    private JLabel iconLabel;
    private JSeparator separator;
    private JLabel textLabel;
    private JPanel titlePanel;

	
}
