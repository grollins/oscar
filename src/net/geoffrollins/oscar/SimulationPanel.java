package net.geoffrollins.oscar;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.event.*;

import com.nexes.wizard.*;

public class SimulationPanel extends JPanel {
 
    private JLabel anotherBlankSpace;
    private JLabel blankSpace;
    private ButtonGroup connectorGroup;
    private JLabel jLabel1;
    private JPanel jPanel1;
    private JLabel progressDescription;
    private JProgressBar progressSent;
    private JLabel welcomeTitle;
    private JLabel yetAnotherBlankSpace1;
    
    private JPanel contentPanel;
    private JLabel iconLabel;
    private JSeparator separator;
    private JLabel textLabel;
    private JPanel titlePanel;
        
    public SimulationPanel() {
        
        super();
        
        Font headerFont = new Font("Arial", Font.BOLD, 24);
                
        contentPanel = getContentPanel();
        ImageIcon icon = getImageIcon();
        contentPanel.setBackground( Color.decode("0xD0E6FF") );
        titlePanel = new javax.swing.JPanel();
        textLabel = new javax.swing.JLabel();
        iconLabel = new javax.swing.JLabel();
        separator = new javax.swing.JSeparator();

        setLayout(new java.awt.BorderLayout());

        titlePanel.setLayout(new java.awt.BorderLayout());
        titlePanel.setBackground(Color.decode("0x3C8CE4"));
        
        textLabel.setBackground(Color.decode("0x3C8CE4"));
        textLabel.setForeground(Color.white);
        textLabel.setFont(headerFont);
        textLabel.setText("Simulation in progress");
        textLabel.setBorder(new EmptyBorder(new Insets(10, 10, 10, 10)));
        textLabel.setOpaque(true);

        iconLabel.setBackground(Color.white);
        if (icon != null)
            iconLabel.setIcon(icon);
        
        titlePanel.add(textLabel, BorderLayout.CENTER);
        titlePanel.add(iconLabel, BorderLayout.EAST);
        titlePanel.add(separator, BorderLayout.SOUTH);

        add(titlePanel, BorderLayout.NORTH);
        JPanel secondaryPanel = new JPanel();
        secondaryPanel.add(contentPanel, BorderLayout.NORTH);
        secondaryPanel.setBackground(Color.decode("0xD0E6FF"));
        add(secondaryPanel, BorderLayout.CENTER);
        
    }  
    
    public void setProgressText(String s) {
        progressDescription.setText(s);
    }
    
    public void setProgressValue(int i) {
        progressSent.setValue(i);
    }
    
    public JProgressBar getProgressBar( )
    {
    	return progressSent;
    }
    
    public void setTextLabel( String text )
    {
    	textLabel.setText( text );
    }
    
    private JPanel getContentPanel() {            
        
        Font titleFont = new Font("Arial", Font.PLAIN, 18);
        Font labelFont = new Font("Arial", Font.BOLD, 16);
        Font labelFont2 = new Font("Arial", Font.PLAIN, 16);
        
        JPanel contentPanel1 = new JPanel();
        
        connectorGroup = new ButtonGroup();
        welcomeTitle = new JLabel();
        jPanel1 = new JPanel();
        blankSpace = new JLabel();
        progressSent = new JProgressBar();
        progressDescription = new JLabel();
        anotherBlankSpace = new JLabel();
        yetAnotherBlankSpace1 = new JLabel();
        jLabel1 = new JLabel();

        contentPanel1.setLayout(new java.awt.BorderLayout());

        jPanel1.setLayout(new java.awt.GridLayout(0, 1));

        jPanel1.add(blankSpace);

        progressSent.setStringPainted(true);
        jPanel1.add(progressSent);

        progressDescription.setFont(labelFont);
        progressDescription.setText("Simulation Progress");
        jPanel1.add(progressDescription);

        jPanel1.add(anotherBlankSpace);

        jPanel1.add(yetAnotherBlankSpace1);
        jPanel1.setBackground( Color.decode("0xD0E6FF") );

        contentPanel1.add(jPanel1, java.awt.BorderLayout.CENTER);

        jLabel1.setText("Please leave this window open until the simulation is finished.");
        jLabel1.setFont(labelFont2);
        contentPanel1.add(jLabel1, java.awt.BorderLayout.SOUTH);
        contentPanel1.setBackground( Color.decode("0xD0E6FF") );
        
        return contentPanel1;
    }
    
    private ImageIcon getImageIcon() {        
        return null;
    }
    
}
