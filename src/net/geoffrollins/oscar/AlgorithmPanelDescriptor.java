package net.geoffrollins.oscar;

import com.nexes.wizard.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;


public class AlgorithmPanelDescriptor extends WizardPanelDescriptor {
    
    public static final String IDENTIFIER = "ALGORITHM_CHOOSE_PANEL";
    
    AlgorithmPanel algorithmPanel;
    
    public AlgorithmPanelDescriptor() {
        
        algorithmPanel = new AlgorithmPanel();
        setPanelDescriptorIdentifier(IDENTIFIER);
        setPanelComponent(algorithmPanel);
        
    }
    
    public Object getNextPanelDescriptor() {
        
        String nextPanelID = MDPanelDescriptor.IDENTIFIER;
        String selectedPanel = algorithmPanel.getAlgorithmSelected();
        
        if( selectedPanel.equalsIgnoreCase( "Energy Minimization" ) )
        {
        	nextPanelID = MinimizationPanelDescriptor.IDENTIFIER;
        }
        else if( selectedPanel.equalsIgnoreCase( "Molecular Dynamics" ) )
        {
        	nextPanelID = MDPanelDescriptor.IDENTIFIER;
        }
        else if( selectedPanel.equalsIgnoreCase( "Monte Carlo" ) )
        {
        	nextPanelID = MCPanelDescriptor.IDENTIFIER;
        }
        else
        {
        	;
        }
        
        return nextPanelID;
    }
    
    public Object getBackPanelDescriptor() {
        return FilePanelDescriptor.IDENTIFIER;
    }
    
    public void aboutToHidePanel() 
    {
    	// save selected algorithm to parameter manager
    	ParameterManager parameterManager = getWizard().getParameterManager();
    	parameterManager.setAlgorithm( algorithmPanel.getAlgorithmSelected() );
    }

}
