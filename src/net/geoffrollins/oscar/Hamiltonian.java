/*****************************************************************************
    Hamiltonian.java

    Last Updated   - January 15, 2008
    Updated By     - Geoff Rollins

    Manages the energy terms of a molecular simulation

*****************************************************************************/

/*****************************************************************************
    Package and import statments
*/

package net.geoffrollins.oscar;

import java.util.LinkedList;
import java.util.ListIterator;


/*****************************************************************************
    Class Definitions
*/

/*============================================================================
    Organizes and maintains all of the potential energy terms in an Oscar simulation
 ............................................................................

    Calculates the potential energy of the molecular system and the force
    on each atom.

============================================================================*/
public class Hamiltonian    
{

    /*--------------------------------------------------------------------
        Constructors
     ...................................................................

       Description of constructors

    --------------------------------------------------------------------*/

    /** 
    *	Default class constructor.
    */
    public Hamiltonian( )
        {
			myEnergyTerms = new EnergyTermTable( );
			setCurrentVirial( 0.0 );
        }

    /** 
    *	Class constructor. Initializes the table that will hold all of the 
    *	interactions in this Hamiltonian.
    *	@param	interactionNames	The names of the interactions. 
    *								Example: { "HarmonicBond", "VDW" }
    */
    public Hamiltonian( String[] interactionNames)
        {
        	myEnergyTerms = new EnergyTermTable( interactionNames.length );
        	
        	// create an entry in the energy table for each type of interaction
			for( int interactionIndex = 0; interactionIndex < interactionNames.length; interactionIndex++ )
			{
				//DEBUG
				System.out.println( interactionNames[ interactionIndex ] );
				
				myEnergyTerms.addInteraction( interactionNames[ interactionIndex ] );
				
			}

			setCurrentVirial( 0.0 );
			
        }


    /*====================================================================
        Public Member Functions
    */
    
    /** 
    *	Adds an energy term to the Hamiltonian. This new energy term must be
    *	an interaction that the Hamiltonian already knows about. For example, 
    *	in order to add a new HarmonicBond to the Hamiltonian, there must already 
    *	be a list designated for HarmonicBonds in the Hamiltonian.
    *
    *	@param	newEnergyTerm
    */
    public void
    addEnergyTerm( AbstractEnergyTerm newEnergyTerm )
    {
    	// This new energy term is what kind of interaction? Example: HarmonicBond
    	String interactionType = newEnergyTerm.getName();
    	
    	// Find the appropriate list to add this new energy term to
    	LinkedList interactionList = myEnergyTerms.getInteractionList( interactionType );
    	// If this kind of interaction doesn't exist in the energy function...
    	if ( interactionList == null )
    	{
    		//...then make a list for it
    		myEnergyTerms.addInteraction( interactionType );
    		interactionList = myEnergyTerms.getInteractionList( interactionType );
    	}
  
    	// add this energy term to the appropriate list
    	interactionList.add( newEnergyTerm );
    }
    
    
    /** 
    *	Searches the Hamiltonian for a given energy term and removes it if found
    *
    *	@param	interactionType
    *	@param	atom1
    *	@param	atom2
    *	@return
    */
    public void
    removeTerm( String interactionType, Atom atom1, Atom atom2 )
    {
    	LinkedList interactionList = myEnergyTerms.getInteractionList( interactionType );
    	ListIterator energyTerms = interactionList.listIterator(0); 
    	while( energyTerms.hasNext() )
    	{
    		AbstractEnergyTerm thisTerm = (AbstractEnergyTerm)energyTerms.next();
    		Atom[] atoms = thisTerm.getAtoms();
    		
    		// if the atoms in this term match the pair of atoms we're searching for...
    		if( (atom1 == atoms[0] && atom2 == atoms[1]) || (atom1 == atoms[1] && atom2 == atoms[0]) )
    		{
    			//...then remove this energy term; this is the one we were looking for.
    			boolean removedTerm = interactionList.remove( thisTerm );
    			
    			/* DEBUG
    			System.out.print( "Removed " + interactionType + ", Atoms " );
    			System.out.print( atom1.getSerialNumber() );
    			System.out.print( " and " );
    			System.out.println( atom2.getSerialNumber() );
    			*/
    			break;
    		}
    		else
    		{
    			;
    		}
    		
    	}
    	
    	return;
    }
    
    /** 
    *	Searches the Hamiltonian for a given energy term.
    *
    *	@param	energyTerm
    *	@return True if this energy term is already in the Hamiltonian
    */
    public boolean
    containsEnergyTerm( AbstractEnergyTerm energyTerm )
    {
    	// This new energy term is what kind of interaction? Example: HarmonicBond
    	String interactionType = energyTerm.getName();
    	// Find the appropriate list to add this new energy term to
    	LinkedList interactionList = myEnergyTerms.getInteractionList( interactionType );
    	// If this kind of interaction doesn't exist in the energy function...
    	if ( interactionList == null )
    	{
    		// ...then we need to stop
    		System.err.println( "Error in the energy function: Failed to find any " + 
    							interactionType + " interactions." ); 
    		throw new NullPointerException(); 
    	}
    	else
    	{
    		return interactionList.contains( energyTerm );
    	}
    }
    
    /** 
    *	Calculate total energy for a given interaction type
    *	
    *	@param	interactionType
    */
    public double
    calculateInteractionEnergy( String interactionType )
    {
    	LinkedList interactionList = myEnergyTerms.getInteractionList( interactionType );
    	ListIterator energyTerms = interactionList.listIterator(0); 
    	double energy = 0.0;
    	AbstractEnergyTerm thisEnergyTerm;
    
    	while( energyTerms.hasNext() )
    	{
    		thisEnergyTerm = (AbstractEnergyTerm)energyTerms.next();
			/*
			System.out.println( thisEnergyTerm.calculateEnergy() );
			System.out.print( "Energy = " );
			System.out.print( energy );
			System.out.print( " + " );
			System.out.println( thisEnergyTerm.calculateEnergy() );
			*/
			energy = energy + thisEnergyTerm.calculateEnergy();	
    	}
    	
    	return energy;
    }
    
    /** 
    *	Calculate total energy for every interaction type
    */
    public LinkedList
    calculateEnergy( )
    {
    	LinkedList interactionList;
    	ListIterator energyTerms;
    	AbstractEnergyTerm thisEnergyTerm;
    	String thisInteractionName;
    	LinkedList energyList = new LinkedList();
    
    	ListIterator interactionNames = myEnergyTerms.getInteractionNames().listIterator(0);
    	while( interactionNames.hasNext() )
    	{
    		thisInteractionName = (String)interactionNames.next();
    		energyList.add( calculateInteractionEnergy( thisInteractionName ) );
    	}
    	return energyList;
    }
    
    /** 
    *	Iterate through all terms in the energy function and calculate the
    *	forces associated with each of them. Also, calculates virial.
    *
    */
    public void
    calculateForce( )
    {
    	LinkedList interactionList;
    	ListIterator energyTerms;
    	AbstractEnergyTerm thisEnergyTerm;
    	String thisInteractionName;
    	double virial = 0.0;
    
    	ListIterator interactionNames = myEnergyTerms.getInteractionNames().listIterator(0);
    	while( interactionNames.hasNext() )
    	{
    		thisInteractionName = (String)interactionNames.next();
    		interactionList = myEnergyTerms.getInteractionList( thisInteractionName );
    		
    		energyTerms = interactionList.listIterator(0);
    		while( energyTerms.hasNext() )
    		{
    			thisEnergyTerm = (AbstractEnergyTerm)energyTerms.next();
    			virial += thisEnergyTerm.calculateForce();	
    		}
    	}
    	
    	setCurrentVirial( virial );
    }
    
    /** 
    *	Prints all the energy terms of the given interaction type.
    *	Mainly for testing.
    *
    *	@param	interactionType
  	*
    */
    public void
    printEnergyTerms( String interactionType )
    {
	    LinkedList interactionList = myEnergyTerms.getInteractionList( interactionType );
	    ListIterator interactionIterator = interactionList.listIterator(0);
	    while( interactionIterator.hasNext() )
	    {
	    	System.out.println( (AbstractEnergyTerm)interactionIterator.next() );
	    }
  	}
  	
  	public LinkedList
  	getInteractionNames()
  	{
  		return getEnergyTerms().getInteractionNames();
  	}
    
	/*-----------------------------------------------------------------
	*	Accessors
	*--------------------------------------------------------------------*/

    public EnergyTermTable
    getEnergyTerms( )
        {
			return myEnergyTerms;
        }

    public void
    setEnergyTerms( EnergyTermTable newEnergyTerms )
        {
			myEnergyTerms = newEnergyTerms;
        }

	public double
    getCurrentVirial( )
        {
			return myCurrentVirial;
        }

    public void
    setCurrentVirial( double newCurrentVirial )
        {
			myCurrentVirial = newCurrentVirial;
        }


    /*====================================================================
        Private Member Functions
    */
    

    /*====================================================================
        Private Data Members
    */

    private EnergyTermTable myEnergyTerms;
	private double myCurrentVirial;
	
}

