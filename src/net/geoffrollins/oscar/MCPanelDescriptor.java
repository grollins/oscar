package net.geoffrollins.oscar;
import com.nexes.wizard.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;


public class MCPanelDescriptor extends WizardPanelDescriptor {
    
    public static final String IDENTIFIER = "MC_PANEL";
    
    MCPanel monteCarloPanel;
    
    public MCPanelDescriptor() 
    {
        
        monteCarloPanel = new MCPanel();
        setPanelDescriptorIdentifier(IDENTIFIER);
        setPanelComponent(monteCarloPanel);    
    }
    
    public Object getNextPanelDescriptor() 
    {
    	return MCTempProtocolPanelDescriptor.IDENTIFIER;
    }
    
    public Object getBackPanelDescriptor() {
        return AlgorithmPanelDescriptor.IDENTIFIER;
    }
    
    public void aboutToHidePanel() 
    {
    	// save MD parameters to parameter manager
    	ParameterManager parameterManager = getWizard().getParameterManager();
    	parameterManager.setMCMethod( monteCarloPanel.getMCMethodSelected() );
    	parameterManager.setMaxDisplacement( Double.valueOf(monteCarloPanel.getStepSizeText()) );
    	parameterManager.setNumSteps( Integer.valueOf(monteCarloPanel.getNumberStepsText()) );
    	parameterManager.setTemperature( Double.valueOf(monteCarloPanel.getTemperatureText()) );
    	parameterManager.setBoxLength( Double.valueOf(monteCarloPanel.getBoxLengthText()) );
    	parameterManager.setNumDimensions( Integer.valueOf(monteCarloPanel.getNumDimensions()) );
    	
    	// create a default temperature protocol for the user to see when the
    	// temperature protocol panel first appears
    	//double initialTemp = parameterManager.getTemperature();
    	//parameterManager.addCycle( "0", "Monte Carlo", "1", "100", String.valueOf( initialTemp ), String.valueOf( initialTemp ) );
    }      

}
