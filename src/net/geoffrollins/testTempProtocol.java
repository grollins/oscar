package net.geoffrollins.oscar;

/* Test of TemperatureProtocol class */
public class testTempProtocol
{

	public testTempProtocol(){
	}
	
	public static void main(String [ ] args)
	{
		TemperatureProtocol thermostat = new TemperatureProtocol();
		
		/*
		thermostat.saveCycle( 0, "Reassignment", 10, 100, 0.0, 500.0 );
		thermostat.saveCycle( 1, "Berendsen Bath", 1, 30, 500.0, 500.0 );
		thermostat.saveCycle( 2, "Rescaling", 1, 50, 500.0, 0.0 );
		thermostat.setTotalNumSteps( 1000 );
		
		thermostat.saveCycle( 0, "DDR", 1, 20, 300.0, 500.0 );
		thermostat.saveCycle( 2, "Reassignment", 10, 100, 0.0, 500.0 );
		thermostat.saveCycle( 5, "Reassignment", 10, 100, 0.0, 500.0 );
		*/
		
		//System.out.println( thermostat );
		
		int step;
		
		try
		{
		
			step = 100;
			System.out.println( step );
			System.out.println( thermostat.isThermStep(step) );
			System.out.println( thermostat.getCurrentThermMethod(step) );
			System.out.println( thermostat.getCurrentTargetTemp(step) );
			System.out.println( "" );
			
			step = 250;
			System.out.println( step );
			System.out.println( thermostat.isThermStep(step) );
			System.out.println( thermostat.getCurrentThermMethod(step) );
			System.out.println( thermostat.getCurrentTargetTemp(step) );
			System.out.println( "" );
			
			step = 999;
			System.out.println( step );
			System.out.println( thermostat.isThermStep(step) );
			System.out.println( thermostat.getCurrentThermMethod(step) );
			System.out.println( thermostat.getCurrentTargetTemp(step) );
			System.out.println( "" );
		}
		catch( Exception e )
		{
			System.out.println(  "Caught exception. Exiting..." );
		}
	}
	
}