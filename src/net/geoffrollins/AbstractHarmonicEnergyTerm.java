/*****************************************************************************
    AbstractHarmonicEnergyTerm

    Last Updated   - January 14, 2008
    Updated By     - Geoff Rollins

*****************************************************************************/

/*****************************************************************************
    Package and import statments
*/

package net.geoffrollins.oscar;


/*****************************************************************************
    Class Definitions
*/

/*============================================================================
    Framework for harmonic energy terms
 ............................................................................

    Description of class

============================================================================*/
public abstract class AbstractHarmonicEnergyTerm 
{

    /*====================================================================
        Public Member Functions
    */

	/**-----------------------------------------------------------------
    *	Calculate the separation between this harmonic term's constituent atoms
	*
	*	@return			current separation
	--------------------------------------------------------------------*/
    abstract public double
    calculateSeparation();

	/**-----------------------------------------------------------------
    *	Calculate the energy of this harmonic term based on the current
    *	positions of its constituent atoms
	*
	*	@return			energy
	--------------------------------------------------------------------*/
    abstract public double
    calculateEnergy();

	/**-----------------------------------------------------------------
	*	Accessors
	--------------------------------------------------------------------*/
	
    public Atom[]
    getAtoms( )
        {
			return myAtoms;
        }

	public void
    setAtoms( Atom[] newAtoms )
        {
			myAtoms = newAtoms;
        }
        
    public double
    getForceConstant( )
        {
			return myForceConstant;
        }

    public void
    setForceConstant( double newForceConstant )
        {
			myForceConstant = newForceConstant;
        }
        
    public double
    getEquilibriumSeparation( )
        {
			return myEquilibriumSeparation;
        }

    public void
    setEquilibriumSeparation( double newEquilibriumSeparation )
        {
			myEquilibriumSeparation = newEquilibriumSeparation;
        }


    /*====================================================================
        Private Member Functions
    */


    /*====================================================================
        Private Data Members
    */

    private Atom[] myAtoms; 				// the atoms governed by this energy term
	private double myForceConstant;			// the stiffness of the interaction
	private double myEquilibriumSeparation;	// the ideal distance or angle between these atoms

    }

