package net.geoffrollins.oscar;

import javax.swing.JProgressBar;

/* Test of OscarSimulation class */
public class testOscarSimulation
{

	public testOscarSimulation(){
	}
	
	public static void main(String [ ] args)
	{
		OscarSimulation testSim = new OscarSimulation( );
		JProgressBar fakeBar = new JProgressBar();
		
		try
		{
			testSim.readCoordinates( "/Users/geoff/Documents/Java/test/LJ10b_neutral.pdb" );
		}
		catch( Exception e )
		{
			System.err.println( "File error" );
			System.exit(0);
		}
		
		try
		{
			testSim.readStructure( "<nothing>" );
		}
		catch( Exception e )
		{
			System.err.println( "File or structure creation error" );
			System.exit(0);
		}
		
		testSim.addBox( 500.0 );
		
		//testSim.getHamiltonian().printEnergyTerms("Box");
		//testSim.getHamiltonian().calculateForce();

		
		//System.out.print( testSim.getMolecularSystem() );
		//System.out.println(testSim.getHamiltonian().calculateInteractionEnergy("HarmonicBond"));
		//System.out.println(testSim.getHamiltonian().calculateInteractionEnergy("VanDerWaals"));
		//testSim.getHamiltonian().printEnergyTerms("HarmonicBond");
		//testSim.getHamiltonian().printEnergyTerms("VanDerWaals");
		
		AbstractSimAlgorithm simAlgorithm = new DefaultMD( 0.001 );
		//simAlgorithm.setTemperature( 0.0 );
		
		//AbstractSimAlgorithm simAlgorithm = new ConjugateGradientMinimization( 0.1, 0.001 );
		//AbstractSimAlgorithm simAlgorithm = new SteepestDescentMinimization( 1.0, 0.001 );
		
		//AbstractSimAlgorithm simAlgorithm = new MetropolisMonteCarlo( 100.0 );
		//simAlgorithm.setTemperature( 300.0 );
		
		testSim.setSimAlgorithm( simAlgorithm );
		
		TemperatureProtocol thermostat = new TemperatureProtocol();
		thermostat.saveCycle( 0, "Reassignment", 100, 100, 300.0, 300.0 );
		//thermostat.saveCycle( 1, "Berendsen Bath", 1, 30, 500.0, 500.0 );
		//thermostat.saveCycle( 2, "Rescaling", 1, 50, 500.0, 0.0 );
		thermostat.setTotalNumSteps( 10000 );
		
		try
		{
			testSim.runSimulation( 10000, 100, "/Users/geoff/Documents/Java/test/", "temp_test", fakeBar, thermostat );
		}
		catch (Exception e) 
		{
			System.out.println("An Error Has Occurred");
			System.out.println( e.getMessage() );
			
        	StackTraceElement elements[] = e.getStackTrace();
        	for (int i = 0, n = elements.length; i < n; i++) 
        	{       
            	System.err.println(elements[i].getFileName() + ":" 
                          + elements[i].getLineNumber() 
                          + ">> " 
                          + elements[i].getMethodName() + "()");
 
			}
		}
		
		
	}
}