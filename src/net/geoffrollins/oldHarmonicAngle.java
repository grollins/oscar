/*****************************************************************************
    HarmonicAngle

    Last Updated   - January 24, 2008
    Updated By     - Geoff Rollins

*****************************************************************************/

/*****************************************************************************
    Package and import statments
*/

package net.geoffrollins.oscar;


/*****************************************************************************
    Class Definitions
*/

/*============================================================================
    Harmonic oscillator approximation of the angle between two covalent bonds
============================================================================*/
public class HarmonicAngle 
extends AbstractEnergyTerm
{

	static final double RADIAN_CONVERSION_FACTOR = Math.PI / 180.0;
	static final String IDENTIFIER = "Angle";
	/** 
    *	Default class constructor.
    */
    public HarmonicAngle( )
        {
			setAtoms( new Atom[3] ); //allocates memory for the three atoms joined by this angle
			setForceConstant( 0.0 );
			setEquilibriumSeparation( 0.0 );
			setName( IDENTIFIER );
        }

	/** 
    *	Class constructor.
    */
    public HarmonicAngle( Atom[] atoms, double forceConstant, double equilibriumAngle  )
        {
        	setAtoms( atoms );
			setForceConstant( forceConstant * AbstractEnergyTerm.CEU_CONVERSION_FACTOR );
			setEquilibriumSeparation( equilibriumAngle );
        	setName( IDENTIFIER );
        }

    
    /*====================================================================
        Public Member Functions
    */
    
    public double
    calculateDeltaX()
    {
    	Atom[] atoms = getAtoms();
    	return atoms[1].getX() - atoms[0].getX(); 
    }

	public double
	calculateDeltaY()
	{
		Atom[] atoms = getAtoms();
		return atoms[1].getY() - atoms[0].getY();
    }
    
    public double
    calculateDeltaZ()
    {
    	Atom[] atoms = getAtoms();
    	return atoms[1].getZ() - atoms[0].getZ();
    }
    
    public double
    calculateDeltaX2()
    {
    	Atom[] atoms = getAtoms();
    	return atoms[2].getX() - atoms[1].getX(); 
    }

	public double
	calculateDeltaY2()
	{
		Atom[] atoms = getAtoms();
		return atoms[2].getY() - atoms[1].getY();
    }
    
    public double
    calculateDeltaZ2()
    {
    	Atom[] atoms = getAtoms();
    	return atoms[2].getZ() - atoms[1].getZ();
    }
    
    public double
    calculateMagnitudeBond1()
    {
    	double xbond1 = calculateDeltaX(); 
    	double ybond1 = calculateDeltaY();
    	double zbond1 = calculateDeltaZ();
    	
    	return Math.sqrt( Math.pow( xbond1, 2 ) + Math.pow( ybond1, 2 ) + Math.pow( zbond1, 2 ) );
    	
    }
    
    public double
    calculateMagnitudeBond2()
    {
    	double xbond2 = calculateDeltaX2(); 
    	double ybond2 = calculateDeltaY2();
    	double zbond2 = calculateDeltaZ2();
    	
    	return Math.sqrt( Math.pow( xbond2, 2 ) + Math.pow( ybond2, 2 ) + Math.pow( zbond2, 2 ) );
    	
    }
    
    public double
    calculateDotProduct()
    {
    	double xbond1 = calculateDeltaX(); 
    	double ybond1 = calculateDeltaY();
    	double zbond1 = calculateDeltaZ();
    	double xbond2 = calculateDeltaX2(); 
    	double ybond2 = calculateDeltaY2();
    	double zbond2 = calculateDeltaZ2();
    	
    	return (xbond1 * xbond2) + (ybond1 * ybond2) + (zbond1 * zbond2);
    }
    
    /**-----------------------------------------------------------------
    *	Calculate the angle measure (the distance between the two atoms connected by this angle)
	*
	*	@return			current angle measure (radians)
	--------------------------------------------------------------------*/
    public double
    calculateSeparation( )
    {	
    	double dotProduct = calculateDotProduct( );
    	double magnitudeBond1 = calculateMagnitudeBond1( ); 
    	double magnitudeBond2 = calculateMagnitudeBond2( );
    	
    	double cosineTheta = dotProduct / (magnitudeBond1 * magnitudeBond2);
    	return Math.acos( cosineTheta );
    }
    
	/**-----------------------------------------------------------------
    *	Calculate the energy of this angle
	*	E = 1/2 * forceConstant * (measure - equilibriumAngle)^2
	*
	*	@return			energy of this angle
	--------------------------------------------------------------------*/
    public double
    calculateEnergy( )
    {
    	double currentAngle = calculateSeparation();
    	double idealAngle = getEquilibriumSeparation() * RADIAN_CONVERSION_FACTOR;
  	
  		//System.out.println( 0.5 * getForceConstant() * Math.pow( ( currentAngle - idealAngle ), 2 ) );
  
    	return 0.5 * getForceConstant() * Math.pow( ( currentAngle - idealAngle ), 2 );
    }

	/**-----------------------------------------------------------------
    *	Calculate the force of this term based on the current
    *	positions of its constituent atoms. Add this force to
    *	the atoms' current force values.
	*
	*	F = -forceConstant * (angle - equilibriumAngle)
	*	Atoms 0, 1, and 2. Atom 1 is the vertex.
	--------------------------------------------------------------------*/
    public double
    calculateForce()
    {
    	double currentAngle = calculateSeparation();
    	double idealAngle = getEquilibriumSeparation() * RADIAN_CONVERSION_FACTOR;
    	double totalForce = -getForceConstant() * ( currentAngle - idealAngle );
    	
    	double magnitudeBond1 = calculateMagnitudeBond1();
    	double magnitudeBond2 = calculateMagnitudeBond2();
    	double dotProduct = calculateDotProduct();
    	double dx1 = calculateDeltaX(); 
    	double dy1 = calculateDeltaY();
    	double dz1 = calculateDeltaZ();
    	double dx2 = calculateDeltaX2(); 
    	double dy2 = calculateDeltaY2();
    	double dz2 = calculateDeltaZ2();
    	
    	double xForceOnAtom0 = -(totalForce / (Math.sin(currentAngle) * Math.sqrt(magnitudeBond1 * magnitudeBond2))) * ( dx2 - (dotProduct/magnitudeBond1)*dx1 ); 
    	double xForceOnAtom2 = -(totalForce / (Math.sin(currentAngle) * Math.sqrt(magnitudeBond1 * magnitudeBond2))) * ( dx2 - (dotProduct/magnitudeBond2)*dx1 ); 
    	double xForceOnAtom1 = -xForceOnAtom0 - xForceOnAtom2;
    	
    	double yForceOnAtom0 = -(totalForce / (Math.sin(currentAngle) * Math.sqrt(magnitudeBond1 * magnitudeBond2))) * ( dy2 - (dotProduct/magnitudeBond1)*dy1 ); 
    	double yForceOnAtom2 = -(totalForce / (Math.sin(currentAngle) * Math.sqrt(magnitudeBond1 * magnitudeBond2))) * ( dy2 - (dotProduct/magnitudeBond2)*dy1 ); 
    	double yForceOnAtom1 = -yForceOnAtom0 - yForceOnAtom2;

		double zForceOnAtom0 = -(totalForce / (Math.sin(currentAngle) * Math.sqrt(magnitudeBond1 * magnitudeBond2))) * ( dz2 - (dotProduct/magnitudeBond1)*dz1 ); 
    	double zForceOnAtom2 = -(totalForce / (Math.sin(currentAngle) * Math.sqrt(magnitudeBond1 * magnitudeBond2))) * ( dz2 - (dotProduct/magnitudeBond2)*dz1 ); 
    	double zForceOnAtom1 = -zForceOnAtom0 - zForceOnAtom2;
    	
    	double[] forceOnAtom0 = {xForceOnAtom0, yForceOnAtom0, zForceOnAtom0};
    	double[] forceOnAtom1 = {xForceOnAtom1, yForceOnAtom1, zForceOnAtom1};
    	double[] forceOnAtom2 = {xForceOnAtom2, yForceOnAtom2, zForceOnAtom2};
    	
    	Atom[] atoms = getAtoms();
    	atoms[0].addForce( forceOnAtom0 );
    	atoms[1].addForce( forceOnAtom1 );
    	atoms[2].addForce( forceOnAtom2 );
    	
    	
    	//System.out.print( "Atom0: " ); System.out.print( xForceOnAtom0 ); System.out.print( "\t" ); System.out.print( yForceOnAtom0 ); System.out.print( "\t" ); System.out.println( zForceOnAtom0 );
    	//System.out.print( "Atom1: " ); System.out.print( xForceOnAtom1 ); System.out.print( "\t" ); System.out.print( yForceOnAtom1 ); System.out.print( "\t" ); System.out.println( zForceOnAtom1 );
    	//System.out.print( "Atom2: " ); System.out.print( xForceOnAtom2 ); System.out.print( "\t" ); System.out.print( yForceOnAtom2 ); System.out.print( "\t" ); System.out.println( zForceOnAtom2 ); System.out.println( "" );
    	
    	return 0.0;
    }



	public String
    toString( )
    {
	    Atom[] atoms = getAtoms();
	    return atoms[0].getSerialNumber() + "\t" + atoms[1].getSerialNumber() + "\t" + atoms[2].getSerialNumber() + "\t" +  getForceConstant()/AbstractEnergyTerm.CEU_CONVERSION_FACTOR + "\t" + getEquilibriumSeparation();
    }

	/**-----------------------------------------------------------------
	*	Accessors
	*--------------------------------------------------------------------*/
	
	public double
    getForceConstant( )
        {
			return myForceConstant;
        }

    public void
    setForceConstant( double newForceConstant )
        {
			myForceConstant = newForceConstant;
        }
        
	public double
    getEquilibriumSeparation( )
        {
			return myEquilibriumSeparation;
        }

    public void
    setEquilibriumSeparation( double newEquilibriumSeparation )
        {
			myEquilibriumSeparation = newEquilibriumSeparation;
        }     


    /*====================================================================
        Private Member Functions
    */

    /*====================================================================
        Private Data Members
    */
    private double myForceConstant;			// the stiffness of the angle, kcal/mol/rad^2
	private double myEquilibriumSeparation;	// the ideal angle measure, degrees

    }

