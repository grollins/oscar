package net.geoffrollins.oscar;

/* Test of Torsion class */
public class testTorsion
{

	public testTorsion(){
	}
	
	public static void main(String [ ] args)
	{
		Atom atom1 = new Atom( 1, "O", -2.5, 4.0, 0.0, 2.0, 20.0 );
		Atom atom2 = new Atom( 2, "O", -2.5, 0.0, 0.0, 2.0, 20.0 );
		Atom atom3 = new Atom( 3, "O", 2.5, 0.0, 0.0, 2.0, 20.0 );
		Atom atom4 = new Atom( 4, "O", 2.5, 0.0, 2.0, 2.0, 20.0 );
		
		double forceConstant = 10.0;
		int multiplicity = 1;
		double delta = 120.0;
		Atom[] torsionAtoms = { atom1, atom2, atom3, atom4 };
		Torsion torsion1 = new Torsion( torsionAtoms, forceConstant, multiplicity, delta );
		
		System.out.println( "Current torsion angle = " + torsion1.calculateSeparation( ) + " radians" );
		
		System.out.println( "Current torsion energy = " + torsion1.calculateEnergy() );
		
		System.out.println( "Force on atom1:" );
		System.out.println( atom1.getForce()[0] );
		System.out.println( atom1.getForce()[1] );
		System.out.println( atom1.getForce()[2] );
		System.out.println( "Force on atom2:" );
		System.out.println( atom2.getForce()[0] );
		System.out.println( atom2.getForce()[1] );
		System.out.println( atom2.getForce()[2] );
		System.out.println( "Force on atom3:" );
		System.out.println( atom3.getForce()[0] );
		System.out.println( atom3.getForce()[1] );
		System.out.println( atom3.getForce()[2] );
		System.out.println( "Force on atom4:" );
		System.out.println( atom4.getForce()[0] );
		System.out.println( atom4.getForce()[1] );
		System.out.println( atom4.getForce()[2] );
		
		torsion1.calculateForce();
		
		System.out.println( "After calculating force..." );
		System.out.println( "Force on atom1:" );
		System.out.println( atom1.getForce()[0] );
		System.out.println( atom1.getForce()[1] );
		System.out.println( atom1.getForce()[2] );
		System.out.println( "Force on atom2:" );
		System.out.println( atom2.getForce()[0] );
		System.out.println( atom2.getForce()[1] );
		System.out.println( atom2.getForce()[2] );
		System.out.println( "Force on atom3:" );
		System.out.println( atom3.getForce()[0] );
		System.out.println( atom3.getForce()[1] );
		System.out.println( atom3.getForce()[2] );
		System.out.println( "Force on atom4:" );
		System.out.println( atom4.getForce()[0] );
		System.out.println( atom4.getForce()[1] );
		System.out.println( atom4.getForce()[2] );
		
		
		/*
		atom1.zeroForce();
		atom2.zeroForce();
		atom3.zeroForce();
		atom4.zeroForce();
		
		atom3.setX( 3.5 );
		System.out.println( "Changed x coordinate of one atom." );
		System.out.println( "Current torsion angle = " + torsion1.calculateSeparation( ) + " radians" );
		System.out.println( "Current torsion energy = " + torsion1.calculateEnergy() );
		
		torsion1.calculateForce();
		
		System.out.println( "After calculating force..." );
		System.out.println( "Force on atom1:" );
		System.out.println( atom1.getForce()[0] );
		System.out.println( atom1.getForce()[1] );
		System.out.println( atom1.getForce()[2] );
		System.out.println( "Force on atom2:" );
		System.out.println( atom2.getForce()[0] );
		System.out.println( atom2.getForce()[1] );
		System.out.println( atom2.getForce()[2] );
		System.out.println( "Force on atom3:" );
		System.out.println( atom3.getForce()[0] );
		System.out.println( atom3.getForce()[1] );
		System.out.println( atom3.getForce()[2] );
		
		
		
		double[] zeroForce = {0.00, 0.00, 0.00};
		neutralAtom.setForce( zeroForce );
		chargedAtom.setForce( zeroForce );
		System.out.print("zeroForce = ");
		System.out.print(zeroForce[0]);
		System.out.print(zeroForce[1]);
		System.out.println(zeroForce[2]);
		
		force0 = neutralAtom.getForce();
		force1 = chargedAtom.getForce();
		
		System.out.println( "After zeroing force..." );
		System.out.println( "Force on atom0:" );
		System.out.println( neutralAtom.getForce()[0] );
		System.out.println( neutralAtom.getForce()[1] );
		System.out.println( neutralAtom.getForce()[2] );
		System.out.println( "Force on atom1:" );
		System.out.println( chargedAtom.getForce()[0] );
		System.out.println( chargedAtom.getForce()[1] );
		System.out.println( chargedAtom.getForce()[2] );
		
		bond1.calculateForce();
		
		System.out.println( "After calculating force..." );
		System.out.println( "Force on atom0:" );
		System.out.println( neutralAtom.getForce()[0] );
		System.out.println( neutralAtom.getForce()[1] );
		System.out.println( neutralAtom.getForce()[2] );
		System.out.println( "Force on atom1:" );
		System.out.println( chargedAtom.getForce()[0] );
		System.out.println( chargedAtom.getForce()[1] );
		System.out.println( chargedAtom.getForce()[2] );
		*/
		
	}
	
}