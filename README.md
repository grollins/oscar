# Running Oscar

Start Oscar by double-clicking on Oscar.jar, or you may type 
"java -jar Oscar.jar" on the command line.

Note: The folder containing Oscar.jar must also contain a folder called "lib", which contains three files: text_format.zip, liquidlnf.jar, and swing-layout-1.0.jar. The lib folder, along with the three files mentioned above, is included in Oscar.zip.

